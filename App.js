import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import React, { useEffect, useRef, useState } from "react";
import { UIManager, Platform } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  SafeAreaProvider,
  initialWindowMetrics,
} from "react-native-safe-area-context";
import Home from "./components/Home/Home";
import GameSetup from "./components/GameSetup/GameSetup";
import Game from "./components/Game/Game";
import { enableScreens } from "react-native-screens";
import Browse from "./components/Browse/Browse";
import Custom from "./components/Custom/Custom";
import useSettings, {
  SettingsContext,
} from "./components/general/SettingsHook/SettingsHook";
import { defaultCasual } from "./components/general/SettingsHook/settingsConstants";
import Settings from "./components/Settings/Settings";
import useTheme, {
  ThemeContext,
} from "./components/general/ThemeHook/ThemeHook";
import { useFonts } from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import { useScreenOrientationLock } from "@use-expo/screen-orientation";
import { OrientationLock } from "expo-screen-orientation";
import useAnimation, {
  AnimationContext,
} from "./components/general/AnimationHook/AnimationHook";
import SignIn from "./components/SignIn/SignIn";
import useUser, { UserContext } from "./components/general/UserHook/UserHook";
import SignUp from "./components/SignUp/SignUp";
import Message from "./components/general/Message/Message";
import useMessage, {
  MessageContext,
} from "./components/general/Message/MessageHook";
import Account from "./components/Account/Account";
import Highscores from "./components/Highscores/Highscores";
import About from "./components/About/About";
import admob, { MaxAdContentRating } from "@react-native-firebase/admob";
import Upgrade from "./components/Upgrade/Upgrade";
import Iaphub from "react-native-iaphub";
import HowToPlay from "./components/HowToPlay/HowToPlay";

enableScreens();

const { Navigator, Screen } = createStackNavigator();
const customFonts = {
  "Comfortaa-Light": require("./assets/fonts/Comfortaa-Light.ttf"),
};

// needed for animationLayout
if (
  Platform.OS === "android" &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

SplashScreen.preventAutoHideAsync(); // TODO: uncomment this once they've figured out their shit

function App() {
  const [fontsLoaded] = useFonts(customFonts);
  const [admobLoaded, setAdmobLoaded] = useState(false);
  const appStarted = useRef(false);
  const settingsInfoAndCallbacks = useSettings(defaultCasual);
  const {
    usePhonesAppearance,
    darkMode,
    animationSpeed,
    showAnimations,
  } = settingsInfoAndCallbacks.settings;
  const theme = useTheme(usePhonesAppearance, darkMode);
  const { mode } = theme;
  const animation = useAnimation(animationSpeed, showAnimations);
  const userInfo = useUser();
  const messageInfoAndCallbacks = useMessage();

  useEffect(() => {
    const setup = async () => {
      try {
        await Iaphub.init({
          // The app id is available on the settings page of your app
          appId: "5f21cc41cffe600ea2508905",
          // The (client) api key is available on the settings page of your app
          apiKey: "sqVL1QLBfTnYPQ6x0pvtjJKokASWD32",
          // App environment (production by default, other environments must be created on the IAPHUB dashboard)
          environment: "production",
        });
      } catch (e) {
        console.log(e);
      }
    };

    setup();

    return () => {};
  }, []);

  useEffect(() => {
    admob()
      .setRequestConfiguration({
        // Update all future requests suitable for parental guidance
        maxAdContentRating: MaxAdContentRating.T,

        // Indicates that you want your content treated as child-directed for purposes of COPPA.
        tagForChildDirectedTreatment: true,

        // Indicates that you want the ad request to be handled in a
        // manner suitable for users under the age of consent.
        tagForUnderAgeOfConsent: false,
      })
      .then(() => {
        // Request config successfully set!
      })
      .finally(() => setAdmobLoaded(true));
  }, []);

  useEffect(() => {
    if (fontsLoaded && !appStarted.current && userInfo.initialized) {
      appStarted.current = true;
      SplashScreen.hideAsync();
    }
  }, [fontsLoaded, userInfo.initialized]);

  const [lockInfo, lockError] = useScreenOrientationLock(
    OrientationLock.PORTRAIT
  );

  if (!fontsLoaded) {
    return null;
  } else {
    return (
      <SafeAreaProvider initialMetrics={initialWindowMetrics}>
        <StatusBar style={mode === "dark" ? "light" : "dark"} />
        <UserContext.Provider value={userInfo}>
          <SettingsContext.Provider value={settingsInfoAndCallbacks}>
            <ThemeContext.Provider value={theme}>
              <AnimationContext.Provider value={animation}>
                <MessageContext.Provider value={messageInfoAndCallbacks}>
                  <Message />
                  <NavigationContainer>
                    <Navigator
                      initialRouteName="Home"
                      headerMode="none"
                      screenOptions={{ animationEnabled: true }}
                    >
                      <Screen name="Home" component={Home} />
                      <Screen name="GameSetup" component={GameSetup} />
                      {/* animation false needed for blur */}
                      <Screen
                        name="Game"
                        component={Game}
                        options={{ animationEnabled: false }}
                      />
                      <Screen name="Browse" component={Browse} />
                      <Screen name="Custom" component={Custom} />
                      <Screen name="Settings" component={Settings} />
                      <Screen name="SignIn" component={SignIn} />
                      <Screen name="SignUp" component={SignUp} />
                      <Screen name="Account" component={Account} />
                      <Screen name="Highscores" component={Highscores} />
                      <Screen name="About" component={About} />
                      <Screen name="Upgrade" component={Upgrade} />
                      <Screen name="HowToPlay" component={HowToPlay} />
                    </Navigator>
                  </NavigationContainer>
                </MessageContext.Provider>
              </AnimationContext.Provider>
            </ThemeContext.Provider>
          </SettingsContext.Provider>
        </UserContext.Provider>
      </SafeAreaProvider>
    );
  }
}

export default App;
