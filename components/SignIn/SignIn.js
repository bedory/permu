import React, { useState, useContext, useEffect } from "react";
import { View } from "react-native";
import useSignInModalStyle from "./SignInStyle";
import StyledButton from "../general/StyledButton/StyledButton";
import { MessageContext, MessageTypes } from "../general/Message/MessageHook";
import auth from "@react-native-firebase/auth";
import {
  checkValidEmailFormat,
  removeWhiteSpaceLeftAndRight,
} from "../general/randomFunctions";
import { UserContext } from "../general/UserHook/UserHook";
import LoadingIndicatorContainer from "../general/LoadingIndicatorContainer/LoadingIndicatorContainer";
import StyledText from "../general/StyledText/StyledText";
import Separator from "../general/Separator/Separator";
import Screen from "../general/Screen/Screen";
import InputSection from "../general/InputSection/InputSection";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";

const SignIn = ({ navigation, route }) => {
  const style = useSignInModalStyle();
  const params = route.params;
  const [email, setEmail] = useState();
  const [emailError, setEmailError] = useState();
  const [password, setPassword] = useState();
  const [passwordError, setPasswordError] = useState();
  const [sendingPasswordReset, setSendingPasswordReset] = useState(false);
  const { reload } = useContext(UserContext);
  const { setMessageAndShow } = useContext(MessageContext);
  const [signingIn, setSigningIn] = useState(false);
  const [signingInWithGoogle, setSigningInWithGoogle] = useState(false);

  useEffect(() => {
    if (params && params.email) {
      setEmail(params.email);
    }
  }, [params]);

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        "439286765899-bru805kga6on0m0b7jg6ua1qgnfkd1dl.apps.googleusercontent.com",
    });
  }, []);

  const checkEmail = (email) => {
    let inputValid = true;
    if (!email || !email.length) {
      inputValid = false;
      setEmailError("Please provide the email associated with the account.");
    } else if (!checkValidEmailFormat(email)) {
      inputValid = false;
      setEmailError(
        "Email must have correct format, e.g. john.doe@example.com"
      );
    } else {
      setEmailError(null);
    }
    setPasswordError(null);
    return inputValid;
  };

  const sendPasswordResetMail = async () => {
    let emailInput = removeWhiteSpaceLeftAndRight(email);
    if (checkEmail(emailInput)) {
      setSendingPasswordReset(true);
      try {
        await auth().sendPasswordResetEmail(emailInput);
        setMessageAndShow(
          `A password reset code was sent to your email.`,
          MessageTypes.SUCCESS
        );
      } catch (error) {
        if (error.code === "auth/user-not-found") {
          setEmailError("could not find a user associated with this email");
        }
      }

      setSendingPasswordReset(false);
    }
  };

  const checkInput = (email, password) => {
    let inputValid = true;
    if (!email || !email.length) {
      inputValid = false;
      setEmailError("Email is required");
    } else if (!checkValidEmailFormat(email)) {
      inputValid = false;
      setEmailError(
        "Email must have correct format, e.g. john.doe@example.com"
      );
    } else {
      setEmailError(null);
    }

    if (!password || !password.length) {
      inputValid = false;
      setPasswordError("Password is required");
    } else {
      setPasswordError(null);
    }
    return inputValid;
  };

  const signIn = async () => {
    let emailInput = removeWhiteSpaceLeftAndRight(email);

    if (checkInput(emailInput, password)) {
      setSigningIn(true);
      try {
        const { user } = await auth().signInWithEmailAndPassword(
          email,
          password
        );
        if (!user.emailVerified) {
          setEmailError("account not verified. Please check your emails");
          await auth().signOut();
        } else {
          await reload();
          setMessageAndShow(
            `Successfully signed in. Welcome ${user.displayName}.`,
            MessageTypes.SUCCESS
          );
          navigation.goBack();
        }
      } catch (error) {
        if (error.code === "auth/user-not-found") {
          setEmailError("could not find a user associated with this email");
        }
        if (error.code === "auth/wrong-password") {
          setPasswordError("password is incorrect");
        }
      }
      setSigningIn(false);
    }
  };

  const forgotPasswordButton = (
    <LoadingIndicatorContainer
      style={style.forgotPasswordContainer}
      loading={sendingPasswordReset}
    >
      <StyledButton
        text="forgot password"
        textStyle={style.forgotPassword}
        onPress={sendPasswordResetMail}
      />
    </LoadingIndicatorContainer>
  );

  async function signInWithGoogle() {
    setSigningInWithGoogle(true);
    try {
      await GoogleSignin.hasPlayServices();
      const { user, idToken } = await GoogleSignin.signIn();
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);
      await auth().signInWithCredential(googleCredential);
      await reload();
      setMessageAndShow(
        `Successfully signed in. Welcome ${user.name}.`,
        MessageTypes.SUCCESS
      );
      navigation.goBack();
    } catch (e) {
      console.log(e.code, e.message);
      if (e.code === statusCodes.SIGN_IN_CANCELLED) {
      } else if (e.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        setMessageAndShow(
          "You're already signing in. Please wait.",
          MessageTypes.WARNING
        );
      } else if (e.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        setMessageAndShow(
          "Play service is currently not available.",
          MessageTypes.ERROR
        );
      } else {
        setMessageAndShow(
          "An error occurred. Please try again later or contact us at info@bedory.com.",
          MessageTypes.ERROR
        );
      }
    }
    setSigningInWithGoogle(false);
  }

  return (
    <Screen style={style.root}>
      <InputSection
        arrayTextInputProps={[
          {
            label: "email",
            placeholder: "john.doe@email.com",
            value: email,
            onChangeText: setEmail,
            error: emailError,
          },
          {
            label: "password",
            placeholder: "secret123",
            value: password,
            onChangeText: setPassword,
            error: passwordError,
            secureTextEntry: true,
            extraComponent: forgotPasswordButton,
          },
        ]}
        loadingIndicatorContainerProps={{ loading: signingIn }}
        buttonProps={{
          text: "sign in",
          onPress: signIn,
        }}
      />

      <View style={style.signUpContainer}>
        <StyledText style={style.message}>
          Don&apos;t have an account yet?{" "}
        </StyledText>
        <StyledButton
          text="Sign up."
          textStyle={style.signUp}
          onPress={() => navigation.navigate("SignUp", { email, password })}
        />
      </View>

      <Separator />

      <GoogleSigninButton
        style={style.socialButton}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Light}
        onPress={signInWithGoogle}
        disabled={signingInWithGoogle}
      />
    </Screen>
  );
};

export default SignIn;
