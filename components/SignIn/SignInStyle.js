import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xsFont, smFont, sm, md, accentColor } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "flex-start",
    },
    forgotPasswordContainer: {
      alignSelf: "flex-end",
      marginTop: sm,
    },
    forgotPassword: {
      fontSize: xsFont,
      lineHeight: xsFont,
      color: accentColor,
    },
    signUpContainer: {
      flexDirection: "row",
      justifyContent: "flex-end",
      alignItems: "center",
      alignSelf: "stretch",
      marginBottom: md,
    },
    message: {
      fontSize: xsFont,
    },
    signUp: {
      fontSize: smFont,
      color: accentColor,
    },
    socialButton: {
      margin: md,
    },
  });
}

export default useStyle;
