import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    xsFont,
    smFont,
    accentColor,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "flex-start",
    },
    signInContainer: {
      flexDirection: "row",
      justifyContent: "flex-end",
      alignItems: "center",
      alignSelf: "stretch",
    },
    message: {
      fontSize: xsFont,
    },
    signIn: {
      fontSize: smFont,
      color: accentColor,
    },
  });
}

export default useStyle;
