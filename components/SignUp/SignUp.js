import React, { useState, useContext } from "react";
import { View } from "react-native";
import useSignInModalStyle from "./SignUpStyle";
import StyledButton from "../general/StyledButton/StyledButton";
import auth from "@react-native-firebase/auth";
import { MessageContext, MessageTypes } from "../general/Message/MessageHook";
import {
  checkValidEmailFormat,
  removeWhiteSpaceLeftAndRight,
} from "../general/randomFunctions";
import InputSection from "../general/InputSection/InputSection";
import StyledText from "../general/StyledText/StyledText";
import Screen from "../general/Screen/Screen";

const SignUp = ({ navigation, route }) => {
  const style = useSignInModalStyle();
  const params = route.params;
  const [username, setUsername] = useState();
  const [usernameError, setUsernameError] = useState();
  const [email, setEmail] = useState(params && params.email);
  const [emailError, setEmailError] = useState();
  const [password, setPassword] = useState(params && params.password);
  const [passwordError, setPasswordError] = useState();
  const { setMessageAndShow } = useContext(MessageContext);
  const [signingUp, setSigningUp] = useState(false);

  const checkInput = (username, email, password) => {
    let inputValid = true;
    if (!username || !username.length) {
      inputValid = false;
      setUsernameError("Username is required");
    } else if (username.length < 5) {
      inputValid = false;
      setUsernameError("Username must be at least 5 characters");
    } else if (username.length > 25) {
      inputValid = false;
      setUsernameError("Username cannot be more than 25 characters");
    } else {
      setUsernameError(null);
    }

    if (!email || !email.length) {
      inputValid = false;
      setEmailError("Email is required");
    } else if (!checkValidEmailFormat(email)) {
      inputValid = false;
      setEmailError(
        "Email must have correct format, e.g. john.doe@example.com"
      );
    } else {
      setEmailError(null);
    }

    if (!password || !password.length) {
      inputValid = false;
      setPasswordError("Password is required");
    } else if (password.length < 6) {
      inputValid = false;
      setPasswordError("Password must be at least 6 characters");
    } else if (password.length > 25) {
      inputValid = false;
      setPasswordError("Password cannot be more than 25 characters");
    } else {
      setPasswordError(null);
    }

    return inputValid;
  };

  const createUser = async () => {
    let usernameInput = removeWhiteSpaceLeftAndRight(username);
    let emailInput = removeWhiteSpaceLeftAndRight(email);

    if (checkInput(usernameInput, emailInput, password)) {
      setSigningUp(true);
      try {
        const { user } = await auth().createUserWithEmailAndPassword(
          emailInput,
          password
        );
        await Promise.all([
          user.updateProfile({ displayName: usernameInput }),
          user.sendEmailVerification(),
        ]);
        setMessageAndShow(
          "Account successfully created. Please check your email to verify your account.",
          MessageTypes.SUCCESS
        );
        await auth().signOut();
        navigation.navigate("SignIn", { email });
      } catch (e) {
        if (e.code === "auth/email-already-in-use") {
          setEmailError("Email is already in use");
        }
        if (e.code === "auth/invalid-email") {
          setEmailError("Email is invalid");
        }
      }
      setSigningUp(false);
    }
  };

  return (
    <Screen style={style.root}>
      <InputSection
        arrayTextInputProps={[
          {
            label: "username",
            placeholder: "PuzzlingJohn",
            value: username,
            onChangeText: setUsername,
            error: usernameError,
          },
          {
            label: "email",
            placeholder: "john.doe@email.com",
            value: email,
            onChangeText: setEmail,
            error: emailError,
          },
          {
            label: "password",
            placeholder: "secret123",
            value: password,
            onChangeText: setPassword,
            error: passwordError,
            secureTextEntry: true,
          },
        ]}
        loadingIndicatorContainerProps={{ loading: signingUp }}
        buttonProps={{
          text: "sign up",
          onPress: createUser,
        }}
      />

      <View style={style.signInContainer}>
        <StyledText style={style.message}>Already have an account? </StyledText>
        <StyledButton
          text="Sign in."
          textStyle={style.signIn}
          onPress={() => navigation.navigate("SignIn", { email })}
        />
      </View>
    </Screen>
  );
};

export default SignUp;
