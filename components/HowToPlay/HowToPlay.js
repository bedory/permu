import React from "react";
import { View, ScrollView } from "react-native";
import useHowToPlayStyle from "./HowToPlayStyle";
import Screen from "../general/Screen/Screen";
import StyledText from "../general/StyledText/StyledText";
import Section from "./Section/Section";
import StyledButton from "../general/StyledButton/StyledButton";
import permutationFamilyMap from "../general/Field/PermutationFamilies";

const HowToPlay = ({navigation}) => {
  const style = useHowToPlayStyle();
  return (
    <Screen style={style.root}>
      <ScrollView contentContainerStyle={style.scrollView}>
        <StyledText style={style.header}>how to play</StyledText>
        <Section headerText="objective">
          <StyledText style={style.text}>
            You start with a scrambled field of cells. The objective is to
            reassemble the field by executing permutations. You execute a
            permutation by swiping a cell.
          </StyledText>
          <View style={style.tipContainer}>
            <StyledText style={style.tip}>Tip: stop reading and </StyledText>
            <StyledButton
              text="start playing."
              textStyle={style.buttonTextStyle}
              onPress={() => {
                navigation.navigate("Game", {
                  numberRows: 4,
                  numberColumns: 4,
                  permutationFamily: permutationFamilyMap.swap,
                  wrap: true,
                });
              }}
            />
          </View>
          <StyledText style={style.tip}>
            Swipe all the cells and press all the buttons. You will quickly get
            a hang of it.
          </StyledText>
        </Section>
        <Section headerText="field">
          <StyledText style={style.text}>
            The field consists of cells. In the simple game mode, each cell has
            a row index in the bottom left corner and a column index in the top
            right corner. These indices define the correct location of the cell.
            The cell with row index 0 and column index 0 has to go to the top
            left corner of the field, and so on.
          </StyledText>
          <StyledText style={style.text}>
            In every other game mode, each cell has small piece of the final
            image. In this case you would reassemble the field the same way you
            would solve a jigsaw puzzle.
          </StyledText>
        </Section>
        <Section headerText="permutations">
          <StyledText style={style.text}>
            A permutation defines how cell move. For example, the swap
            permutation swaps two adjacent cells, the shift permutation moves
            all the cells in a row or column one place in a certain direction.
            You execute permutations by swiping a cell in a certain direction.
          </StyledText>
        </Section>
        <Section headerText="wrap">
          <StyledText style={style.text}>
            Turning on wrapping makes it possible for a permutation to move
            cells past the edge of the field, after which they appear on the
            other side of the field. Some permutations such as the shift
            permutation can only be played with swapping turned on.
          </StyledText>
        </Section>
      </ScrollView>
    </Screen>
  );
};

export default React.memo(HowToPlay);
