import React from "react";
import { View } from "react-native";
import useSectionStyle from "./SectionStyle";
import StyledText from "../../general/StyledText/StyledText";

const Section = ({ children, headerText }) => {
  const style = useSectionStyle();
  return (
    <View style={style.root}>
      <StyledText style={style.sectionHeader}>{headerText}</StyledText>
      {children}
    </View>
  );
};

export default Section;
