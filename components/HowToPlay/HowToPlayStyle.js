import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xlFont, smFont, accentColor, xl, md } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "flex-start",
      alignItems: "stretch",
      padding: 0,
    },
    scrollView: {
      paddingHorizontal: xl,
    },
    header: {
      fontSize: xlFont,
      alignSelf: "center",
    },
    tipContainer: {
      flexDirection: "row",
    },
    tip: {
      fontSize: smFont,
      fontStyle: "italic",
    },
    buttonTextStyle: {
      color: accentColor,
      fontSize: smFont,
      fontStyle: "italic",
    },
    text: {
      fontSize: smFont,
    },
  });
}

export default useStyle;
