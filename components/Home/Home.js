import React, { useContext, useState, useEffect } from "react";
import { LayoutAnimation, View } from "react-native";
import Logo from "../general/Logo/Logo";
import StyledButton from "../general/StyledButton/StyledButton";
import useHomeStyle from "./HomeStyle";
import { UserContext } from "../general/UserHook/UserHook";
import Help from "../../assets/icons/help.svg";
import Screen from "../general/Screen/Screen";
import ConsentModal from "../general/ConsentModal/ConsentModal";

const Home = ({ navigation }) => {
  const style = useHomeStyle();
  const { userJson } = useContext(UserContext);
  const [showingPlayPicker, setShowingPlayPicker] = useState(false);
  const toggleShowingPlayPicker = () => {
    LayoutAnimation.configureNext({
      duration: 500,
      create: { property: "opacity", type: "easeOut" },
      update: { type: "easeOut" },
    });
    setShowingPlayPicker((prevValue) => !prevValue);
  };

  return (
    <Screen style={style.root}>
      <ConsentModal />
      <StyledButton
        Icon={Help}
        containerStyle={style.about}
        onPress={() => navigation.navigate("About")}
        width={30}
        height={30}
      />
      <Logo />
      <View style={style.playContainer}>
        <StyledButton
          text="play"
          onPress={toggleShowingPlayPicker}
          textStyle={style.emphasisButton}
        />
        {showingPlayPicker && (
          <View style={style.playPickerContainer}>
            <StyledButton
              text="&bull; simple"
              onPress={() => navigation.navigate("GameSetup")}
              textStyle={style.play}
            />
            <StyledButton
              text="&bull; browse"
              onPress={() => navigation.navigate("Browse")}
              textStyle={style.play}
            />
            <StyledButton
              text="&bull; custom"
              onPress={() => navigation.navigate("Custom")}
              textStyle={style.play}
              disabled={!(userJson && userJson.premium)}
            />
          </View>
        )}
      </View>
      <StyledButton
        text="settings"
        onPress={() => navigation.navigate("Settings")}
        textStyle={style.button}
      />
      <StyledButton
        text="highscores"
        textStyle={style.button}
        onPress={() => navigation.navigate("Highscores")}
      />
      {userJson && !userJson.isAnonymous ? (
        <StyledButton
          text="account"
          textStyle={style.button}
          onPress={() => navigation.navigate("Account")}
        />
      ) : (
        <StyledButton
          text="sign in"
          onPress={() => navigation.navigate("SignIn")}
          textStyle={style.button}
        />
      )}
      {!(userJson && userJson.premium) && (
        <StyledButton
          text="upgrade"
          textStyle={style.button}
          onPress={() => navigation.navigate("Upgrade")}
        />
      )}
      <StyledButton
        text="how to play"
        textStyle={style.button}
        onPress={() => navigation.navigate("HowToPlay")}
      />
    </Screen>
  );
};

export default Home;
