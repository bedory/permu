import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";
import Constants from "expo-constants";

function useStyle() {
  const {
    mdFont,
    lgFont,
    xlFont,
    xxlFont,
    md,
    disabledOpacity,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    about: {
      position: "absolute",
      right: md,
      top: md + Constants.statusBarHeight,
      opacity: disabledOpacity,
    },
    button: {
      fontSize: xlFont,
    },
    playContainer: {
      alignItems: "center",
    },
    emphasisButton: {
      fontSize: xxlFont,
    },
    playPickerContainer: {
      marginTop: md,
      marginBottom: -md,
    },
    play: {
      fontSize: lgFont,
    },
  });
}
export default useStyle;
