import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { windowHeight, xl, xlFont } = useStyleVariables();
  return StyleSheet.create({
    root: {
      padding: 0,
      alignItems: "stretch",
    },
    container: {
      minHeight: Math.max(windowHeight - 2 * xl, 200),
      display: "flex",
      alignItems: "center",
      justifyContent: "space-evenly",
      paddingVertical: xl,
    },
    buttonTextStyle: {
      fontSize: xlFont,
    },
  });
}

export default useStyle;
