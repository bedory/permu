import React, { useEffect} from "react";
import { ScrollView } from "react-native";
import GameSettings from "../general/GameSettings/GameSettings";
import StyledButton from "../general/StyledButton/StyledButton";
import permutationFamilyMap from "../general/Field/PermutationFamilies";

import FieldPreview from "../general/Field/FieldPreview";
import useFieldWithPermutationPreview, {
  FieldWithPermutationPreviewContext,
} from "../general/Field/FieldWithPermutationPreviewHook";
import useGameSetupStyle from "./GameSetupStyle";
import Screen from "../general/Screen/Screen";
import {printObject} from "../general/randomFunctions";

const GameSetup = ({ navigation, route }) => {
  const style = useGameSetupStyle();
  const imageObject =
    route.params && route.params.imageObject
      ? route.params.imageObject
      : undefined;

  const fieldWithPermutation = useFieldWithPermutationPreview(
    4,
    4,
    permutationFamilyMap.swap,
    true
  );
  const { field, permutationFamily, wrap } = fieldWithPermutation;

  return (
    <FieldWithPermutationPreviewContext.Provider value={fieldWithPermutation}>
      <Screen style={style.root}>
        <ScrollView contentContainerStyle={style.container}>
          <FieldPreview imageObject={imageObject} />
          <GameSettings style={style.settings} />
          <StyledButton
            text="ready"
            textStyle={style.buttonTextStyle}
            onPress={() => {
              navigation.navigate("Game", {
                numberRows: field.numberRows,
                numberColumns: field.numberColumns,
                permutationFamily: permutationFamily.permutationFamily,
                wrap,
                imageObject,
              });
            }}
          />
        </ScrollView>
      </Screen>
    </FieldWithPermutationPreviewContext.Provider>
  );
};

export default GameSetup;
