import React, { useState, useEffect, useCallback } from "react";
import { View, FlatList } from "react-native";
import useHighscoresStyle from "./HighscoresStyle";
import useFieldWithPermutationPreview, {
  FieldWithPermutationPreviewContext,
} from "../general/Field/FieldWithPermutationPreviewHook";
import permutationFamilyMap from "../general/Field/PermutationFamilies";
import firestore from "@react-native-firebase/firestore";
import HighscoreEntry from "./HighscoreEntry/HighscoreEntry";
import Header from "./Header/Header";
import { settingsToId } from "../general/Field/FieldWithPermutationFunctions";
import StyledText from "../general/StyledText/StyledText";
import LoadingIndicator from "../general/LoadingIndicator/LoadingIndicator";
import Screen from "../general/Screen/Screen";

const Highscores = () => {
  const style = useHighscoresStyle();
  const fieldWithPermutation = useFieldWithPermutationPreview(
    4,
    4,
    permutationFamilyMap.swap,
    true
  );
  const { field, permutationFamily, wrap } = fieldWithPermutation;
  const { numberRows, numberColumns } = field;
  const [type, setType] = useState("time");
  const [usernameShowing, setUsernameShowing] = useState();
  const [userIdShowing, setUserIdShowing] = useState(null);
  const [highscoreSnapshots, setHighscoreSnapshots] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [fetching, setFetching] = useState(false);
  const highscoresCollectionRef = firestore().collection("highscores");
  const [noResultsMessage, setNoResultsMessage] = useState("");
  const [reachedEnd, setReachedEnd] = useState(false);
  const numberHighscoresToFetchEachTime = 20;

  const fetchNextHighscores = (overridePrevious = false) => {
    if (fetching) {
      return undefined;
    }

    setFetching(true);
    const collectionRefBasedOnGameSettings = highscoresCollectionRef
      .doc(
        settingsToId(
          numberRows,
          numberColumns,
          permutationFamily.permutationFamily,
          wrap
        )
      )
      .collection("plays");

    let queryFilteredByUser;
    if (userIdShowing) {
      queryFilteredByUser = collectionRefBasedOnGameSettings.where(
        "userId",
        "==",
        userIdShowing
      );
    } else {
      queryFilteredByUser = collectionRefBasedOnGameSettings;
    }

    let queryOrderedBasedOnType = queryFilteredByUser.orderBy(type, "asc");

    let queryStartingFromLast;
    if (highscoreSnapshots.length > 0 && !overridePrevious) {
      queryStartingFromLast = queryOrderedBasedOnType.startAfter(
        highscoreSnapshots[highscoreSnapshots.length - 1]
      );
    } else {
      queryStartingFromLast = queryOrderedBasedOnType;
    }

    queryStartingFromLast
      .limit(numberHighscoresToFetchEachTime)
      .get()
      .then((query) => {
        if (overridePrevious) {
          setHighscoreSnapshots(query.docs);
        } else {
          setHighscoreSnapshots((prevArray) => {
            return prevArray.concat(query.docs);
          });
        }
        if (query.size === 0) {
          setReachedEnd(true);
          setNoResultsMessage("No results found.");
        }
      })
      .catch((e) => {
        setNoResultsMessage(
          "oops, something went wrong. Please try again later."
        );
        console.log(e);
      })
      .finally(() => {
        setFetching(false);
        setRefreshing(false);
      });
  };

  const setShowingUser = useCallback((username, userId) => {
    setUsernameShowing(username);
    setUserIdShowing(userId);
    setRefreshing(true);
  }, []);

  const reset = () => {
    setRefreshing(true);
    setReachedEnd(false);
    fetchNextHighscores(true);
  };

  useEffect(() => {
    reset();
  }, [
    numberRows,
    numberColumns,
    wrap,
    type,
    permutationFamily.permutationFamily,
    userIdShowing,
  ]);

  return (
    <FieldWithPermutationPreviewContext.Provider value={fieldWithPermutation}>
      <Screen style={style.root}>
        <FlatList
          ListHeaderComponent={
            <Header
              type={type}
              setType={setType}
              userIdShowing={userIdShowing}
              usernameShowing={usernameShowing}
              setShowingUser={setShowingUser}
              refreshing={refreshing}
            />
          }
          contentContainerStyle={style.results}
          data={refreshing ? [] : highscoreSnapshots}
          renderItem={({ item, index }) => (
            <HighscoreEntry
              highscore={item.data()}
              type={type}
              userIdShowing={userIdShowing}
              setShowingUser={setShowingUser}
              rank={index + 1}
            />
          )}
          keyExtractor={(highscoreSnapshot) => highscoreSnapshot.id}
          ListEmptyComponent={
            <StyledText style={style.noResultMessage}>
              {noResultsMessage}
            </StyledText>
          }
          onEndReachedThreshold={0.2}
          onEndReached={() => !reachedEnd && fetchNextHighscores(false)}
          ItemSeparatorComponent={() => <View style={style.separator} />}
          initialNumToRender={20}
          onRefresh={reset}
          refreshing={refreshing}
          ListFooterComponent={<LoadingIndicator animating={fetching} />}
        />
      </Screen>
    </FieldWithPermutationPreviewContext.Provider>
  );
};

export default Highscores;
