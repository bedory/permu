import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    windowWidth,
    fontFamily,
    textColor,
    smFont,
    md,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      padding: 0,
    },
    results: {
      justifyContent: "flex-start",
      alignItems: "center",
      width: windowWidth,
    },
    separator: {
      height: 20,
    },
    noResultMessage: {
      fontFamily,
      color: textColor,
      fontSize: smFont,
      marginVertical: md,
      width: 0.75 * windowWidth,
      textAlign: "center",
    },
  });
}

export default useStyle;
