import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import useHeaderStyle from "./HeaderStyle";
import GameSettings from "../../general/GameSettings/GameSettings";
import { UserContext } from "../../general/UserHook/UserHook";
import StyledButton from "../../general/StyledButton/StyledButton";
import Clear from "../../../assets/icons/clear.svg";
import StyledPicker from "../../general/StyledPicker/StyledPicker";
import PickerItem from "../../general/StyledPicker/PickerItem/PickerItem";
import StyledText from "../../general/StyledText/StyledText";
import Separator from "../../general/Separator/Separator";

const Header = ({
  type,
  setType,
  userIdShowing,
  usernameShowing,
  setShowingUser,
  refreshing,
}) => {
  const { userJson } = useContext(UserContext);
  const showingUser = userJson && userJson.uid === userIdShowing;
  const style = useHeaderStyle(!showingUser);
  return (
    <View style={style.root}>
      <View style={style.settingsContainer}>
        <GameSettings forcePremium={true} />
        <StyledPicker
          buttonContainerStyle={style.pickerButton}
          buttonTextStyle={style.pickerButtonText}
          selectedValue={type}
          onValueChange={setType}
        >
          <PickerItem label={"time"} value={"time"} />
          <PickerItem label={"number permutations"} value={"numberSteps"} />
        </StyledPicker>
        {userJson && (
          <StyledPicker
            buttonContainerStyle={style.pickerButton}
            buttonTextStyle={style.pickerButtonText}
            selectedValue={userIdShowing}
            onValueChange={(value) => {
              setShowingUser(null, value);
            }}
          >
            <PickerItem
              key={"global"}
              label={"global highscore"}
              value={null}
            />
            <PickerItem
              key={"personal"}
              label={"personal highscores"}
              value={userJson.uid}
            />
          </StyledPicker>
        )}
      </View>
      <Separator />
      {!showingUser && userIdShowing && !refreshing && (
        <View style={style.viewingContainer}>
          <StyledText style={style.viewingText}>
            Viewing highscores of {usernameShowing}
          </StyledText>
          <StyledButton
            Icon={Clear}
            width={30}
            height={30}
            onPress={() => {
              setShowingUser(null, null);
            }}
          />
        </View>
      )}
      <View style={style.columnLabels}>
        <StyledText
          style={StyleSheet.compose(style.columnLabel, style.rankLabel)}
        >
          rank
        </StyledText>
        {!showingUser && (
          <StyledText
            style={StyleSheet.compose(style.columnLabel, style.usernameLabel)}
          >
            username
          </StyledText>
        )}
        {type === "time" ? (
          <StyledText
            style={StyleSheet.compose(style.columnLabel, style.timeLabel)}
          >
            time
          </StyledText>
        ) : (
          <StyledText
            style={StyleSheet.compose(style.columnLabel, style.numPermLabel)}
          >
            steps
          </StyledText>
        )}
      </View>
    </View>
  );
};

export default Header;
