import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle(showingUsername) {
  const {
    smFont,
    mdFont,
    contrastBackgroundColor,
    sm,
    md,
    windowWidth,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      alignItems: "center",
    },
    settingsContainer: {
      width: 0.5 * windowWidth,
      margin: md,
    },
    pickerButton: {
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: contrastBackgroundColor,
      borderRadius: 10,
      padding: sm,
      margin: sm,
    },
    pickerButtonText: {
      textAlign: "center",
      margin: sm,
      lineHeight: mdFont + 2,
    },
    columnLabels: {
      width: 0.95 * windowWidth,
      flexDirection: "row",
      justifyContent: showingUsername ? "space-between" : "space-evenly",
    },
    columnLabel: {
      textAlign: "center",
    },
    rankLabel: {
      width: 60,
      flex: 0,
    },
    usernameLabel: {
      flex: 1,
    },
    timeLabel: {
      width: 120,
      flex: 0,
    },
    numPermLabel: {
      width: 65,
      flex: 0,
    },
    viewingContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      alignSelf: "stretch",
      margin: md,
    },
    viewingBackButtonText: {
      fontSize: smFont,
    },
    viewingText: {
      fontSize: smFont,
    },
  });
}

export default useStyle;
