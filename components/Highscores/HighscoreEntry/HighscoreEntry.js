import React, { useCallback, useContext } from "react";
import { View, StyleSheet } from "react-native";
import useHighscoreEntryStyle from "./HighscoreEntryStyle";
import { formatTime } from "../../general/randomFunctions";
import StyledButton from "../../general/StyledButton/StyledButton";
import { UserContext } from "../../general/UserHook/UserHook";
import StyledText from "../../general/StyledText/StyledText";

const HighscoreEntry = ({
  highscore,
  type,
  rank,
  userIdShowing,
  setShowingUser,
}) => {
  const { username, time, numberSteps, userId } = highscore;
  const { userJson } = useContext(UserContext);
  const showUsername =
    username && !(userJson && userIdShowing === userJson.uid);
  const style = useHighscoreEntryStyle(showUsername);
  const maxCharactersUsername = 15;

  const setUserShowingCallback = useCallback(() => {
    setShowingUser(username, userId);
  }, [username, userId]);

  return (
    <View style={style.root}>
      <StyledText style={StyleSheet.compose(style.text, style.rank)}>
        {rank}.
      </StyledText>
      {showUsername && (
        <StyledButton
          text={
            username.length <= maxCharactersUsername
              ? username
              : username.substring(0, maxCharactersUsername - 3) + "..."
          }
          contianerStyle={style.usernameContainer}
          textStyle={StyleSheet.compose(style.text, style.username)}
          onPress={setUserShowingCallback}
        />
      )}
      <StyledText
        style={StyleSheet.compose(
          style.text,
          type === "time" ? style.time : style.numPerm
        )}
      >
        {type === "time" ? formatTime(time) : numberSteps}
      </StyledText>
    </View>
  );
};

export default React.memo(HighscoreEntry);
