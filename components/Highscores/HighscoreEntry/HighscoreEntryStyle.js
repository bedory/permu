import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle(usernameExists) {
  const {
    windowWidth,
    fontFamily,
    mdFont,
    textColor,
    accentColor,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      flexDirection: "row",
      justifyContent: usernameExists ? "space-between" : "space-evenly",
      alignItems: "center",
      width: 0.95 * windowWidth,
    },
    text: {
      fontFamily,
      fontSize: mdFont,
      color: textColor,
      textAlign: "center",
    },
    rank: {
      width: 60,
      flex: 0,
    },
    usernameContainer: {
      flex: 1,
    },
    username: {
      color: accentColor,
    },
    time: {
      width: 120,
      flex: 0,
    },
    numPerm: {
      width: 65,
      flex: 0,
    },
  });
}

export default useStyle;
