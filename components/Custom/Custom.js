import React, { useState, useEffect, useCallback } from "react";
import { Image, View, useWindowDimensions } from "react-native";
import useCustomStyle from "./CustomStyle";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import StyledButton from "../general/StyledButton/StyledButton";
import Screen from "../general/Screen/Screen";

const Custom = ({ navigation }) => {
  const style = useCustomStyle();
  const [imageObject, setImageObject] = useState();
  const windowWidth = useWindowDimensions().width;

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  const pickImage = useCallback(async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        quality: 0.5,
        allowsMultipleSelection: false,
        base64: false,
        exif: false,
      });
      if (!result.cancelled) {
        setImageObject(result);
      }
    } catch (E) {
      console.log(E);
    }
  }, []);

  const takePicture = useCallback(async () => {
    try {
      let result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        quality: 0.5,
        base64: false,
        exif: false,
      });
      if (!result.cancelled) {
        setImageObject(result);
      }
    } catch (E) {
      console.log(E);
    }
  }, []);

  useEffect(() => {
    getPermissionAsync();
  }, []);

  let width = 200;
  let height = 200;
  if (imageObject) {
    if (imageObject.width >= imageObject.height) {
      width = windowWidth * 0.9;
      height = (width * imageObject.height) / imageObject.width;
    } else {
      height = windowWidth * 0.9;
      width = (height * imageObject.width) / imageObject.height;
    }
  }

  return (
    <Screen style={style.root}>
      <View style={style.buttonsContainer}>
        <StyledButton
          text="pick an image"
          textStyle={style.buttonText}
          onPress={pickImage}
        />
        <StyledButton
          text="take a picture"
          textStyle={style.buttonText}
          onPress={takePicture}
        />
      </View>
      {imageObject && (
        <>
          <View
            style={[
              style.imageContainer,
              { width: windowWidth * 0.9, height: windowWidth * 0.9 },
            ]}
          >
            <Image source={imageObject} style={{ width, height }} />
          </View>
          <StyledButton
            text="confirm"
            textStyle={style.buttonText}
            onPress={() =>
              navigation.navigate("GameSetup", {
                imageObject: {
                  width: imageObject.width,
                  height: imageObject.height,
                  url: imageObject.uri,
                },
              })
            }
          />
        </>
      )}
    </Screen>
  );
};

export default Custom;
