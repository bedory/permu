import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xlFont } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    buttonsContainer: {
      height: 150,
      justifyContent: "space-evenly",
    },
    buttonText: {
      fontSize: xlFont,
    },
    imageContainer: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  });
}

export default useStyle;
