import React, { useCallback, useState, useContext, useEffect } from "react";
import { View, Modal, InteractionManager } from "react-native";
import useBannerStyle from "./BannerStyle";
import GameStats from "../GameStats/GameStats";
import {
  FieldContext,
  FieldWithPermutationContext,
} from "../../general/Field/FieldWithPermutationHook";
import StyledButton from "../../general/StyledButton/StyledButton";
import { AnimationContext } from "../../general/AnimationHook/AnimationHook";
import SaveHighscoreButton from "./SaveHighscoreButton/SaveHighscoreButton";
import { BlurView } from "@react-native-community/blur";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";
import { useNavigation, useFocusEffect } from "@react-navigation/native";

const Banner = () => {
  const style = useBannerStyle();
  const { mode, backgroundColor } = useStyleVariables();
  const { scramble } = useContext(FieldWithPermutationContext);
  const { animateNext } = useContext(AnimationContext);
  const { finished, numberRows, numberColumns } = useContext(FieldContext);
  const [showing, setShowing] = useState(false);
  const [started, setStarted] = useState(false);
  const navigation = useNavigation();

  const start = useCallback(() => {
    setShowing(false);
    animateNext();
    scramble(Math.ceil((numberColumns + numberRows) / 2) * 20);
    InteractionManager.runAfterInteractions(() => {
      setStarted(true);
    });
  }, [numberColumns, numberRows, scramble]);

  useEffect(() => {
    let task;
    if (finished && started) {
      task = InteractionManager.runAfterInteractions(() => {
        setShowing(true);
      });
    }

    return () => task && task.cancel();
  }, [finished]);

  useFocusEffect(
    useCallback(() => {
      setShowing(true);
      return () => setShowing(false);
    }, [])
  );

  return (
    <Modal
      visible={showing}
      transparent={true}
      animationType="fade"
      onRequestClose={() => navigation.goBack()}
    >
      <View style={style.root}>
        <BlurView
          style={style.blurView}
          blurType={mode}
          blurAmount={8}
          reducedTransparencyFallbackColor={backgroundColor}
        />
        <View style={style.startBanner}>
          <StyledButton
            text={!started ? "start" : "play again"}
            textStyle={style.bannerButton}
            onPress={start}
          />
          {started && <GameStats />}
          {started && <SaveHighscoreButton />}
        </View>
      </View>
    </Modal>
  );
};

export default Banner;
