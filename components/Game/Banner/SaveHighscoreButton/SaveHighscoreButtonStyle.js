import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { correctColor, errorColor, accentColor, smFont } = useStyleVariables();
  return StyleSheet.create({
    root: {
      height: 50,
    },
    signInText: {},
    successText: {
      color: correctColor,
    },
    saveText: {
      color: accentColor,
    },
    errorContainer: {
      alignItems: "center",
      justifyContent: "center",
    },
    errorText: {
      color: errorColor,
      fontSize: smFont,
    },
  });
}

export default useStyle;
