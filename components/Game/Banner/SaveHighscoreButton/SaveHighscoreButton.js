import React, { useContext, useState } from "react";
import { View } from "react-native";
import useSaveHighscoreButtonStyle from "./SaveHighscoreButtonStyle";
import StyledButton from "../../../general/StyledButton/StyledButton";
import firestore from "@react-native-firebase/firestore";
import {
  FieldContext,
  FieldWithPermutationContext,
  PermutationFamilyContext,
  TimeContext,
} from "../../../general/Field/FieldWithPermutationHook";
import { UserContext } from "../../../general/UserHook/UserHook";
import { settingsToId } from "../../../general/Field/FieldWithPermutationFunctions";
import StyledText from "../../../general/StyledText/StyledText";
import CheckSignedInContainer from "../../../general/CheckSignedInContainer/CheckSignedInContainer";
import LoadingIndicatorContainer from "../../../general/LoadingIndicatorContainer/LoadingIndicatorContainer";

const States = {
  NOT_YET_SAVED: 0,
  SAVING: 1,
  SAVED: 2,
  ERROR_WHILE_SAVING: 3,
};

const SaveHighscoreButton = () => {
  const style = useSaveHighscoreButtonStyle();
  const highscoresCollectionRef = firestore().collection("highscores");
  const { numberRows, numberColumns, numberSteps } = useContext(FieldContext);
  const { permutationFamily } = useContext(PermutationFamilyContext);
  const time = useContext(TimeContext);
  const { wrap, gameInfo } = useContext(FieldWithPermutationContext);
  const { userJson } = useContext(UserContext);
  const [state, setState] = useState(States.NOT_YET_SAVED);

  const saveHighscore = async () => {
    setState(States.SAVING);
    try {
      const docRef = await highscoresCollectionRef
        .doc(settingsToId(numberRows, numberColumns, permutationFamily, wrap))
        .collection("plays")
        .add({
          time,
          numberSteps,
          username: userJson.displayName,
          userId: userJson.uid,
        });
      await docRef.collection("extraInfo").doc("playDetails").set(gameInfo);
      setState(States.SAVED);
    } catch (e) {
      setState(States.ERROR_WHILE_SAVING);
      console.log(e);
    }
  };

  const Inner = () => {
    if (state === States.NOT_YET_SAVED) {
      return (
        <StyledButton
          text="save score"
          onPress={saveHighscore}
          textStyle={style.saveText}
        />
      );
    } else if (state === States.SAVED) {
      return <StyledText style={style.successText}>saved</StyledText>;
    } else if (state === States.ERROR_WHILE_SAVING) {
      return (
        <View style={style.errorContainer}>
          <StyledText style={style.errorText}>Error while saving.</StyledText>
          <StyledButton
            text="try again"
            onPress={saveHighscore}
            textStyle={style.saveText}
          />
        </View>
      );
    } else {
      return null;
    }
  };

  return (
    <LoadingIndicatorContainer
      style={style.root}
      loading={state === States.SAVING}
    >
      <CheckSignedInContainer
        textStyle={style.signInText}
        buttonTextStyle={style.signInButtonText}
        signInToText="to save score."
      >
        <Inner />
      </CheckSignedInContainer>
    </LoadingIndicatorContainer>
  );
};

export default SaveHighscoreButton;
