import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { backgroundColor, xxlFont, fontFamily, lg } = useStyleVariables();
  return StyleSheet.create({
    blurView: {
      ...StyleSheet.absoluteFill,
    },
    root: {
      ...StyleSheet.absoluteFill,
      justifyContent: "center",
      alignItems: "stretch",
      zIndex: 1000,
    },
    startBanner: {
      alignItems: "center",
      justifyContent: "center",
      zIndex: 1001,
      paddingVertical: lg,
    },
    bannerButton: {
      fontSize: xxlFont,
      lineHeight: xxlFont + 6,
      fontFamily: fontFamily,
    },
    background: {
      ...StyleSheet.absoluteFill,
      backgroundColor,
      opacity: 0.6,
    },
  });
}

export default useStyle;
