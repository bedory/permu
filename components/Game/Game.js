import React, { useState, useCallback, useEffect, useContext } from "react";
import { View } from "react-native";
import useGameStyle from "./GameStyle";
import useFieldWithPermutation, {
  FieldContext,
  FieldWithPermutationContext,
  PermutationFamilyContext,
  TimeContext,
} from "../general/Field/FieldWithPermutationHook";
import FieldGame from "../general/Field/FieldGame";
import Banner from "./Banner/Banner";
import GameInfo from "./GameInfo/GameInfo";
import Controls from "./Controls/Controls";
import {
  OrientationLock,
  unlockAsync,
  lockAsync,
} from "expo-screen-orientation";
import { useIsFocused } from "@react-navigation/native";
import { SettingsContext } from "../general/SettingsHook/SettingsHook";
import { BannerAd, TestIds, BannerAdSize } from "@react-native-firebase/admob";
import { UserContext } from "../general/UserHook/UserHook";
import Screen from "../general/Screen/Screen";

const Game = ({ route }) => {
  const style = useGameStyle();
  const params = route.params;
  const isFocused = useIsFocused();
  const { userJson } = useContext(UserContext);
  const { settings, adConsentGiven, inEeaOrUnknown } = useContext(
    SettingsContext
  );
  const {
    enableUndoRedo,
    maxNumberUndo,
    stopwatchPrecision,
    showStopwatch,
    personalizedAds,
  } = settings;

  useEffect(() => {
    if (
      isFocused &&
      params.imageObject &&
      params.imageObject.width > params.imageObject.height
    ) {
      unlockAsync();
    } else {
      lockAsync(OrientationLock.PORTRAIT);
    }
    return () => {
      lockAsync(OrientationLock.PORTRAIT);
    };
  }, [isFocused]);

  const numberUndos = enableUndoRedo ? maxNumberUndo : 0;
  const fieldWithPermutationFamily = useFieldWithPermutation(
    params.numberRows,
    params.numberColumns,
    params.permutationFamily,
    params.wrap,
    numberUndos,
    showStopwatch ? stopwatchPrecision : 0
  );
  const {
    field,
    fieldWithPermutation,
    permutationFamily,
    time,
  } = fieldWithPermutationFamily;
  const [showingEndResult, setShowingEndResult] = useState(false);
  const [highlightThisGoesToMode, setHighlightThisGoesToMode] = useState(false);
  const toggleHighlightThisGoesToMode = useCallback(() => {
    setHighlightThisGoesToMode((prevState) => !prevState);
    turnHighlightGoesToThisModeOff();
  }, []);
  const turnHighlightThisGoesToModeOff = useCallback(
    () => setHighlightThisGoesToMode(false),
    []
  );

  const [highlightGoesToThisMode, setHighlightGoesToThisMode] = useState(false);
  const toggleHighlightGoesToThisMode = useCallback(() => {
    setHighlightGoesToThisMode((prevState) => !prevState);
    turnHighlightThisGoesToModeOff();
  }, []);
  const turnHighlightGoesToThisModeOff = useCallback(
    () => setHighlightGoesToThisMode(false),
    []
  );

  const [topBannerSize, setTopBannerSize] = useState();
  const computeBannerSize = (event) => {
    const height = event.nativeEvent.layout.height;
    if (height >= 250) {
      return BannerAdSize.MEDIUM_RECTANGLE;
    } else if (height >= 100) {
      return BannerAdSize.LARGE_BANNER;
    } else if (height >= 50) {
      return BannerAdSize.BANNER;
    } else {
      return null;
    }
  };
  const computeTopBannerSize = (event) => {
    setTopBannerSize(computeBannerSize(event));
  };

  return (
    <PermutationFamilyContext.Provider value={permutationFamily}>
      <FieldContext.Provider value={field}>
        <FieldWithPermutationContext.Provider value={fieldWithPermutation}>
          <TimeContext.Provider value={time}>
            <Screen style={style.root}>
              <Banner />
              <View
                style={style.topBannerContainer}
                onLayout={computeTopBannerSize}
              >
                {topBannerSize &&
                  !(userJson && userJson.premium) &&
                  (adConsentGiven || !inEeaOrUnknown) && (
                    <BannerAd
                      unitId="ca-app-pub-4802298437271917/8577756260"
                      size={topBannerSize}
                      requestOptions={{
                        requestNonPersonalizedAdsOnly: !personalizedAds,
                      }}
                    />
                  )}
              </View>
              <View style={style.gameContainer}>
                <GameInfo />
                <FieldGame
                  showEndResult={showingEndResult}
                  highlightThisGoesToMode={highlightThisGoesToMode}
                  highlightGoesToThisMode={highlightGoesToThisMode}
                  imageObject={params.imageObject}
                  turnHighlightThisGoesToModeOff={
                    turnHighlightThisGoesToModeOff
                  }
                  turnHighlightGoesToThisModeOff={
                    turnHighlightGoesToThisModeOff
                  }
                />
                <Controls
                  showingEndResult={showingEndResult}
                  setShowingEndResult={setShowingEndResult}
                  highlightThisGoesToMode={highlightThisGoesToMode}
                  highlightGoesToThisMode={highlightGoesToThisMode}
                  toggleHighlightThisGoesToMode={toggleHighlightThisGoesToMode}
                  toggleHighlightGoesToThisMode={toggleHighlightGoesToThisMode}
                />
              </View>
              <View style={style.bottomBannerContainer} />
            </Screen>
          </TimeContext.Provider>
        </FieldWithPermutationContext.Provider>
      </FieldContext.Provider>
    </PermutationFamilyContext.Provider>
  );
};

export default Game;
