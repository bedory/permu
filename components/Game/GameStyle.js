import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { md, landscape } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "space-between",
      padding: 0,
    },
    gameContainer: {
      flex: 0,
      flexDirection: landscape ? "row" : "column",
      justifyContent: "center",
      alignItems: "center",
      padding: landscape ? null : md,
    },
    topBannerContainer: {
      flex: 1,
      justifyContent: "flex-start",
      alignSelf: "stretch",
      alignItems: "center",
    },
    bottomBannerContainer: {
      flex: 1,
      justifyContent: "flex-end",
      alignSelf: "stretch",
      alignItems: "center",
    },
  });
}

export default useStyle;
