import React, { useContext } from "react";
import { View } from "react-native";
import useGameStatsStyle from "./GameStatsStyle";
import Time from "../../general/Time/Time";
import {
  FieldContext,
  TimeContext,
} from "../../general/Field/FieldWithPermutationHook";
import { stringToTextLetters } from "../../general/randomFunctions";
import { SettingsContext } from "../../general/SettingsHook/SettingsHook";
import StyledText from "../../general/StyledText/StyledText";

const GameStats = ({ showLandscape = false }) => {
  const style = useGameStatsStyle(showLandscape);
  const time = useContext(TimeContext);
  const { numberSteps } = useContext(FieldContext);
  const { settings } = useContext(SettingsContext);
  const { showNumberPermutations, showStopwatch } = settings;
  return (
    <>
      {showStopwatch && (
        <View style={style.timeContainer}>
          {showLandscape ? (
            stringToTextLetters("time ", style.label)
          ) : (
            <StyledText style={style.label}>time: </StyledText>
          )}
          <Time duration={time} vertical={showLandscape} />
        </View>
      )}
      {showNumberPermutations && (
        <View style={style.stepsContainer}>
          {showLandscape ? (
            stringToTextLetters("steps " + numberSteps, style.label)
          ) : (
            <>
              <StyledText style={style.label}>steps: </StyledText>
              <StyledText style={style.numberSteps}>{numberSteps}</StyledText>
            </>
          )}
        </View>
      )}
    </>
  );
};

export default React.memo(GameStats, () => false);
