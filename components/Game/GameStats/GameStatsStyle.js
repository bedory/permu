import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle(showLandscape) {
  const {
    smFont,
    landscape,
  } = useStyleVariables();
  return StyleSheet.create({
    timeContainer: {
      flexDirection: showLandscape ? "column" : "row",
      alignItems: "center",
      width: landscape ? null : 160,
      justifyContent: landscape ? "flex-start" : "space-between",
    },
    stepsContainer: {
      flexDirection: showLandscape ? "column" : "row",
      alignItems: "center",
      width: landscape ? null : 100,
      justifyContent: showLandscape ? "flex-start" : "space-between",
    },
    label: {
      fontSize: smFont,
      lineHeight: showLandscape ? smFont + 2 : null,
    },
    numberSteps: {
      width: showLandscape ? null : 45,
      textAlign: "center",
    },
  });
}

export default useStyle;
