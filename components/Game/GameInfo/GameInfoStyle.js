import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    md,
    landscape,
    showNumberPermutations,
    showStopwatch,
  } = useStyleVariables();
  const root = {
    flexDirection: "row",
    justifyContent:
      showNumberPermutations || showStopwatch ? "space-between" : "flex-end",
    alignSelf: "stretch",
    height: 50,
    paddingBottom: md,
  };
  const landscapeRoot = {
    alignSelf: "stretch",
    alignItems: "center",
  };
  return StyleSheet.create({
    root: landscape ? landscapeRoot : root,
    gameStatsContainer: {
      flexDirection: "row",
      justifyContent: "space-evenly",
      alignSelf: "stretch",
      width: 50,
      marginTop: md,
    },
  });
}

export default useStyle;
