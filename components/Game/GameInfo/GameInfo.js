import React, { useContext, useCallback } from "react";
import { View, Alert } from "react-native";
import useGameInfoStyle from "./GameInfoStyle";
import GameStats from "../GameStats/GameStats";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";
import StyledButton from "../../general/StyledButton/StyledButton";
import Reset from "../../../assets/icons/reset.svg";
import {
  FieldContext,
  FieldWithPermutationContext,
} from "../../general/Field/FieldWithPermutationHook";
import { AnimationContext } from "../../general/AnimationHook/AnimationHook";

const GameInfo = () => {
  const style = useGameInfoStyle();
  const { landscape } = useStyleVariables();
  const { numberRows, numberColumns } = useContext(FieldContext);
  const { scramble } = useContext(FieldWithPermutationContext);
  const { animateNext } = useContext(AnimationContext);

  const reset = () => {
    animateNext();
    scramble(Math.ceil((numberColumns + numberRows) / 2) * 20);
  };

  const resetAlert = useCallback(() => {
    Alert.alert(
      "reset field",
      "Are you sure you want to reset the field.",
      [{ text: "cancel" }, { text: "confirm", onPress: reset }],
      { cancelable: true }
    );
  }, []);

  return (
    <View style={style.root}>
      {!landscape && <GameStats showLandscape={landscape} />}
      <StyledButton Icon={Reset} onPress={reset} />
      {landscape && (
        <View style={style.gameStatsContainer}>
          {<GameStats showLandscape={landscape} />}
        </View>
      )}
    </View>
  );
};

export default GameInfo;
