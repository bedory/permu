import React, { useCallback, useContext, useRef } from "react";
import { View } from "react-native";
import useControlsStyle from "./ControlsStyle";
import ToggleButton from "../../general/ToggleButton/ToggleButton";
import Visibility from "../../../assets/icons/visibility.svg";
import StyledButton from "../../general/StyledButton/StyledButton";
import Undo from "../../../assets/icons/undo.svg";
import Redo from "../../../assets/icons/redo.svg";
import HighlightThisGoesTo from "../../../assets/icons/highlightThisGoesTo.svg";
import HighlightGoesToThis from "../../../assets/icons/highlightGoesToThis.svg";
import { FieldContext } from "../../general/Field/FieldWithPermutationHook";
import { AnimationContext } from "../../general/AnimationHook/AnimationHook";
import { SettingsContext } from "../../general/SettingsHook/SettingsHook";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

const Controls = ({
  showingEndResult,
  setShowingEndResult,
  highlightThisGoesToMode,
  toggleHighlightThisGoesToMode,
  highlightGoesToThisMode,
  toggleHighlightGoesToThisMode,
}) => {
  const style = useControlsStyle();
  const { finished, undo, canUndo, redo, canRedo } = useContext(FieldContext);
  const { showAnimations, animationSpeed } = useStyleVariables();
  const { animateNext } = useContext(AnimationContext);
  const { settings } = useContext(SettingsContext);
  const {
    enableHighlightThisGoesToMode,
    enableHighlightGoesToThisMode,
    enableShowFinal,
    enableUndoRedo,
  } = settings;
  const animationStartTime = useRef();

  const startShowingEndResult = useCallback(() => {
    animateNext();
    animationStartTime.current = Date.now();
    setShowingEndResult(true);
  }, []);

  const stopShowingEndResult = useCallback(() => {
    const animateEndResult = () => {
      animateNext();
      setShowingEndResult(false);
    };
    const timeSinceStartAnimation = Date.now() - animationStartTime.current;
    const minimumShowTime = 100;
    const timeBeforeStartAnimationEnds =
      animationSpeed + minimumShowTime - timeSinceStartAnimation;
    if (showAnimations && timeBeforeStartAnimationEnds > 0) {
      setTimeout(animateEndResult, timeBeforeStartAnimationEnds);
    } else {
      animateEndResult();
    }
  }, []);

  const undoCallback = useCallback(() => {
    animateNext();
    undo();
  }, [undo]);

  const redoCallback = useCallback(() => {
    animateNext();
    redo();
  }, [redo]);

  return (
    <View style={style.root}>
      {enableUndoRedo && (
        <StyledButton
          Icon={Undo}
          disabled={!canUndo || finished}
          onPress={undoCallback}
        />
      )}

      {enableHighlightGoesToThisMode && (
        <ToggleButton
          Icon={HighlightGoesToThis}
          onPress={toggleHighlightGoesToThisMode}
          toggled={highlightGoesToThisMode}
          disabled={finished}
        />
      )}

      {enableShowFinal && (
        <ToggleButton
          Icon={Visibility}
          toggled={showingEndResult}
          onPressIn={startShowingEndResult}
          onPressOut={stopShowingEndResult}
          disabled={finished}
        />
      )}

      {enableHighlightThisGoesToMode && (
        <ToggleButton
          Icon={HighlightThisGoesTo}
          onPress={toggleHighlightThisGoesToMode}
          toggled={highlightThisGoesToMode}
          disabled={finished}
        />
      )}

      {enableUndoRedo && (
        <StyledButton
          Icon={Redo}
          disabled={!canRedo || finished}
          onPress={redoCallback}
        />
      )}
    </View>
  );
};

export default Controls;
