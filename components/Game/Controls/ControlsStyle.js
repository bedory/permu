import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { md, landscape } = useStyleVariables();
  return StyleSheet.create({
    root: {
      flexDirection: landscape ? "column" : "row",
      justifyContent: "space-between",
      alignItems: "center",
      alignSelf: "stretch",
      height: landscape ? null : 50,
      width: landscape ? 50 : null,
      paddingTop: landscape ? null : md,
    },
  });
}

export default useStyle;
