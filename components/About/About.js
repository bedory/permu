import React from "react";
import { View, Linking } from "react-native";
import useAboutStyle from "./AboutStyle";
import StyledButton from "../general/StyledButton/StyledButton";
import Logo from "../general/Logo/Logo";
import StyledText from "../general/StyledText/StyledText";
import Screen from "../general/Screen/Screen";

const About = () => {
  const style = useAboutStyle();
  return (
    <Screen style={style.root}>
      <View style={style.container}>
        <Logo />
        <StyledText style={style.text}>
          Permu is created and owned by bedory.
        </StyledText>
        <View style={style.entryContainer}>
          <StyledText style={style.text}>Privacy policy: </StyledText>
          <StyledButton
            text="permu.bedory.com/privacy"
            onPress={() => Linking.openURL("http://permu.bedory.com/privacy")}
            textStyle={style.link}
          />
        </View>
        <View style={style.entryContainer}>
          <StyledText style={style.text}>permu website: </StyledText>
          <StyledButton
            text="permu.bedory.com"
            onPress={() => Linking.openURL("http://permu.bedory.com")}
            textStyle={style.link}
          />
        </View>
        <View style={style.entryContainer}>
          <StyledText style={style.text}>bedory website: </StyledText>
          <StyledButton
            text="bedory.com"
            onPress={() => Linking.openURL("http://bedory.com")}
            textStyle={style.link}
          />
        </View>
        <View style={style.entryContainer}>
          <StyledText style={style.text}>email: </StyledText>
          <StyledButton
            text="info@bedory.com"
            onPress={() =>
              Linking.openURL(
                "mailto:info@bedory.com?subject=permu question&body=Dear permu team,"
              )
            }
            textStyle={style.link}
          />
        </View>
      </View>
    </Screen>
  );
};

export default About;
