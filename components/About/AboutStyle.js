import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont, accentColor } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "center",
    },
    container: {
      height: 300,
      justifyContent: "space-between",
      alignItems: "center",
    },
    entryContainer: {
      flexDirection: "row",
      alignItems: "center",
    },
    text: {
      fontSize: smFont,
    },
    link: {
      fontSize: smFont,
      color: accentColor,
    },
  });
}

export default useStyle;
