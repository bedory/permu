import React, { useContext, useEffect, useState } from "react";
import { Alert, View } from "react-native";
import useUpgradeStyle from "./UpgradeStyle";
import StyledButton from "../general/StyledButton/StyledButton";
import { UserContext } from "../general/UserHook/UserHook";
import Screen from "../general/Screen/Screen";
import StyledText from "../general/StyledText/StyledText";
import CheckSignedInContainer from "../general/CheckSignedInContainer/CheckSignedInContainer";
import { MessageContext, MessageTypes } from "../general/Message/MessageHook";
import Iaphub from "react-native-iaphub";
import RNIap from "react-native-iap";
import LoadingIndicatorContainer from "../general/LoadingIndicatorContainer/LoadingIndicatorContainer";
import ProcessingModal from "./ProcessingModal/ProcessingModal";

const Upgrade = ({ navigation }) => {
  const style = useUpgradeStyle();
  const { userJson, reload } = useContext(UserContext);
  const [initializingPayment, setInitializingPayment] = useState(false);
  const [processingPayment, setProcessingPayment] = useState(false);
  const [premium, setPremium] = useState([]);
  const { setMessageAndShow } = useContext(MessageContext);

  // retrieves the products from google play
  // could also have been done through IAPHU's getProductsForSale()
  // but doing it through google play is faster and doesn't count as user for IAPHUB's pricing model
  useEffect(() => {
    RNIap.getProducts(["premium"])
      .then((products) => {
        setPremium(products.length > 0 && products[0]);
      })
      .catch(console.log);
  }, []);

  const purchasePremium = async () => {
    setInitializingPayment(true);
    try {
      await Iaphub.setUserId(userJson.uid);
      await Iaphub.getProductsForSale();
      let transaction = await Iaphub.buy("premium", {
        onReceiptProcess: () => setProcessingPayment(true),
      });
      /*
       * The purchase has been successful but we need to check that the webhook to our server was successful as well
       * If the webhook request failed, IAPHUB will send you an alert and retry again in 1 minute, 10 minutes, 1 hour and 24 hours.
       * You can retry the webhook directly from the dashboard as well
       */
      if (transaction.webhookStatus === "failed") {
        setMessageAndShow(
          "Your purchase was successful but we need some more time to validate it, should arrive soon! Otherwise contact the support (info@bedory.com)",
          MessageTypes.WARNING
        );
      }
      // Everything was successful! Yay!
      else {
        // TODO: temporarily set user.premium. Once it is confirmed, set definitely
        await reload(true);
        navigation.navigate("Home");
        setMessageAndShow(
          "Your purchase has been processed successfully!",
          MessageTypes.SUCCESS
        );
      }
    } catch (err) {
      // Purchase popup cancelled by the user (ios only)
      if (err.code === "user_cancelled") {
        console.log("user canceled");
      }
      // Couldn't buy product because it has been bought in the past but hasn't been consumed (restore needed)
      else if (err.code === "product_already_owned") {
        Alert.alert(
          "Product already owned",
          "Please restore your purchases in order to fix that issue",
          [
            { text: "Cancel", style: "cancel" },
            {
              text: "Restore",
              onPress: async () => {
                await Iaphub.setUserId(userJson.uid);
                return Iaphub.restore();
              },
            },
          ]
        );
      } else if (err.code === "deferred_payment") {
        // The payment has been deferred (awaiting approval from parental control)
        setMessageAndShow(
          "Your purchase is awaiting approval from the parental control",
          MessageTypes.WARNING
        );
      } else if (err.code === "receipt_validation_failed") {
        /*
         * The receipt has been processed on IAPHUB but something went wrong
         * It is probably because of an issue with the configuration of your app or a call to the Itunes/GooglePlay API that failed
         * IAPHUB will send you an email notification when a receipt fails, by checking the receipt on the dashboard you'll find a detailed report of the error
         * After fixing the issue (if there's any), just click on the 'New report' button in order to process the receipt again
         * If it is an error contacting the Itunes/GooglePlay API, IAPHUB will retry to process the receipt automatically as well
         */
        setMessageAndShow(
          "We're having trouble validating your transaction. Give us some time, we'll retry to validate your transaction ASAP!",
          MessageTypes.WARNING
        );
      } else if (err.code === "receipt_invalid") {
        /*
         * The receipt has been processed on IAPHUB but is invalid
         * It could be a fraud attempt, using apps such as Freedom or Lucky Patcher on an Android rooted device
         */
        setMessageAndShow(
          "We were not able to process your purchase, if you've been charged please contact the support (info@bedory.com)",
          MessageTypes.ERROR
        );
      } else if (err.code === "receipt_request_failed") {
        /*
         * The receipt hasn't been validated on IAPHUB (Could be an issue like a network error...)
         * The user will have to restore its purchases in order to validate the transaction
         * An automatic restore should be triggered on every relaunch of your app since the transaction hasn't been 'finished'
         * Android should automatically refund transactions that are not 'finished' after 3 days
         */
        setMessageAndShow(
          "We're having trouble validating your transaction. Please try again later or contact the support (info@bedory.com)",
          MessageTypes.ERROR
        );
      }
      // Couldn't buy product for many other reasons (the user shouldn't be charged)
      else {
        console.log("error", err);
        setMessageAndShow(
          "We were not able to process your purchase, please try again later or contact the support (info@bedory.com)",
          MessageTypes.ERROR
        );
      }
    }
    setInitializingPayment(false);
    setProcessingPayment(false);
    Iaphub.setUserId(null);
  };

  let button;
  if (userJson && userJson.premium) {
    button = (
      <StyledText style={style.signInText}>You already own premium.</StyledText>
    );
  } else {
    button = (
      <CheckSignedInContainer
        signInToText="to upgrade"
        textStyle={style.buttonText}
        buttonTextStyle={style.buttonText}
      >
        <StyledButton
          text="upgrade"
          onPress={purchasePremium}
          textStyle={style.buttonText}
        />
      </CheckSignedInContainer>
    );
  }

  button = (
    <LoadingIndicatorContainer
      loading={initializingPayment}
      style={style.buttonContainer}
    >
      {button}
    </LoadingIndicatorContainer>
  );

  return (
    <Screen style={style.root}>
      <ProcessingModal processing={processingPayment} />
      <StyledText style={style.header}>
        Upgrade to the premium version
      </StyledText>
      <View style={style.priceContainer}>
        <StyledText>price: </StyledText>
        <LoadingIndicatorContainer
          loading={!(premium && premium.localizedPrice)}
        >
          <StyledText>{premium.localizedPrice}</StyledText>
        </LoadingIndicatorContainer>
      </View>
      <StyledText style={style.text}>Benefits include:</StyledText>
      <View style={style.list}>
        <StyledText style={style.bullet}>
          &bull; using your own images
        </StyledText>
        <StyledText style={style.bullet}>&bull; 8 new permutations</StyledText>
        <StyledText style={style.bullet}>
          &bull; field dimensions up to 10x10
        </StyledText>
        <StyledText style={style.bullet}>&bull; no ads</StyledText>
      </View>
      {button}
    </Screen>
  );
};

export default Upgrade;
