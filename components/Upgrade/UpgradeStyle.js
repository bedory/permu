import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont, xlFont, sm, xl, windowWidth } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "center",
    },
    header: {
      textAlign: "center",
      fontSize: xlFont,
      marginVertical: xl,
    },
    list: {
      width: 0.7 * windowWidth,
    },
    bullet: {
      marginVertical: sm,
    },
    buttonText: {
      fontSize: xlFont,
    },
    buttonContainer: {
      marginVertical: xl,
      height: 50,
    },
    text: {
      fontSize: smFont,
    },
    priceContainer: {
      flexDirection: "row",
      alignItems: "center",
      marginVertical: xl,
    },
  });
}

export default useStyle;
