import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xl, mdFont } = useStyleVariables();
  return StyleSheet.create({
    blur: {
      ...StyleSheet.absoluteFill,
    },
    root: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      padding: xl,
    },
    text: {
      margin: xl,
      textAlign: "center",
      lineHeight: mdFont + 8,
    },
  });
}

export default useStyle;
