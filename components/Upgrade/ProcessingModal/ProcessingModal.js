import React from "react";
import { Modal, View } from "react-native";
import useProcessingModalStyle from "./ProcessingModalStyle";
import StyledText from "../../general/StyledText/StyledText";
import { BlurView } from "@react-native-community/blur";
import LoadingIndicator from "../../general/LoadingIndicator/LoadingIndicator";

const ProcessingModal = ({ processing }) => {
  const style = useProcessingModalStyle();
  return (
    <Modal visible={processing} animationType="fade" transparant={true}>
      <BlurView style={style.blur} blurAmount={5} />
      <View style={style.root}>
        <StyledText style={style.text}>
          We are processing your purchase. This can take up to 30 seconds.
          Please wait.
        </StyledText>
        <LoadingIndicator size="large" />
      </View>
    </Modal>
  );
};

export default React.memo(ProcessingModal);
