import { StyleSheet } from "react-native";

function useStyle() {
  return StyleSheet.create({
    root: {},
  });
}

export default useStyle;
