import React from "react";
import { View } from "react-native";
import useComponentStyleTemplate from "./ComponentStyleTemplate";
import StyledText from "../general/StyledText/StyledText";

const ComponentTemplate = () => {
  const style = useComponentStyleTemplate();
  return (
    <View style={style.root}>
      <StyledText>ComponentTemplate</StyledText>
    </View>
  );
};

export default React.memo(ComponentTemplate);
