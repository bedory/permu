import React from "react";
import LogoIcon from "../../../assets/icons/logo.svg";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

const Logo = () => {
  const { textColor } = useStyleVariables();
  return <LogoIcon width={100} height={100} stroke={textColor} />;
};

export default Logo;
