import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import useStyledButtonStyle from "./StyledButtonStyle";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import StyledText from "../StyledText/StyledText";

const StyledButton = ({
  Icon,
  iconFillColor,
  text,
  textStyle,
  onPress,
  onPressIn,
  onPressOut,
  disabled = false,
  containerStyle,
  ...props
}) => {
  const style = useStyledButtonStyle(disabled);
  const { textColor } = useStyleVariables();
  let content;
  if (Icon && !text) {
    content = (
      <Icon
        width={35}
        height={35}
        fill={iconFillColor ? iconFillColor : textColor}
        {...props}
      />
    );
  } else if (text && !Icon) {
    content = (
      <StyledText style={StyleSheet.compose(style.text, textStyle)}>
        {text}
      </StyledText>
    );
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      disabled={disabled}
      style={StyleSheet.compose(style.root, containerStyle)}
    >
      {content}
    </TouchableOpacity>
  );
};

export default React.memo(StyledButton);
