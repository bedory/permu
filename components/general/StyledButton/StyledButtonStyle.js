import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle(disabled) {
  const {
    disabledOpacity,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      opacity: disabled ? disabledOpacity : 1.0,
    },
    text: {},
  });
}

export default useStyle;
