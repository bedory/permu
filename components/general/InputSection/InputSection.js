import React from "react";
import { View, StyleSheet } from "react-native";
import useInputSectionStyle from "./InputSectionStyle";
import StyledText from "../StyledText/StyledText";
import StyledTextInput from "../StyledTextInput/StyledTextInput";
import LoadingIndicatorContainer from "../LoadingIndicatorContainer/LoadingIndicatorContainer";
import StyledButton from "../StyledButton/StyledButton";

const InputSection = ({
  arrayTextInputProps = [],
  title,
  titleStyle,
  containerStyle,
  buttonProps = {},
  loadingIndicatorContainerProps = {},
}) => {
  const style = useInputSectionStyle();
  return (
    <View style={StyleSheet.compose(style.root, containerStyle)}>
      {title && (
        <StyledText style={StyleSheet.compose(style.title, titleStyle)}>
          {title}
        </StyledText>
      )}
      {arrayTextInputProps.map((textInputProps) => (
        <StyledTextInput key={textInputProps.label} {...textInputProps} />
      ))}
      <LoadingIndicatorContainer
        {...loadingIndicatorContainerProps}
        style={StyleSheet.compose(
          style.buttonContainer,
          loadingIndicatorContainerProps.style
        )}
      >
        <StyledButton
          {...buttonProps}
          textStyle={StyleSheet.compose(style.buttonText, buttonProps.style)}
        />
      </LoadingIndicatorContainer>
    </View>
  );
};

export default React.memo(InputSection);
