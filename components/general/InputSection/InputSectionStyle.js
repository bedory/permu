import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { md, lgFont, xlFont, windowWidth } = useStyleVariables();
  return StyleSheet.create({
    root: {
      marginVertical: md,
    },
    title: {
      fontSize: xlFont,
      width: 0.9 * windowWidth,
    },
    buttonContainer: {
      height: 30,
      marginVertical: md,
    },
    buttonText: {
      fontSize: lgFont,
      lineHeight: lgFont,
    },
  });
}

export default useStyle;
