import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle(disabled) {
  const { disabledOpacity, mdFont } = useStyleVariables();
  return StyleSheet.create({
    root: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    text: {
      opacity: disabled ? disabledOpacity : 1.0,
      lineHeight: mdFont,
    },
  });
}

export default useStyle;
