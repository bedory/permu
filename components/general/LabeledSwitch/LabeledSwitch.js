import React, { useState } from "react";
import { View, Switch, StyleSheet } from "react-native";
import useLabeledSwitchStyle from "./LabeledSwitchStyle";
import StyledText from "../StyledText/StyledText";

const LabeledSwitch = ({
  label,
  labelStyle,
  toggled,
  onValueChange,
  disabled = false,
}) => {
  const style = useLabeledSwitchStyle(disabled);
  const [internalState, setInternalState] = useState(false);
  const toggleSwitch = () =>
    setInternalState((previousState) => !previousState);

  const onValueChangeWithInternalState = (val) => {
    onValueChange && onValueChange(val);
    toggleSwitch();
  };

  return (
    <View style={style.root}>
      <StyledText style={StyleSheet.compose(style.text, labelStyle)}>
        {label}
      </StyledText>
      <Switch
        value={typeof toggled !== "undefined" ? toggled : internalState}
        onValueChange={onValueChangeWithInternalState}
        disabled={disabled}
      />
    </View>
  );
};

export default LabeledSwitch;
