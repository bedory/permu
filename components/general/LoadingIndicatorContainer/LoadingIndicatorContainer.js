import React from "react";
import { View, StyleSheet } from "react-native";
import useLoadingIndicatorContainerStyle from "./LoadingIndicatorContainerStyle";
import LoadingIndicator from "../LoadingIndicator/LoadingIndicator";

const LoadingIndicatorContainer = ({ children, loading, style }) => {
  const inherentStyle = useLoadingIndicatorContainerStyle();
  return (
    <View style={StyleSheet.compose(inherentStyle.root, style)}>
      {loading ? <LoadingIndicator /> : children}
    </View>
  );
};

export default LoadingIndicatorContainer;
