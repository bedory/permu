import { StyleSheet } from "react-native";

function useStyle() {
  return StyleSheet.create({
    root: {
      justifyContent: "center",
      alignItems: "center",
    },
  });
}

export default useStyle;
