import React, { useState } from "react";
import { StyleSheet } from "react-native";
import useToggleButtonStyle from "./ToggleButtonStyle";
import StyledButton from "../StyledButton/StyledButton";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

const ToggleButton = ({ toggled, onPress, textStyle, ...properties }) => {
  const style = useToggleButtonStyle(toggled);
  const [internalState, setInternalState] = useState(false);
  const toggle = () => setInternalState((prevState) => !prevState);
  const { accentColor, textColor } = useStyleVariables();

  const onPressAndToggle = () => {
    onPress && onPress(internalState);
    toggle();
  };

  // only use internal state if toggled isn't defined externally
  if (toggled === undefined) {
    toggled = internalState;
  }

  return (
    <StyledButton
      iconFillColor={toggled ? accentColor : textColor}
      textStyle={StyleSheet.compose(style.root, textStyle)}
      {...properties}
      onPress={onPressAndToggle}
    />
  );
};

export default React.memo(ToggleButton);
