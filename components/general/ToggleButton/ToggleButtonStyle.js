import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle(toggled) {
  const { accentColor, textColor } = useStyleVariables();
  return StyleSheet.create({
    root: { color: toggled ? accentColor : textColor },
  });
}

export default useStyle;
