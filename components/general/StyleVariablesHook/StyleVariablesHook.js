import React, { useContext } from "react";
import { useWindowDimensions } from "react-native";
import Constants from "expo-constants";
import { ThemeContext } from "../ThemeHook/ThemeHook";
import { SettingsContext } from "../SettingsHook/SettingsHook";

function useStyleVariables() {
  const windowWidth = useWindowDimensions().width;
  const windowHeight = useWindowDimensions().height;
  const { statusBarHeight } = Constants;
  const theme = useContext(ThemeContext);
  const { settings } = useContext(SettingsContext);

  return {
    windowWidth,
    windowHeight,
    statusBarHeight,
    landscape: windowWidth > windowHeight,
    fontFamily: "Comfortaa-Light",
    ...settings,
    ...theme,
  };
}

export default useStyleVariables;
