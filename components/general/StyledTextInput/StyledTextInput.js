import React from "react";
import { View, TextInput } from "react-native";
import useStyledTextInputStyle from "./StyledTextInputStyle";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import StyledText from "../StyledText/StyledText";

const StyledTextInput = ({ error, label, extraComponent, ...props }) => {
  const style = useStyledTextInputStyle();
  const { textColor, disabledTextColor } = useStyleVariables();
  return (
    <View style={style.root}>
      {label && <StyledText style={style.label}>{label}</StyledText>}
      <TextInput
        style={style.textInput}
        underlineColorAndroid={textColor}
        placeholderTextColor={disabledTextColor}
        {...props}
      />
      {error && <StyledText style={style.error}>{error}</StyledText>}
      {extraComponent}
    </View>
  );
};

export default React.memo(StyledTextInput);
