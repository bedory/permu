import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    windowWidth,
    md,
    smFont,
    errorColor,
    xsFont,
    fontFamily,
    textColor,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      width: 0.9 * windowWidth,
      padding: md,
    },
    label: {
      fontSize: smFont,
    },
    textInput: {
      fontFamily,
      color: textColor,
      fontSize: smFont,
    },
    error: {
      color: errorColor,
      fontSize: xsFont,
      alignSelf: "flex-end",
      lineHeight: xsFont,
      maxWidth: 0.5 * windowWidth,
      textAlign: "right",
    },
  });
}

export default useStyle;
