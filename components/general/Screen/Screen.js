import React from "react";
import { StyleSheet } from "react-native";
import useScreenStyle from "./ScreenStyle";
import { SafeAreaView } from "react-native-safe-area-context";

const Screen = ({ children, style, ...props }) => {
  const inherentStyle = useScreenStyle();
  return (
    <SafeAreaView
      style={StyleSheet.compose(inherentStyle.root, style)}
      {...props}
    >
      {children}
    </SafeAreaView>
  );
};

export default React.memo(Screen);
