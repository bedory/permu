import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { backgroundColor, xl } = useStyleVariables();
  return StyleSheet.create({
    root: {
      flex: 1,
      backgroundColor,
      padding: xl,
      alignItems: "center",
      justifyContent: "space-evenly",
    },
  });
}

export default useStyle;
