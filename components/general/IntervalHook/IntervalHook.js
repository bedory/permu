import React, {useEffect, useRef, useCallback} from 'react';

function useInterval(callback, delay) {
  const savedCallback = useRef();
  const interval = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      interval.current = setInterval(tick, delay);
      return () => clearInterval(interval.current);
    }
  }, [delay]);

  const clear = useCallback(() => {
    clearInterval(interval.current);
  }, []);

  const reset = useCallback(() => {
    clear();
    interval.current = setInterval(savedCallback.current, delay);
  }, [clear, delay]);

  return [reset, clear];
}

export default useInterval;
