import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    mdFont,
    accentColor,
  } = useStyleVariables();
  return StyleSheet.create({
    signIn: {
      flexDirection: "row",
      alignItems: "center",
    },
    signInButtonText: {
      color: accentColor,
      fontSize: mdFont,
    },
    signInText: {
      fontSize: mdFont,
    },
  });
}

export default useStyle;
