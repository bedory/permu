import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import useCheckSignedInContainerStyle from "./CheckSignedInContainerStyle";
import { UserContext } from "../UserHook/UserHook";
import StyledButton from "../StyledButton/StyledButton";
import StyledText from "../StyledText/StyledText";
import { useNavigation } from "@react-navigation/core";

const CheckSignedInContainer = ({
  children,
  signInToText,
  textStyle,
  buttonTextStyle,
}) => {
  const inherentStyle = useCheckSignedInContainerStyle();
  const { user } = useContext(UserContext);
  const navigation = useNavigation();

  if (user && !user.isAnonymous) {
    return children ? children : null;
  } else {
    return (
      <View style={inherentStyle.signIn}>
        <StyledButton
          text="sign in"
          onPress={() => navigation.navigate("SignIn")}
          textStyle={StyleSheet.compose(
            inherentStyle.signInButtonText,
            buttonTextStyle
          )}
        />
        {signInToText && (
          <StyledText
            style={StyleSheet.compose(inherentStyle.signInText, textStyle)}
          >
            {" "}
            {signInToText}
          </StyledText>
        )}
      </View>
    );
  }
};

export default CheckSignedInContainer;
