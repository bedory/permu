const settingsStructure = {
  categories: [
    {
      name: "appearance",
      data: [
        {
          name: "usePhonesAppearance",
          label: "use the phone's appearance setting",
          description:
            "On android 10+ you can set your preferred mode (light, dark) in the system settings. Enabling this option allows permu to use those settings.",
          type: "bool",
        },
        {
          name: "darkMode",
          label: "dark mode",
          type: "bool",
          dependsOn: [["usePhoneAppearance", false]],
        },
        {
          name: "showAnimations",
          label: "show permutation animations",
          description:
            "Enabling this option makes the cells animate from their original position before applying a permutation to their final position after applying the permutation.",
          type: "bool",
        },
        {
          name: "animationSpeed",
          label: "permutation animation speed",
          description: "Speed at which the above animation is performed.",
          type: "contValue",
          lowerBound: 50,
          upperBound: 1000,
          stepSize: 10,
          unit: "ms",
          dependsOn: [["showAnimations", true]],
        },
        {
          name: "showBorder",
          label: "show border",
          description:
            "This option allows you to specify when to show borders between the cells. The options include to always show a border, to never show a border and to only show a border between cells if those cells aren't correct neighbors.",
          type: "discrValue",
          possibleValues: [
            { value: "always", label: "always" },
            {
              value: "incorrectNeighbors",
              label: "between incorrect neighbors",
            },
            { value: "never", label: "never" },
          ],
        },
        {
          name: "highlightIndefinitely",
          label: "highlight indefinitely",
          description:
            "Disabling this option makes the unhighlights the highlighted cell after some time.",
          type: "bool",
        },
        {
          name: "highlightDuration",
          label: "duration of highlight",
          description:
            "Duration after which the highlighted cell is unhighlighted.",
          type: "contValue",
          lowerBound: 500,
          upperBound: 10000,
          stepSize: 10,
          unit: "ms",
          dependsOn: [["highlightIndefinitely", false]],
        },
        {
          name: "applyPreviewPermutation",
          label: "apply periodic permutation in preview",
          description:
            "Enabling this option makes the preview screen in the game setup apply a permutation periodically in order to show how the permutation works.",
          type: "bool",
        },
        {
          name: "intervalPreviewPermutation",
          label: "interval to apply permutation in preview",
          description:
            "Time between two consecutive permutations in the preview field in the game setup.",
          type: "contValue",
          lowerBound: 1000,
          upperBound: 5000,
          stepSize: 10,
          unit: "ms",
          dependsOn: [["applyPreviewPermutation", true]],
        },
        {
          name: "showNumberPermutations",
          label: "show number of permutations",
          description:
            "Enabling this option show you the number of permutations you've performed while playing the game.",
          type: "bool",
        },
        {
          name: "showStopwatch",
          label: "show stopwatch",
          description:
            "Enabling this option shows you a stopwatch indicating how long you've been playing.",
          type: "bool",
        },
        {
          name: "stopwatchPrecision",
          label: "precision of stopwatch",
          description:
            "The smallest unit of time by which the stopwatch is increased.",
          type: "contValue",
          lowerBound: 100,
          upperBound: 10000,
          stepSize: 10,
          unit: "ms",
          dependsOn: [["showStopwatch", true]],
        },
        {
          name: "distancePermutationIconsFromCenter",
          label: "distance of permutation icons from the center",
          type: "contValue",
          lowerBound: 30,
          upperBound: 150,
          stepSize: 60,
          unit: "px",
        },
        {
          name: "sizePermutationIcons",
          label: "the size of the permutation icons",
          type: "contValue",
          lowerBound: 10,
          upperBound: 60,
          stepSize: 5,
          unit: "px",
        },
      ],
    },
    {
      name: "controls",
      data: [
        {
          name: "longPressDuration",
          label: "duration to register long press",
          description:
            "Specifies how long it takes before a long press is registered as such.",
          type: "contValue",
          lowerBound: 100,
          upperBound: 5000,
          stepSize: 10,
          unit: "ms",
        },
        {
          name: "enableHighlightThisGoesToMode",
          label: "enable highlight mode button",
          description:
            "Enabling this button shows the highlight button. Pressing this button and then pressing a cell highlights the cell that has to go to the location of the pressed cell.",
          type: "bool",
        },
        {
          name: "enableHighlightGoesToThisMode",
          label: "enable highlight mode button",
          description:
            "Enabling this button shows the highlight button. Pressing this button and then pressing a cell highlights the cell that has to go to the location of the pressed cell.",
          type: "bool",
        },
        {
          name: "enableShowFinal",
          label: "enable show final button",
          description:
            "Enabling this button shows the 'show final field' button. Pressing this button assembles the field so you can see what the assembled field should look like.",
          type: "bool",
        },
        {
          name: "enableUndoRedo",
          label: "enable undo/redo",
          type: "bool",
        },
        {
          name: "maxNumberUndo",
          label: "max number undos",
          description:
            "The maximum number of times you can undo a permutation.",
          type: "contValue",
          lowerBound: 1,
          upperBound: 100,
          stepSize: 1,
          dependsOn: [["enableUndoRedo", true]],
        },
        {
          name: "pressAction",
          label: "On cell press",
          description:
            "This option allows you to specify what action to perform when pressing a cell.",
          type: "discrValue",
          possibleValues: [
            {
              value: "locationPermutation",
              label: "perform location based permutation",
            },
            {
              value: "highlightThisGoesTo",
              label: "highlight location that the pressed cell has to go to",
            },
            {
              value: "highlightGoesToThis",
              label: "highlight cell that has to go to the pressed location",
            },
            { value: "nothing", label: "do nothing" },
          ],
        },
        {
          name: "longPressAction",
          label: "On cell long press",
          description:
            "This option allows you to specify what action to perform when pressing a cell for a long time.",
          type: "discrValue",
          possibleValues: [
            {
              value: "locationPermutation",
              label: "perform location based permutation",
            },
            {
              value: "highlightThisGoesTo",
              label: "highlight location that the pressed cell has to go to",
            },
            {
              value: "highlightGoesToThis",
              label: "highlight cell that has to go to the pressed location",
            },
            { value: "nothing", label: "do nothing" },
          ],
        },
        {
          name: "swipeAction",
          label: "on swipe",
          description:
            "This option allows you to specify what action to perform when swiping from a cell in a certain direction.",
          type: "discrValue",
          possibleValues: [
            {
              value: "swipePermutation",
              label: "perform swipe direction based permutation",
            },
            { value: "nothing", label: "do nothing" },
          ],
        },
        {
          name: "showPermutationIndicators",
          label: "show permutation indicators",
          description:
            "This option allows you to specify when to show the permutation indicators. (Note that regardless of this setting, the permutation indicators won't be shown if swiping is not enabled.)",
          type: "discrValue",
          possibleValues: [
            { value: "press", label: "on press" },
            {
              value: "pressManyPermutations",
              label: "on press when number permutations is greater than four",
            },
            { value: "longPress", label: "on long press" },
            {
              value: "longPressManyPermutations",
              label:
                "on long press when number permutations is greater than four",
            },
            { value: "never", label: "never" },
          ],
          dependsOn: [["swipeAction", "swipePermutation"]],
        },
        {
          name: "permutationIndicatorsCenter",
          label: "center permutation indicators around",
          description:
            "This option allows you to specify around what point the permutation indicators are centered.",
          type: "discrValue",
          possibleValues: [
            { value: "cell", label: "the pressed cell" },
            { value: "location", label: "the pressed location" },
          ],
          dependsOn: [["swipeAction", "swipePermutation"]],
        },
        {
          name: "preferenceFirstSwipeDirection",
          label: "give more weight to the first swipe direction",
          description:
            "If this option is enabled, and you first swipe up, down, left or right before moving your finger in a different direction, then the direction of the first swipe is given more weight in determining your intended permutation. This is especially useful when you want to perform a rotation permutation by mimicking the motion of the rotation you want to perform.",
          type: "discrValue",
          possibleValues: [
            { value: "always", label: "always" },
            {
              value: "unlessPermutationIndicators",
              label: "when not showing permutation indicators",
            },
            { value: "never", label: "never" },
          ],
        },
      ],
    },
    {
      name: "controls",
      data: [
        {
          name: "personalizedAds",
          label: "personalized ads",
          description:
            "This option specifies whether the ads shown are personalized or non-personalized.",
          type: "bool",
        },
      ],
    },
  ],
};

const defaultSettings = {
  usePhonesAppearance: false,
  darkMode: true,
  showAnimations: true,
  animationSpeed: 150,
  showBorder: "incorrectNeighbors",
  highlightIndefinitely: true,
  highlightDuration: 3000,
  applyPreviewPermutation: true,
  intervalPreviewPermutation: 2500,
  showNumberPermutations: true,
  showStopwatch: true,
  stopwatchPrecision: 1000,
  distancePermutationIconsFromCenter: 60,
  sizePermutationIcons: 30,
  longPressDuration: 300,
  enableHighlightThisGoesToMode: true,
  enableHighlightGoesToThisMode: true,
  enableShowFinal: true,
  enableUndoRedo: true,
  maxNumberUndo: 40,
  pressAction: "nothing",
  longPressAction: "highlightGoesToThis",
  swipeAction: "swipePermutation",
  showPermutationIndicators: "longPressManyPermutations",
  permutationIndicatorsCenter: "cell",
  preferenceFirstSwipeDirection: "unlessPermutationIndicators",
  personalizedAds: true,
};

const defaultCasual = {
  ...defaultSettings,
  showAnimations: true,
  showNumberPermutations: false,
  showStopwatch: false,
  enableHighlightThisGoesToMode: true,
  enableHighlightGoesToThisMode: true,
  enableShowFinal: true,
  enableUndoRedo: true,
};

// for competitive you can't use highlighting, show final or undo/redo
const defaultCompetitive = {
  ...defaultSettings,
  showAnimations: false,
  showNumberPermutations: true,
  showStopwatch: true,
  enableHighlightThisGoesToMode: true,
  enableHighlightGoesToThisMode: true,
  enableShowFinal: true,
  enableUndoRedo: true,
};

export { defaultCasual, defaultCompetitive };
export default settingsStructure;
