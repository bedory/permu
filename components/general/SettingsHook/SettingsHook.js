import React, { useState, createContext, useCallback, useEffect } from "react";
import { defaultCasual } from "./settingsConstants";
import AsyncStorage from "@react-native-community/async-storage";
import { AdsConsent } from "@react-native-firebase/admob";

function useSettings(defaultSettings) {
  const [settings, setSettings] = useState(defaultSettings);
  const [adConsentGiven, setAdConsentGiven] = useState(false);
  const [inEeaOrUnknown, setInEeaOrUnknown] = useState();

  useEffect(() => {
    const setSettings = async () => {
      try {
        const settingsJsonString = await AsyncStorage.getItem("settings");
        if (settingsJsonString) {
          setSettings(JSON.parse(settingsJsonString));
        }
      } catch (e) {
        console.log(e);
      }
    };

    setSettings();
  }, []);

  useEffect(() => {
    const setAdsConsentInfo = async () => {
      try {
        const consentGivenPromise = AsyncStorage.getItem("adConsentGiven");
        const adsConsentInfoPromise = AdsConsent.requestInfoUpdate([
          "pub-4802298437271917",
        ]);
        const [consentGiven, adsConsentInfo] = await Promise.all([
          consentGivenPromise,
          adsConsentInfoPromise,
        ]);
        setAdConsentGiven(JSON.parse(consentGiven));
        setInEeaOrUnknown(adsConsentInfo.isRequestLocationInEeaOrUnknown);
      } catch (e) {
        console.log(e);
      }
    };

    setAdsConsentInfo();
  });

  const giveAdConsent = async () => {
    try {
      setAdConsentGiven(true);
      await AsyncStorage.setItem("adConsentGiven", JSON.stringify(true));
    } catch (e) {
      console.log(e);
    }
  };

  const changeSettings = useCallback((newSettings) => {
    setSettings((prevSettings) => {
      const newCompleteSettings = { ...prevSettings, ...newSettings };
      const jsonValue = JSON.stringify(newCompleteSettings);
      AsyncStorage.setItem("settings", jsonValue).catch(() =>
        console.warn(
          "Something went wrong while trying to save settings in storage."
        )
      );
      return newCompleteSettings;
    });
  }, []);

  const setSingleSetting = useCallback(
    (name, value) => {
      let newSettings = {};
      newSettings[name] = value;
      changeSettings(newSettings);
    },
    [changeSettings]
  );

  return {
    settings,
    setSettings: changeSettings,
    setSingleSetting,
    giveAdConsent,
    adConsentGiven,
    inEeaOrUnknown,
  };
}

const SettingsContext = createContext({
  settings: defaultCasual,
  setSettings: () => undefined,
  setSingleSetting: () => undefined,
  giveAdConsent: () => undefined,
  adConsentGiven: false,
  inEeaOrUnknown: false,
});

export { SettingsContext };
export default useSettings;
