import React, { createContext } from "react";
import { useColorScheme } from "react-native";
import { darkTheme, general, lightTheme } from "./ThemeConstants";

const useTheme = (usePhonesAppearance, darkMode) => {
  const colorScheme = useColorScheme();
  let theme;
  if (usePhonesAppearance && colorScheme) {
    if (colorScheme === "light") {
      theme = lightTheme;
    } else {
      theme = darkTheme;
    }
  } else {
    if (darkMode || darkMode === null || darkMode === undefined) {
      theme = darkTheme;
    } else {
      theme = lightTheme;
    }
  }

  theme = { ...theme, ...general };
  return theme;
};

export default useTheme;

const ThemeContext = createContext(lightTheme);
export { ThemeContext };
