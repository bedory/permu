const general = {
  xsFont: 12,
  smFont: 14,
  mdFont: 18,
  lgFont: 20,
  xlFont: 28,
  xxlFont: 40,
  xs: 3,
  sm: 5,
  md: 10,
  lg: 15,
  xl: 20,
  disabledOpacity: 0.3,
};

const lightTheme = {
  mode: "light",
  backgroundColor: "#f8f8f8",
  cellBackgroundColor: "#ffffff",
  contrastBackgroundColor: "#c1c1c1",
  accentColor: "#56bafc",
  darkAccentColor: "#367fa5",
  errorColor: "#ff4c4c",
  highlightColor: "#fbff91",
  correctColor: "#a3ff96",
  textColor: "#1d1d1d",
  disabledTextColor: "#b4b4b4",
};

const darkTheme = {
  mode: "dark",
  backgroundColor: "#202020",
  cellBackgroundColor: "#303030",
  contrastBackgroundColor: "#101010",
  accentColor: "#448bb8",
  darkAccentColor: "#264c65",
  errorColor: "#aa1f1f",
  highlightColor: "#a7ad27",
  correctColor: "#1f611f",
  textColor: "#eaeaea",
  disabledTextColor: "#787878",
};

export { lightTheme, darkTheme, general };
