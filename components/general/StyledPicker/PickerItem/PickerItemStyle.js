import { StyleSheet } from "react-native";
import useStyleVariables from "../../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { lgFont, md } = useStyleVariables();
  return StyleSheet.create({
    buttonText: {
      fontSize: lgFont,
      margin: md,
      textAlign: "center",
    },
    buttonContainer: {
      flex: 1,
    },
  });
}

export default useStyle;
