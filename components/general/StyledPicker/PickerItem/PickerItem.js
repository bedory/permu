import React from "react";
import { StyleSheet } from "react-native";
import usePickerItemStyle from "./PickerItemStyle";
import StyledButton from "../../StyledButton/StyledButton";

const PickerItem = ({ label, value, style, disabled, onValueChange }) => {
  const inherentStyle = usePickerItemStyle();
  return (
    <StyledButton
      text={label}
      textStyle={StyleSheet.compose(inherentStyle.buttonText, style)}
      constainerStyle={inherentStyle.buttonContainer}
      disabled={disabled}
      onPress={() => onValueChange(value)}
    />
  );
};

export default React.memo(PickerItem);
