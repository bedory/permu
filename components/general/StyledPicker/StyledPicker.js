import React, { Children, useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback,
} from "react-native";
import { BlurView } from "@react-native-community/blur";
import useStyledPickerStyle from "./StyledPickerStyle";
import StyledButton from "../StyledButton/StyledButton";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import { ScrollView } from "react-native-web";

const StyledPicker = ({
  children,
  selectedValue,
  style,
  onValueChange,
  buttonContainerStyle,
  buttonTextStyle,
}) => {
  const [showingItems, setShowingItems] = useState(false);
  const [selectedLabel, setSelectedLabel] = useState();
  const inherentStyle = useStyledPickerStyle();
  const { mode, backgroundColor } = useStyleVariables();

  useEffect(() => {
    Children.forEach(children, (child) => {
      if (selectedValue === child.props.value) {
        setSelectedLabel(child.props.label);
      }
    });
  }, [selectedValue]);

  const onValueChangeToOnValueChangeAndClose = (onValueChange) => {
    return (value) => {
      setShowingItems(false);
      return onValueChange(value);
    };
  };

  const pickerItems = Children.map(children, (child) => {
    if (!child.props.onValueChange) {
      return React.cloneElement(child, {
        onValueChange: onValueChangeToOnValueChangeAndClose(onValueChange),
      });
    } else {
      return React.cloneElement(child, {
        onValueChange: onValueChangeToOnValueChangeAndClose(
          child.props.onValueChange
        ),
      });
    }
  });

  return (
    <View style={StyleSheet.compose(inherentStyle.root, style)}>
      <StyledButton
        text={selectedLabel}
        onPress={() => setShowingItems(true)}
        textStyle={buttonTextStyle}
        containerStyle={buttonContainerStyle}
      />
      <Modal
        visible={showingItems}
        transparent={true}
        animationType="fade"
        onRequestClose={() => setShowingItems(false)}
      >
        <BlurView
          style={inherentStyle.blurView}
          blurType={mode}
          blurAmount={15}
          reducedTransparencyFallbackColor={backgroundColor}
        />
        <TouchableWithoutFeedback onPress={() => setShowingItems(false)}>
          <View style={inherentStyle.modal}>
            {/* useful is item is disabled. You don't want it to close when pressing disabled button */}
            <TouchableWithoutFeedback>
              <View style={inherentStyle.itemsContainer}>{pickerItems}</View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

export default StyledPicker;
