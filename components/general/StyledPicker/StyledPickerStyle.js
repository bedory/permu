import { StyleSheet } from "react-native";

function useStyle() {
  return StyleSheet.create({
    root: {},
    blurView: {
      ...StyleSheet.absoluteFill,
    },
    modal: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    itemsContainer: {
      alignItems: "center",
    },
  });
}

export default useStyle;
