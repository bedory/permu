import React, { useContext } from "react";
import { View } from "react-native";
import useMessageStyle from "./MessageStyle";
import { MessageContext } from "./MessageHook";
import Clear from "../../../assets/icons/clear.svg";
import StyledButton from "../StyledButton/StyledButton";
import Screen from "../Screen/Screen";
import StyledText from "../StyledText/StyledText";

const Message = () => {
  const { message, type, showing, hideMessage } = useContext(MessageContext);
  const style = useMessageStyle(type);
  if (!showing) {
    return null;
  }
  return (
    <Screen style={style.root}>
      <View style={style.message}>
        <StyledButton
          Icon={Clear}
          containerStyle={style.clearButton}
          width={20}
          height={20}
          onPress={hideMessage}
        />
        <StyledText style={style.messageText}>{message}</StyledText>
      </View>
    </Screen>
  );
};

export default Message;
