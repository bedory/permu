import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import { MessageTypes } from "./MessageHook";

function useStyle(type) {
  const {
    windowWidth,
    contrastBackgroundColor,
    md,
    smFont,
    correctColor,
    errorColor,
    highlightColor,
  } = useStyleVariables();
  let typeDependentBackgroundColor;
  switch (type) {
    case MessageTypes.INFO:
      typeDependentBackgroundColor = contrastBackgroundColor;
      break;
    case MessageTypes.WARNING:
      typeDependentBackgroundColor = highlightColor;
      break;
    case MessageTypes.ERROR:
      typeDependentBackgroundColor = errorColor;
      break;
    case MessageTypes.SUCCESS:
      typeDependentBackgroundColor = correctColor;
      break;
    default:
      break;
  }
  return StyleSheet.create({
    root: {
      position: "absolute",
      zIndex: 2000,
      alignItems: "center",
      alignSelf: "stretch",
      width: windowWidth,
    },
    message: {
      width: 0.95 * windowWidth,
      backgroundColor: typeDependentBackgroundColor,
      padding: 25,
      borderRadius: md,
    },
    clearButton: {
      position: "absolute",
      right: md,
      top: md,
    },
    messageText: {
      fontSize: smFont,
      lineHeight: smFont + 6,
      textAlign: "center",
    },
  });
}

export default useStyle;
