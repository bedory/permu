import React, { useState, createContext, useRef } from "react";

const MessageTypes = {
  INFO: 1,
  WARNING: 2,
  ERROR: 3,
  SUCCESS: 4,
};

const useMessage = () => {
  const [message, setMessage] = useState("");
  const [showing, setShowing] = useState(false);
  const [type, setType] = useState(MessageTypes.INFO);
  const messageShowDuration = 4000;
  const messageShowTimeout = useRef();

  const setMessageAndShow = (messageString, messageType) => {
    clearTimeout(messageShowTimeout.current);
    setMessage(messageString);
    setType(messageType);
    setShowing(true);
    messageShowTimeout.current = setTimeout(() => {
      setShowing(false);
    }, messageShowDuration);
  };

  const hideMessage = () => {
    setShowing(false);
    clearTimeout(messageShowTimeout.current);
  };

  return {
    message,
    showing,
    type,
    setMessageAndShow,
    hideMessage,
    setMessage,
  };
};

export default useMessage;

const MessageContext = createContext({
  message: "",
  showing: false,
  type: MessageTypes.INFO,
  setMessageAndShow: () => undefined,
  hideMessage: () => undefined,
  setMessage: () => undefined,
});

export { MessageContext, MessageTypes };
