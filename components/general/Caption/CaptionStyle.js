import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { accentColor, xsFont } = useStyleVariables();
  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-end",
    },
    text: {
      fontSize: xsFont,
    },
    link: {
      fontSize: xsFont,
      color: accentColor,
    },
  });
}

export default useStyle;
