import React from "react";
import { View, Linking } from "react-native";
import useCaptionStyle from "./CaptionStyle";
import StyledText from "../StyledText/StyledText";

const Caption = ({ imageObject }) => {
  const style = useCaptionStyle();
  return (
    <View style={style.root}>
      <StyledText style={style.text}>By&nbsp;</StyledText>
      <StyledText
        style={style.link}
        onPress={() => Linking.openURL(imageObject.artistUrl)}
      >
        {imageObject.artist}
      </StyledText>
      <StyledText style={style.text}>&nbsp;on&nbsp;</StyledText>
      <StyledText
        style={style.link}
        onPress={() => Linking.openURL(imageObject.platformUrl)}
      >
        {imageObject.platform}
      </StyledText>
    </View>
  );
};

export default React.memo(Caption, () => false);
