import React, { useCallback, createContext } from "react";
import { LayoutAnimation } from "react-native";

function useAnimation(duration, animationOn) {
  const animateNext = useCallback(
    (force = false, animationDuration = duration) => {
      if (animationOn || force) {
        LayoutAnimation.configureNext({
          duration: animationDuration,
          create: { property: "opacity", type: "easeOut" },
          update: { type: "easeOut" },
          // delete animation doesn't work for permutationIndicators
          // delete: { property: "opacity", type: "easeOut" },
        });
      }
    }
  );

  return { animateNext };
}

export default useAnimation;

const AnimationContext = createContext({ animateNext: () => undefined });
export { AnimationContext };
