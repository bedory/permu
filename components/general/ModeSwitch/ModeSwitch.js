import React from "react";
import { View, Switch } from "react-native";
import style from "./ModeSwitchStyle";
import { Colors } from "../../../theme";

const ModeSwitch = ({ LeftIcon, RightIcon, onValueChange }) => {
  return (
    <View style={style.root}>
      <LeftIcon fill={Colors.iconColor} />
      <Switch onValueChange={onValueChange} />
      <RightIcon fill={Colors.iconColor} />
    </View>
  );
};

export default ModeSwitch;
