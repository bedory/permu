import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    windowWidth,
    contrastBackgroundColor,
    lg,
    xl,
    smFont,
    backgroundColor,
    accentColor,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    container: {
      justifyContent: "center",
      alignItems: "center",
      flex: 1,
    },
    modal: {
      width: 0.8 * windowWidth,
      backgroundColor: contrastBackgroundColor,
      padding: 25,
      borderRadius: xl,
      zIndex: 5,
      alignItems: "center",
    },
    backgroundBlur: {
      ...StyleSheet.absoluteFill,
      backgroundColor,
      opacity: 0.5,
    },
    description: {
      fontSize: smFont,
      lineHeight: smFont + 6,
      textAlign: "center",
    },
    buttonText: {
      textAlign: "center",
      margin: lg,
    },
    link: {
      fontSize: smFont,
      lineHeight: smFont + 6,
      color: accentColor,
    },
  });
}

export default useStyle;
