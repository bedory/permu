import React, { useContext, useEffect, useState } from "react";
import { View, Modal, Linking } from "react-native";
import useConsentModalStyle from "./ConsentModalStyle";
import StyledText from "../StyledText/StyledText";
import StyledButton from "../StyledButton/StyledButton";
import { useNavigation } from "@react-navigation/native";
import { SettingsContext } from "../SettingsHook/SettingsHook";
import { UserContext } from "../UserHook/UserHook";
import { useIsFocused } from "@react-navigation/native";

const ConsentModal = () => {
  const style = useConsentModalStyle();
  const navigation = useNavigation();
  const { userJson, initialized } = useContext(UserContext);
  const {
    setSingleSetting,
    giveAdConsent,
    adConsentGiven,
    inEeaOrUnknown,
  } = useContext(SettingsContext);
  const isFocused = useIsFocused();

  const visible =
    initialized &&
    !(userJson && userJson.premium) &&
    inEeaOrUnknown !== undefined &&
    inEeaOrUnknown &&
    !adConsentGiven &&
    isFocused;

  return (
    <Modal visible={visible} transparent={true} animationType="fade">
      <View style={style.container}>
        <View style={style.modal}>
          <StyledText>
            <StyledText style={style.description}>
              This free version of permu contains ads provided through google
              admob. Please select one of the following options. By doing so you
              agree with our{" "}
            </StyledText>
            <StyledText
              style={style.link}
              onPress={() => {
                Linking.openURL("http://permu.bedory.com/privacy");
              }}
            >
              privacy policy.
            </StyledText>
          </StyledText>
          <StyledButton
            text="use personalized ads"
            textStyle={style.buttonText}
            onPress={() => {
              giveAdConsent();
              setSingleSetting("personalizedAds", true);
            }}
          />
          <StyledButton
            text="use non-personalized ads"
            textStyle={style.buttonText}
            onPress={() => {
              giveAdConsent();
              setSingleSetting("personalizedAds", false);
            }}
          />
          <StyledButton
            text="buy premium for an ad-free experience"
            textStyle={style.buttonText}
            onPress={() => navigation.navigate("Upgrade")}
          />
        </View>
        <View style={style.backgroundBlur} />
      </View>
    </Modal>
  );
};

export default React.memo(ConsentModal);
