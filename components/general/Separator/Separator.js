import React from "react";
import { View } from "react-native";
import useSeparatorStyle from "./SeparatorStyle";

const Separator = () => {
  const style = useSeparatorStyle();
  return <View style={style.root} />;
};

export default React.memo(Separator);
