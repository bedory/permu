import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { windowWidth, textColor, disabledOpacity } = useStyleVariables();
  return StyleSheet.create({
    root: {
      width: 0.9 * windowWidth,
      backgroundColor: textColor,
      opacity: disabledOpacity,
      height: StyleSheet.hairlineWidth,
    },
  });
}

export default useStyle;
