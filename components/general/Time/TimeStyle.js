import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    text: {},
    verticalText: {
      fontSize: smFont,
      lineHeight: smFont + 2,
    },
  });
}

export default useStyle;
