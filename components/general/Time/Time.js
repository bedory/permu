import React from "react";
import { View } from "react-native";
import useTimeStyle from "./TimeStyle";
import { formatTime, stringToTextLetters } from "../randomFunctions";
import StyledText from "../StyledText/StyledText";

const Time = ({ duration, vertical = false }) => {
  const style = useTimeStyle();
  const durationString = formatTime(duration);
  let textDom;
  if (vertical) {
    textDom = stringToTextLetters(
      durationString.split(":").join(" "),
      style.verticalText
    );
  } else {
    textDom = (
      <StyledText style={style.text}>{formatTime(duration)}</StyledText>
    );
  }
  return <View style={style.root}>{textDom}</View>;
};

export default Time;
