import React, { useState, useContext } from "react";
import { View } from "react-native";
import usePermutationPickerStyle from "./PermutationPickerStyle";
import permutationFamilyMap from "../../Field/PermutationFamilies";
import { FieldWithPermutationPreviewContext } from "../../Field/FieldWithPermutationPreviewHook";
import { UserContext } from "../../UserHook/UserHook";
import PickerItem from "../../StyledPicker/PickerItem/PickerItem";
import StyledPicker from "../../StyledPicker/StyledPicker";

const PermutationPicker = ({ forcePremium }) => {
  const style = usePermutationPickerStyle();
  const [nameSelectedPermutation, setNameSelectedPermutation] = useState(
    "swap"
  );
  const { permutationFamily } = useContext(FieldWithPermutationPreviewContext);
  const { userJson } = useContext(UserContext);
  const permutationItems = Object.keys(
    permutationFamilyMap
  ).map((permutationName) => (
    <PickerItem
      label={permutationName}
      value={permutationName}
      key={permutationName}
      disabled={
        permutationName !== "swap" &&
        permutationName !== "shift" &&
        permutationName !== "rotate" &&
        !(userJson && userJson.premium) &&
        !forcePremium
      }
    />
  ));

  return (
    <View style={style.root}>
      <StyledPicker
        selectedValue={nameSelectedPermutation}
        style={style.picker}
        buttonTextStyle={style.buttonText}
        onValueChange={(permutationName) => {
          permutationFamily.setPermutationFamily(
            permutationFamilyMap[permutationName]
          );
          setNameSelectedPermutation(permutationName);
        }}
      >
        {permutationItems}
      </StyledPicker>
    </View>
  );
};

export default PermutationPicker;
