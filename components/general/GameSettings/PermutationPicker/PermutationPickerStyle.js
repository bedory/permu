import { StyleSheet } from "react-native";
import useStyleVariables from "../../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { lgFont, xlFont, md } = useStyleVariables();
  return StyleSheet.create({
    root: {
      alignItems: "center",
    },
    buttonText: {
      fontSize: xlFont,
      margin: md,
    },
  });
}

export default useStyle;
