import React, { useContext } from "react";
import { View } from "react-native";
import DimensionsPicker from "./DimensionsPicker/DimensionsPicker";
import PermutationPicker from "./PermutationPicker/PermutationPicker";
import LabeledSwitch from "../LabeledSwitch/LabeledSwitch";
import { FieldWithPermutationPreviewContext } from "../Field/FieldWithPermutationPreviewHook";
import useGameSettingsStyle from "./GameSettingsStyle";

const GameSettings = ({ forcePremium }) => {
  const style = useGameSettingsStyle();
  const { permutationFamily, wrap, setWrap } = useContext(
    FieldWithPermutationPreviewContext
  );

  return (
    <View style={style.root}>
      <DimensionsPicker forcePremium={forcePremium} />
      <PermutationPicker forcePremium={forcePremium} />
      <LabeledSwitch
        label="wrap around"
        toggled={wrap || !permutationFamily.permutationFamily.allowsNoWrap}
        onValueChange={(val) => setWrap(val)}
        disabled={!permutationFamily.permutationFamily.allowsNoWrap}
        labelStyle={style.label}
      />
    </View>
  );
};

export default GameSettings;
