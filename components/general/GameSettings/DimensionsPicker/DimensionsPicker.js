import React, { useContext, useState } from "react";
import { View } from "react-native";
import useDimensionsPickerStyle from "./DimensionsPickerStyle";
import { FieldWithPermutationPreviewContext } from "../../Field/FieldWithPermutationPreviewHook";
import Cross from "./../../../../assets/icons/clear.svg";
import { ThemeContext } from "../../ThemeHook/ThemeHook";
import { UserContext } from "../../UserHook/UserHook";
import PickerItem from "../../StyledPicker/PickerItem/PickerItem";
import StyledPicker from "../../StyledPicker/StyledPicker";
import StyledText from "../../StyledText/StyledText";

const DimensionsPicker = ({ forcePremium }) => {
  const style = useDimensionsPickerStyle();
  const { textColor, disabledTextColor } = useContext(ThemeContext);
  const { userJson } = useContext(UserContext);
  const { field } = useContext(FieldWithPermutationPreviewContext);
  const [numberRows, setNumberRows] = useState(field.numberRows);
  const [numberColumns, setNumberColumns] = useState(field.numberColumns);

  const possibleValues = [3, 4, 5, 6, 7, 8, 9, 10];
  const items = possibleValues.map((value) => (
    <PickerItem
      label={value.toString()}
      value={value}
      key={value}
      disabled={!forcePremium && !(userJson && userJson.premium) && value > 6}
    />
  ));

  return (
    <View style={style.root}>
      <View style={style.pickerContainer}>
        <View style={style.button}>
          <StyledText style={style.buttonText}>{numberRows}</StyledText>
        </View>
        <StyledPicker
          selectedValue={numberRows}
          style={style.picker}
          onValueChange={(value) => {
            field.setDimensions(value, numberColumns);
            setNumberRows(value);
          }}
        >
          {items}
        </StyledPicker>
      </View>
      <Cross height={25} width={25} fill={disabledTextColor} />
      <View style={style.pickerContainer}>
        <View style={style.button}>
          <StyledText style={style.buttonText}>{numberColumns}</StyledText>
        </View>
        <StyledPicker
          selectedValue={numberColumns}
          style={style.picker}
          itemStyle={style.item}
          onValueChange={(value) => {
            field.setDimensions(numberRows, value);
            setNumberColumns(value);
          }}
        >
          {items}
        </StyledPicker>
      </View>
    </View>
  );
};

export default DimensionsPicker;
