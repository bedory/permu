import { StyleSheet } from "react-native";
import useStyleVariables from "../../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xlFont, disabledTextColor } = useStyleVariables();
  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-evenly",
      alignItems: "center",
      borderBottomWidth: 2 * StyleSheet.hairlineWidth,
      borderTopWidth: 2 * StyleSheet.hairlineWidth,
      borderColor: disabledTextColor,
    },
    pickerContainer: {
      width: 75,
      height: 50,
    },
    button: {
      ...StyleSheet.absoluteFill,
      justifyContent: "center",
      alignItems: "center",
    },
    buttonText: {
      fontSize: xlFont,
      lineHeight: xlFont + 8,
    },
    picker: {
      ...StyleSheet.absoluteFill,
      opacity: 0,
    },
    item: {},
  });
}

export default useStyle;
