import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { lgFont, md } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "space-evenly",
      flex: 1,
      alignItems: "center",
      margin: md,
    },
    label: {
      fontSize: lgFont,
      margin: md,
    },
  });
}

export default useStyle;
