import React from "react";
import { StyleSheet, Text } from "react-native";
import useStyledTextStyle from "./StyledTextStyle";

const StyledText = ({ children, style, ...props }) => {
  const inherentStyle = useStyledTextStyle();
  return (
    <Text style={StyleSheet.compose(inherentStyle.root, style)} {...props}>
      {children}
    </Text>
  );
};

export default React.memo(StyledText);
