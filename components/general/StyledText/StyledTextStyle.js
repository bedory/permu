import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { fontFamily, textColor, mdFont } = useStyleVariables();
  return StyleSheet.create({
    root: {
      fontFamily,
      color: textColor,
      fontSize: mdFont,
    },
  });
}

export default useStyle;
