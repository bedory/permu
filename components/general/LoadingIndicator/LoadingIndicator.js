import React from "react";
import { ActivityIndicator } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

const LoadingIndicator = ({ ...props }) => {
  const { textColor } = useStyleVariables();
  return <ActivityIndicator color={textColor} size="small" {...props} />;
};

export default LoadingIndicator;
