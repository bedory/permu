import React, { createContext, useState, useEffect } from "react";
import auth from "@react-native-firebase/auth";
import AsyncStorage from "@react-native-community/async-storage";

const useUser = () => {
  const [initialized, setInitialized] = useState(false);
  const [user, setUser] = useState();
  const [userJson, setUserJson] = useState();

  async function setUserAndPremium(user, forceIdTokenReload) {
    if (user) {
      try {
        const idTokenResult = await user.getIdTokenResult(forceIdTokenReload);
        user.premium = idTokenResult.claims.premium;
        const userJsonObj = {
          ...user.toJSON(),
          premium: idTokenResult.claims.premium,
        };
        setUserJson(userJsonObj);
        setUser(user);
        await AsyncStorage.setItem("@user", JSON.stringify(userJsonObj));
      } catch (e) {
        console.log(e);
      }
    } else {
      setUser(null);
      setUserJson(null);
      await AsyncStorage.setItem("@user", JSON.stringify(null));
    }
  }

  async function reload(forceIdTokenReload) {
    try {
      if (auth().currentUser) {
        await auth().currentUser.reload();
        setUserAndPremium(auth().currentUser, forceIdTokenReload);
      } else {
        setUserAndPremium(null);
      }
    } catch (e) {
      console.log("from reload", e);
    }
  }

  useEffect(() => {
    const initializeUser = async () => {
      try {
        const userJsonString = await AsyncStorage.getItem("@user");
        const userJsonObj = JSON.parse(userJsonString);
        if (userJsonObj) {
          setUserJson(userJsonObj);
          setInitialized(true);
        }
        await reload();
        setInitialized(true);
      } catch (e) {
        console.log(e);
      }
    };

    initializeUser();
  }, []);

  return { initialized, user, userJson, reload };
};

export default useUser;

const UserContext = createContext({ initialized: false, user: null });
export { UserContext };
