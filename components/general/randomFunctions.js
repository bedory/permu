import React from "react";
import StyledText from "./StyledText/StyledText";

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function lcm_two_numbers(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    return false;
  }
  return !x || !y ? 0 : Math.abs((x * y) / gcd_two_numbers(x, y));
}

function gcd_two_numbers(x, y) {
  x = Math.abs(x);
  y = Math.abs(y);
  while (y) {
    let t = y;
    y = x % y;
    x = t;
  }
  return x;
}

function stringToTextLetters(string, textStyle = null) {
  const textDom = [];
  for (let i = 0; i < string.length; i++) {
    const letter = string.charAt(i);
    textDom.push(
      <StyledText key={i} style={textStyle}>
        {letter}
      </StyledText>
    );
  }
  return textDom;
}

function removeWhiteSpaceLeftSide(str) {
  if (!str) return str;
  return str.replace(/^\s+/g, "");
}

function removeWhiteSpaceRightSide(str) {
  if (!str) return str;
  return str.replace(/\s+$/g, "");
}

function checkValidEmailFormat(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function removeWhiteSpaceLeftAndRight(str) {
  return removeWhiteSpaceLeftSide(removeWhiteSpaceRightSide(str));
}

const intToDoubleDigitString = (number) => {
  if (number <= 0) {
    return "00";
  } else if (number <= 9) {
    return "0" + number.toString();
  } else {
    return number.toString().slice(0, 2);
  }
};

// formats time in milliseconds into HH:mm:ss:SS
const formatTime = (time) => {
  const secondsInMillisecond = 1000;
  const secondsInMinute = 60;
  const minutesInHour = 60;

  const milliseconds = time % secondsInMillisecond;
  const totalSeconds = Math.floor(time / secondsInMillisecond);
  const seconds = totalSeconds % secondsInMinute;
  const totalMinutes = Math.floor(totalSeconds / secondsInMinute);
  const minutes = totalMinutes % minutesInHour;
  const hours = Math.floor(totalMinutes / minutesInHour);
  return `${intToDoubleDigitString(hours)}:${intToDoubleDigitString(
    minutes
  )}:${intToDoubleDigitString(seconds)}:${intToDoubleDigitString(
    milliseconds
  )}`;
};

function padZero(str, len) {
  len = len || 2;
  let zeros = new Array(len).join("0");
  return (zeros + str).slice(-len);
}

function invertColor(hex, bw = false) {
  if (hex.indexOf("#") === 0) {
    hex = hex.slice(1);
  }
  // convert 3-digit hex to 6-digits.
  if (hex.length === 3) {
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  if (hex.length !== 6) {
    throw new Error("Invalid HEX color.");
  }
  let r = parseInt(hex.slice(0, 2), 16),
    g = parseInt(hex.slice(2, 4), 16),
    b = parseInt(hex.slice(4, 6), 16);
  if (bw) {
    // http://stackoverflow.com/a/3943023/112731
    return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? "#000000" : "#FFFFFF";
  }
  // invert color components
  r = (255 - r).toString(16);
  g = (255 - g).toString(16);
  b = (255 - b).toString(16);
  // pad each with zeros and return
  return "#" + padZero(r) + padZero(g) + padZero(b);
}

function printObject(object) {
  console.log(JSON.stringify(object, null, "\t"));
}

function turnAllArraysIntoObject(obj) {
  if (Object.keys(obj).length === 0) {
    return obj;
  }
  let newObj = {};
  Object.keys(obj).forEach((key) => {
    if (Array.isArray(obj[key])) {
      newObj[key] = {};
      obj[key].forEach((item, index) => {
        newObj[key][index] = turnAllArraysIntoObject(item);
      });
    } else {
      newObj[key] = turnAllArraysIntoObject(obj[key]);
    }
  });
  return newObj;
}

export {
  getRandomInt,
  lcm_two_numbers,
  gcd_two_numbers,
  stringToTextLetters,
  removeWhiteSpaceLeftAndRight,
  removeWhiteSpaceRightSide,
  removeWhiteSpaceLeftSide,
  checkValidEmailFormat,
  intToDoubleDigitString,
  formatTime,
  invertColor,
  printObject,
  turnAllArraysIntoObject,
};
