import React, { useRef, useState, useEffect, useCallback } from "react";

const STARTED = 0;
const STOPPED = 1;

const useStopwatch = (precision = 1000) => {
  const [time, setTime] = useState(0);
  const dateAtStart = useRef();
  const interval = useRef();
  const [state, setState] = useState(STOPPED);

  const startTime = useCallback(() => {
    if (state !== STARTED) {
      setState(STARTED);
      dateAtStart.current = Date.now();
      clearInterval(interval.current);
      if (precision > 0) {
        interval.current = setInterval(() => {
          setTime((prevTime) => prevTime + precision);
        }, precision);
      }
    }
  }, [precision, state]);

  const stopTime = useCallback(() => {
    if (state !== STOPPED) {
      setState(STOPPED);
      clearInterval(interval.current);
      setTime(Date.now() - dateAtStart.current);
    }
  }, [state]);

  const resetTime = useCallback(() => {
    setState(STOPPED);
    clearInterval(interval.current);
    setTime(0);
  }, []);

  const restartTime = useCallback(() => {
    setState(STARTED);
    clearInterval(interval.current);
    dateAtStart.current = Date.now();
    setTime(0);
    if (precision > 0) {
      interval.current = setInterval(() => {
        setTime((prevTime) => prevTime + precision);
      }, precision);
    }
  }, [precision]);

  useEffect(() => {
    return () => {
      clearInterval(interval.current);
    };
  }, []);

  return {
    startTime,
    stopTime,
    resetTime,
    restartTime,
    time,
    dateAtStart: dateAtStart.current,
  };
};

export default useStopwatch;
