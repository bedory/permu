import React, { useEffect, useState, useRef } from "react";
import { Animated, StyleSheet } from "react-native";
import useSwipeAnimationStyle from "./SwipeAnimationStyle";
import { angleAndLengthToXY } from "../CellButton/geoFunctions";

const SwipeAnimation = ({ width, height, swipeAngle, swipeDuration }) => {
  const circleDiameter = 40;
  const style = useSwipeAnimationStyle(circleDiameter);
  const positionAnim = useRef(new Animated.ValueXY()).current;
  const swipeDistance = 100;

  const swipe = () => {
    Animated.timing(positionAnim, {
      toValue: angleAndLengthToXY(swipeAngle, swipeDistance),
      duration: swipeDuration,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    swipe();
  }, []);

  return <Animated.View style={[style.circle, {transform: positionAnim.getTranslateTransform()} ]} />;
};

export default SwipeAnimation;
