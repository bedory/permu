import { StyleSheet } from "react-native";
import useStyleVariables from "../../../StyleVariablesHook/StyleVariablesHook";

function useStyle(circleDiameter) {
  const { textColor, disabledOpacity } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    circle: {
      position: "absolute",
      backgroundColor: textColor,
      width: circleDiameter,
      height: circleDiameter,
      borderRadius: circleDiameter / 2,
      zIndex: 20,
      opacity: disabledOpacity,
    },
  });
}

export default useStyle;
