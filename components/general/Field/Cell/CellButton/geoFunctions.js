function getHypotenuseLength(x, y) {
  return Math.sqrt(x * x + y * y);
}

function getAngle(x, y) {
  // the -y is because the positive y direction is from the top of the screen to the bottom.
  return Math.atan2(-y, x);
}

function radToDeg(angleInDeg) {
  return ((angleInDeg * 180) / Math.PI + 360) % 360;
}

function degToRad(angleInRad) {
  return ((angleInRad * Math.PI) / 180 + 2 * Math.PI) % (2 * Math.PI);
}

function getAngleDeg(x, y) {
  return radToDeg(getAngle(x, y));
}

function isAngleInAngleRange(angle, range) {
  if (range.from > range.to) {
    return angle >= range.from || angle <= range.to;
  } else {
    return angle >= range.from && angle <= range.to;
  }
}

function getHalfAngle(range) {
  if (range.from > range.to) {
    return ((range.from + range.to + 360) / 2) % 360;
  } else {
    return (range.from + range.to) / 2;
  }
}

function isAngleDirectionPositiveX(angle, detectionAngle) {
  return isAngleInAngleRange(angle, {
    from: 360 - detectionAngle / 2,
    to: detectionAngle / 2,
  });
}

function isAngleDirectionNegativeX(angle, detectionAngle) {
  return isAngleInAngleRange(angle, {
    from: 180 - detectionAngle / 2,
    to: 180 + detectionAngle / 2,
  });
}

function isAngleDirectionPositiveY(angle, detectionAngle) {
  return isAngleInAngleRange(angle, {
    from: 90 - detectionAngle / 2,
    to: 90 + detectionAngle / 2,
  });
}

function isAngleDirectionNegativeY(angle, detectionAngle) {
  return isAngleInAngleRange(angle, {
    from: 270 - detectionAngle / 2,
    to: 270 + detectionAngle / 2,
  });
}

function doesAngleRangeRepresentDirectionPositiveX(angleRange) {
  return getHalfAngle(angleRange) === 0;
}

function doesAngleRangeRepresentDirectionNegativeX(angleRange) {
  return getHalfAngle(angleRange) === 180;
}

function doesAngleRangeRepresentDirectionPositiveY(angleRange) {
  return getHalfAngle(angleRange) === 90;
}

function doesAngleRangeRepresentDirectionNegativeY(angleRange) {
  return getHalfAngle(angleRange) === 270;
}

function angleAndLengthToXY(angle, length) {
  return {
    x: length * Math.cos(degToRad(angle)),
    y: -length * Math.sin(degToRad(angle)),
  };
}

export {
  getHypotenuseLength,
  getAngle,
  radToDeg,
  degToRad,
  getAngleDeg,
  isAngleInAngleRange,
  getHalfAngle,
  isAngleDirectionNegativeY,
  isAngleDirectionPositiveY,
  isAngleDirectionNegativeX,
  isAngleDirectionPositiveX,
  doesAngleRangeRepresentDirectionNegativeX,
  doesAngleRangeRepresentDirectionNegativeY,
  doesAngleRangeRepresentDirectionPositiveX,
  doesAngleRangeRepresentDirectionPositiveY,
  angleAndLengthToXY,
};
