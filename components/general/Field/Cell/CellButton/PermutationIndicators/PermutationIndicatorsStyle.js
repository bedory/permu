import { StyleSheet } from "react-native";

function useStyle(
  permutationIndicatorsCenter,
  initialDxFromCenterButton,
  initialDyFromCenterButton,
  width,
  height
) {
  return StyleSheet.create({
    root: {
      position: "absolute",
      left:
        permutationIndicatorsCenter === "location"
          ? width / 2 + initialDxFromCenterButton
          : width / 2,
      top:
        permutationIndicatorsCenter === "location"
          ? height / 2 + initialDyFromCenterButton
          : height / 2,
    }
  });
}

export default useStyle;
