import React from "react";
import { View } from "react-native";
import usePermutationIndicatorsStyle from "./PermutationIndicatorsStyle";
import PermutationIndicator from "./PermutationIndicator/PermutationIndicator";

const PermutationIndicators = ({
  permutationIndicatorsCenter,
  initialDxFromCenterButton,
  initialDyFromCenterButton,
  width,
  height,
  permutationFamily,
  isPermutationWithIndexExecutable,
  focusedPermutationIndex,
  permutationIndicatorColor,
}) => {
  const style = usePermutationIndicatorsStyle(
    permutationIndicatorsCenter,
    initialDxFromCenterButton,
    initialDyFromCenterButton,
    width,
    height,
    isPermutationWithIndexExecutable
  );
  return (
    <View style={style.root}>
      {permutationFamily.permutations.map((permutation, index) => {
        if (isPermutationWithIndexExecutable(index)) {
          return (
            <PermutationIndicator
              key={permutation.name}
              permutation={permutation}
              focused={focusedPermutationIndex === index}
              color={permutationIndicatorColor}
            />
          );
        } else {
          return null;
        }
      })}
    </View>
  );
};

export default React.memo(PermutationIndicators);
