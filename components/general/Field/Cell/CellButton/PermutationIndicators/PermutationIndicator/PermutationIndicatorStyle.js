import { StyleSheet } from "react-native";
import { angleAndLengthToXY, getHalfAngle } from "../../geoFunctions";
import useStyleVariables from "../../../../../StyleVariablesHook/StyleVariablesHook";

function useStyle(swipeAngle, focused) {
  const {
    sizePermutationIcons,
    distancePermutationIconsFromCenter,
  } = useStyleVariables();
  const halfwayAngle = getHalfAngle(swipeAngle);
  const { x, y } = angleAndLengthToXY(
    halfwayAngle,
    distancePermutationIconsFromCenter
  );
  return StyleSheet.create({
    root: {
      opacity: focused ? 1.0 : 0.3,
      position: "absolute",
      left: x - sizePermutationIcons / 2,
      top: y - sizePermutationIcons / 2,
      width: sizePermutationIcons,
      height: sizePermutationIcons,
    },
  });
}

export default useStyle;
