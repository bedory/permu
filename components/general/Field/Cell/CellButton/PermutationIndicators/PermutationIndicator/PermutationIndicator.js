import React from "react";
import { View } from "react-native";
import usePermutationIndicatorStyle from "./PermutationIndicatorStyle";
import PermutationIconMap from "../../../../PermutationIcons";
import Dot from "../../../../../../../assets/permutationIcons/dot.svg";
import useStyleVariables from "../../../../../StyleVariablesHook/StyleVariablesHook";

const PermutationIndicator = ({ permutation, focused, color }) => {
  const { swipeAngle, iconName, iconTransform } = permutation;
  const { sizePermutationIcons, textColor } = useStyleVariables();
  const style = usePermutationIndicatorStyle(swipeAngle, focused);
  const Icon = iconName ? PermutationIconMap[iconName] : Dot;
  return (
    <View style={style.root}>
      <Icon
        width={sizePermutationIcons}
        height={sizePermutationIcons}
        fill={color ? color : textColor}
        style={{ transform: iconTransform }}
      />
    </View>
  );
};

export default React.memo(PermutationIndicator);
