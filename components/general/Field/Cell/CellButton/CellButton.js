import React, { useRef, useContext, useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import { PanResponder } from "react-native";
import { SettingsContext } from "../../../SettingsHook/SettingsHook";
import useCellButtonStyle from "./CellButtonStyle";
import { AnimationContext } from "../../../AnimationHook/AnimationHook";
import PermutationIndicators from "./PermutationIndicators/PermutationIndicators";
import {
  getHypotenuseLength,
  getAngleDeg,
  isAngleInAngleRange,
  isAngleDirectionNegativeY,
  isAngleDirectionPositiveY,
  isAngleDirectionNegativeX,
  isAngleDirectionPositiveX,
  doesAngleRangeRepresentDirectionPositiveX,
  doesAngleRangeRepresentDirectionNegativeX,
  doesAngleRangeRepresentDirectionPositiveY, doesAngleRangeRepresentDirectionNegativeY,
} from "./geoFunctions";

function emptyFunction() {
  return undefined;
}

const CellButton = ({
  onFocus = emptyFunction,
  onBlur = emptyFunction,
  highlightThisGoesTo = emptyFunction,
  highlightGoesToThis = emptyFunction,
  highlightThisGoesToMode = false,
  highlightGoesToThisMode = false,
  disabled = false,
  width,
  height,
  row,
  column,
  applyPermutation,
  permutationFamily,
  isPermutationWithIndexExecutable,
  permutationIndicatorColor,
}) => {
  const registerSwipeThreshold = 25;
  const angleToDetectSwipeAlongAxis = 80;
  const initialSwipeDirectionBonus = 150;
  const { animateNext } = useContext(AnimationContext);
  const { settings } = useContext(SettingsContext);
  const {
    longPressDuration,
    preferenceFirstSwipeDirection,
    pressAction,
    longPressAction,
    swipeAction,
    permutationIndicatorsCenter,
    showPermutationIndicators,
  } = settings;
  const [
    showingPermutationIndicators,
    setShowingPermutationIndicators,
  ] = useState(false);
  const [focusedPermutationIndex, setFocusedPermutationIndex] = useState(null);

  const longPressTimeout = useRef();
  const initialSwipeDirection = useRef(null);
  const initialDxFromCenterButton = useRef(0);
  const initialDyFromCenterButton = useRef(0);
  const initialPageX = useRef(0);
  const initialPageY = useRef(0);
  const dxFromPressLocation = useRef(0);
  const dyFromPressLocation = useRef(0);
  const dxFromCenterButton = useRef(0);
  const dyFromCenterButton = useRef(0);
  const pressing = useRef(false);
  const longPressing = useRef(false);
  const swiping = useRef(false);
  const completed = useRef(false);

  if (disabled) {
    return null;
  }

  const initializeStates = () => {
    pressing.current = false;
    longPressing.current = false;
    swiping.current = false;
    completed.current = false;
    setShowingPermutationIndicators(false);
    setFocusedPermutationIndex(null);
    initialSwipeDirection.current = null;
  };

  const initializeRefs = (evt, gestureState) => {
    initialDxFromCenterButton.current = evt.nativeEvent.locationX - width / 2;
    initialDyFromCenterButton.current = evt.nativeEvent.locationY - height / 2;
    initialPageX.current = evt.nativeEvent.pageX;
    initialPageY.current = evt.nativeEvent.pageY;

    updateRefs(evt, gestureState);
  };

  const updateRefs = (evt) => {
    dxFromPressLocation.current = evt.nativeEvent.pageX - initialPageX.current;
    dyFromPressLocation.current = evt.nativeEvent.pageY - initialPageY.current;
    dxFromCenterButton.current =
      initialDxFromCenterButton.current + dxFromPressLocation.current;
    dyFromCenterButton.current =
      initialDyFromCenterButton.current + dyFromPressLocation.current;

    if (
      (preferenceFirstSwipeDirection === "always" ||
        preferenceFirstSwipeDirection === "unlessPermutationIndicators") &&
      !initialSwipeDirection.current &&
      (swiping.current || didSwipe())
    ) {
      const angle = getAngleSwipe();
      if (
        isAngleDirectionPositiveX(angle, angleToDetectSwipeAlongAxis) &&
        !permutationFamily.permutations.some((permutation) =>
          doesAngleRangeRepresentDirectionPositiveX(permutation.swipeAngle)
        )
      ) {
        initialSwipeDirection.current = "right";
      } else if (
        isAngleDirectionNegativeX(angle, angleToDetectSwipeAlongAxis) &&
        !permutationFamily.permutations.some((permutation) =>
          doesAngleRangeRepresentDirectionNegativeX(permutation.swipeAngle)
        )
      ) {
        initialSwipeDirection.current = "left";
      } else if (
        isAngleDirectionPositiveY(angle, angleToDetectSwipeAlongAxis) &&
        !permutationFamily.permutations.some((permutation) =>
          doesAngleRangeRepresentDirectionPositiveY(permutation.swipeAngle)
        )
      ) {
        initialSwipeDirection.current = "up";
      } else if (
        isAngleDirectionNegativeY(angle, angleToDetectSwipeAlongAxis) &&
        !permutationFamily.permutations.some((permutation) =>
          doesAngleRangeRepresentDirectionNegativeY(permutation.swipeAngle)
        )
      ) {
        initialSwipeDirection.current = "down";
      } else {
        initialSwipeDirection.current = "not_along_main_axis";
      }
    }

    if (
      (preferenceFirstSwipeDirection === "always" ||
        (preferenceFirstSwipeDirection === "unlessPermutationIndicators" &&
          !showingPermutationIndicators)) &&
      didSwipe()
    ) {
      switch (initialSwipeDirection.current) {
        case "right":
          dxFromPressLocation.current += initialSwipeDirectionBonus;
          dxFromCenterButton.current += initialSwipeDirectionBonus;
          break;
        case "left":
          dxFromPressLocation.current -= initialSwipeDirectionBonus;
          dxFromCenterButton.current -= initialSwipeDirectionBonus;
          break;
        case "up":
          dyFromPressLocation.current -= initialSwipeDirectionBonus;
          dyFromCenterButton.current -= initialSwipeDirectionBonus;
          break;
        case "down":
          dyFromPressLocation.current += initialSwipeDirectionBonus;
          dyFromCenterButton.current += initialSwipeDirectionBonus;
          break;
        default:
          break;
      }
    }
  };

  const clearState = () => {
    pressing.current = false;
    longPressing.current = false;
    swiping.current = false;
    completed.current = false;
    setShowingPermutationIndicators(false);
    setFocusedPermutationIndex(null);
    clearTimeout(longPressTimeout.current);
  };

  const didSwipe = () => {
    return getDistanceSwipe() > registerSwipeThreshold;
  };

  const getDistanceSwipe = () => {
    return getHypotenuseLength(
      dxFromPressLocation.current,
      dyFromPressLocation.current
    );
  };

  const getDistanceFromCenter = () => {
    return getHypotenuseLength(
      dxFromCenterButton.current,
      dyFromCenterButton.current
    );
  };

  const getAngleSwipe = () => {
    return getAngleDeg(
      dxFromPressLocation.current,
      dyFromPressLocation.current
    );
  };

  const getAngleFromCenter = () => {
    return getAngleDeg(dxFromCenterButton.current, dyFromCenterButton.current);
  };

  const findIndexPermutationBasedOnAngle = (angle) => {
    return permutationFamily.permutations.findIndex(({ swipeAngle }) => {
      return isAngleInAngleRange(angle, swipeAngle);
    });
  };

  const findAndExecutePermutationBasedOnAngle = (angle) => {
    const index = findIndexPermutationBasedOnAngle(angle);
    index !== undefined &&
      index !== null &&
      index >= 0 &&
      isPermutationWithIndexExecutable(index) &&
      applyPermutationWithAnimation(index);
  };

  const computeAndSetFocusedPermutationIndex = () => {
    let distance, angle;
    if (permutationIndicatorsCenter === "location") {
      distance = getDistanceSwipe();
      angle = getAngleSwipe();
    } else {
      distance = getDistanceFromCenter();
      angle = getAngleFromCenter();
    }
    if (distance > registerSwipeThreshold) {
      setFocusedPermutationIndex(findIndexPermutationBasedOnAngle(angle));
    } else {
      setFocusedPermutationIndex(null);
    }
  };

  const highlightIfHighlightMode = () => {
    if (highlightGoesToThisMode) {
      highlightGoesToThis();
      return true;
    } else if (highlightThisGoesToMode) {
      highlightThisGoesTo();
      return true;
    }
    return false;
  };

  const shouldStartShowingPermutationIndicators = (pressOrLongPressString) => {
    return (
      swipeAction === "swipePermutation" &&
      permutationFamily.permutations.length >= 2 &&
      (showPermutationIndicators === pressOrLongPressString ||
        (showPermutationIndicators ===
          pressOrLongPressString + "ManyPermutations" &&
          permutationFamily.permutations.length > 4))
    );
  };

  const onPressDetected = () => {
    pressing.current = true;
    if (shouldStartShowingPermutationIndicators("press")) {
      onShowPermutationIndicators();
    }
  };

  const onPress = () => {
    if (!highlightIfHighlightMode()) {
      switch (pressAction) {
        case "locationPermutation":
          findAndExecutePermutationBasedOnAngle(getAngleFromCenter());
          break;
        case "highlightThisGoesTo":
          highlightThisGoesTo();
          break;
        case "highlightGoesToThis":
          highlightGoesToThis();
          break;
        default:
          break;
      }
    }
    onComplete();
  };

  const onLongPressDetected = () => {
    longPressing.current = true;
    if (swiping.current) {
      if (shouldStartShowingPermutationIndicators("longPress")) {
        onShowPermutationIndicators();
      }
    } else {
      onLongPress();
    }
  };

  const onLongPress = () => {
    if (!highlightIfHighlightMode()) {
      switch (longPressAction) {
        case "locationPermutation":
          findAndExecutePermutationBasedOnAngle(getAngleFromCenter());
          break;
        case "highlightThisGoesTo":
          highlightThisGoesTo();
          break;
        case "highlightGoesToThis":
          highlightGoesToThis();
          break;
        default:
          break;
      }
    }
    onComplete();
  };

  const onSwipeDetected = () => {
    swiping.current = true;
  };

  const onSwipe = () => {
    switch (swipeAction) {
      case "swipePermutation":
        if (
          showPermutationIndicators &&
          permutationIndicatorsCenter === "cell"
        ) {
          findAndExecutePermutationBasedOnAngle(getAngleFromCenter());
        } else {
          findAndExecutePermutationBasedOnAngle(getAngleSwipe());
        }
        break;
      default:
        break;
    }
    onComplete();
  };

  const onShowPermutationIndicators = () => {
    setShowingPermutationIndicators(true);
  };

  const onComplete = () => {
    completed.current = true;
    setShowingPermutationIndicators(false);
    onBlur();
  };

  const applyPermutationWithAnimation = (permutationIndex) => {
    animateNext();
    applyPermutation(row, column, permutationIndex);
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => {
      return true;
    },
    onMoveShouldSetPanResponder: () => {
      return true;
    },
    onPanResponderGrant: (evt, gestureState) => {
      initializeStates();
      initializeRefs(evt, gestureState);
      onFocus();
      onPressDetected();
      longPressTimeout.current = setTimeout(
        onLongPressDetected,
        longPressDuration
      );
    },
    onPanResponderMove: (evt, gestureState) => {
      updateRefs(evt, gestureState);
      if (didSwipe()) {
        onSwipeDetected();
      }
      if (showingPermutationIndicators) {
        computeAndSetFocusedPermutationIndex();
      }
    },
    onPanResponderRelease: (evt, gestureState) => {
      updateRefs(evt, gestureState);
      if (!completed.current) {
        if (swiping.current) {
          if (didSwipe()) {
            onSwipe();
          } else {
            // if user first swipes, but then goes back to center, then don't do anything.
            onComplete();
          }
        } else {
          onPress();
        }
      }
      clearState();
    },
  });

  const style = useCellButtonStyle();
  return (
    <View {...panResponder.panHandlers} style={style.root}>
      {showingPermutationIndicators && (
        <PermutationIndicators
          permutationIndicatorsCenter={permutationIndicatorsCenter}
          initialDxFromCenterButton={initialDxFromCenterButton.current}
          initialDyFromCenterButton={initialDyFromCenterButton.current}
          width={width}
          height={height}
          permutationFamily={permutationFamily}
          isPermutationWithIndexExecutable={isPermutationWithIndexExecutable}
          focusedPermutationIndex={focusedPermutationIndex}
          permutationIndicatorColor={permutationIndicatorColor}
        />
      )}
    </View>
  );
};

export default React.memo(CellButton);
