import { StyleSheet } from "react-native";

function useStyle() {
  return StyleSheet.create({
    root: {
      ...StyleSheet.absoluteFill,
      zIndex: 200,
    },
  });
}

export default useStyle;
