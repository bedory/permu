import { StyleSheet } from "react-native";
import useStyleVariables from "../../StyleVariablesHook/StyleVariablesHook";

function useStyle(width, height, top, left) {
  return StyleSheet.create({
    root: {
      position: "absolute",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      padding: 2,
      width,
      height,
      top,
      left,
    },
  });
}

export default useStyle;
