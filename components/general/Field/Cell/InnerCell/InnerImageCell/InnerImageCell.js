import React from "react";
import { Image, View } from "react-native";
import useInnerImageStyle from "./InnerImageCellStyle";

const InnerImageCell = ({
  correctPosition,
  highlighted,
  disabled,
  width,
  height,
  imageObject,
  correctRightNeighbor,
  correctBottomNeighbor,
  imageTop,
  imageLeft,
  fieldWidth,
  fieldHeight,
}) => {
  const style = useInnerImageStyle(
    width,
    height,
    disabled,
    highlighted,
    correctPosition,
    correctBottomNeighbor,
    correctRightNeighbor,
    fieldWidth,
    fieldHeight,
    imageTop,
    imageLeft
  );

  return (
    <View style={style.root}>
      <Image style={style.image} source={{ uri: imageObject.url }} />
    </View>
  );
};

export default React.memo(InnerImageCell);
