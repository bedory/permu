import { StyleSheet } from "react-native";
import useStyleVariables from "../../../../StyleVariablesHook/StyleVariablesHook";

function useStyle(
  width,
  height,
  disabled,
  highlighted,
  correctPosition,
  correctBottomNeighbor,
  correctRightNeighbor,
  fieldWidth,
  fieldHeight,
  imageTop,
  imageLeft
) {
  const {
    disabledOpacity,
    highlightColor,
    backgroundColor,
    showBorder,
  } = useStyleVariables();
  let borderLeftWidth = 0;
  let borderRightWidth = 0;
  let borderTopWidth = 0;
  let borderBottomWidth = 0;
  if (highlighted && !correctPosition) {
    borderBottomWidth = 5;
    borderLeftWidth = 4;
    borderRightWidth = 5;
    borderTopWidth = 4;
  } else {
    if (
      showBorder === "always" ||
      (showBorder === "incorrectNeighbors" && !correctBottomNeighbor)
    ) {
      borderBottomWidth = 2;
    }
    if (
      showBorder === "always" ||
      (showBorder === "incorrectNeighbors" && !correctRightNeighbor)
    ) {
      borderRightWidth = 2;
    }
  }

  return StyleSheet.create({
    root: {
      overflow: "hidden",
      width: width,
      height: height,
      opacity: disabled ? disabledOpacity : 1.0,
      borderRightWidth,
      borderLeftWidth,
      borderTopWidth,
      borderBottomWidth,
      borderColor:
        highlighted && !correctPosition ? highlightColor : backgroundColor,
    },
    image: {
      width: fieldWidth,
      height: fieldHeight,
      position: "absolute",
      top: -imageTop - borderTopWidth,
      left: -imageLeft - borderLeftWidth,
    },
  });
}

export default useStyle;
