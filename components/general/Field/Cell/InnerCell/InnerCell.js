import React from "react";
import InnerBlankCell from "./InnerBlankCell/InnerBlankCell";
import InnerImageCell from "./InnerImageCell/InnerImageCell";

const InnerCell = (props) => {
  if (props.imageObject) {
    return <InnerImageCell {...props} />;
  } else {
    return <InnerBlankCell {...props} />;
  }
};

export default React.memo(InnerCell);
