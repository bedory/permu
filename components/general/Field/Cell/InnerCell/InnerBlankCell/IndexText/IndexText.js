import React from "react";
import { View } from "react-native";
import useIndexStyle from "./IndexTextStyle";
import StyledText from "../../../../../StyledText/StyledText";

const IndexText = ({ row, column, fontSize }) => {
  const style = useIndexStyle(fontSize);
  return (
    <View style={style.root}>
      <StyledText style={style.column}>{column}</StyledText>
      <StyledText style={style.row}>{row}</StyledText>
    </View>
  );
};

export default React.memo(IndexText);
