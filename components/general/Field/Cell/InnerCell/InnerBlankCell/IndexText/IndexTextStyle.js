import { StyleSheet } from "react-native";

function useStyle(fontSize) {
  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
      alignItems: "stretch",
      width: "100%",
      height: "100%",
    },
    column: {
      textAlign: "right",
      fontSize: fontSize,
      lineHeight: fontSize + 2,
    },
    row: {
      textAlign: "left",
      fontSize: fontSize,
      lineHeight: fontSize + 2,
    },
  });
}

export default useStyle;
