import { StyleSheet } from "react-native";
import useStyleVariables from "../../../../StyleVariablesHook/StyleVariablesHook";

function useStyle(highlighted, correctPosition, disabled, width) {
  const {
    cellBackgroundColor,
    highlightColor,
    correctColor,
    disabledOpacity,
  } = useStyleVariables();
  let bgColor = cellBackgroundColor;
  if (highlighted) {
    bgColor = highlightColor;
  }
  if (correctPosition) {
    bgColor = correctColor;
  }

  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      padding: 3,
      width: "100%",
      height: "100%",
      backgroundColor: bgColor,
      opacity: disabled ? disabledOpacity : 1.0,
      borderRadius: width ? width / 8 : 5,
    },
  });
}

export default useStyle;
