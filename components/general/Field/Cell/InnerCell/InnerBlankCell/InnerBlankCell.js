import React from "react";
import { View } from "react-native";
import IndexText from "./IndexText/IndexText";
import useStyle from "./InnerBlankCellStyle";

const InnerBlankCell = ({
  row,
  column,
  correctPosition,
  highlighted,
  disabled,
  showIndex,
  width,
  height,
}) => {
  const style = useStyle(highlighted, correctPosition, disabled, width);

  return (
    <View style={style.root}>
      {showIndex && (
        <IndexText
          row={row}
          column={column}
          fontSize={Math.min(width, height) * 0.35}
        />
      )}
    </View>
  );
};

export default React.memo(InnerBlankCell);
