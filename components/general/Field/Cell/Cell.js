import React, { useState, useCallback } from "react";
import useCellStyle from "./CellStyle";
import { View } from "react-native";
import CellButton from "./CellButton/CellButton";
import InnerCell from "./InnerCell/InnerCell";
import SwipeAnimation from "./SwipeAnimation/SwipeAnimation";

const Cell = ({
  row,
  column,
  rowValue,
  columnValue,
  correctPosition = false,
  correctRightNeighbor = false,
  correctBottomNeighbor = false,
  highlighted = false,
  disabled = false,
  showIndex = true,
  buttonDisabled,
  highlightThisGoesTo,
  highlightGoesToThis,
  highlightThisGoesToMode,
  highlightGoesToThisMode,
  width,
  height,
  top,
  left,
  fieldWidth,
  fieldHeight,
  imageTop,
  imageLeft,
  imageObject,
  permutationFamily,
  applyPermutation,
  isPermutationWithIndexExecutable,
  showSwipeAnimation,
  swipeAngle,
  swipeDuration,
  permutationIndicatorColor,
}) => {
  const [focused, setFocused] = useState(false);
  const onFocus = useCallback(() => setFocused(true), []);
  const onBlur = useCallback(() => setFocused(false), []);
  const style = useCellStyle(width, height, top, left);

  return (
    <View style={style.root}>
      {showSwipeAnimation && (
        <SwipeAnimation
          width={width}
          height={height}
          swipeAngle={swipeAngle}
          swipeDuration={swipeDuration}
        />
      )}
      <CellButton
        onFocus={onFocus}
        onBlur={onBlur}
        disabled={buttonDisabled}
        width={width}
        height={height}
        row={row}
        column={column}
        permutationFamily={permutationFamily}
        applyPermutation={applyPermutation}
        highlightThisGoesTo={highlightThisGoesTo}
        highlightGoesToThis={highlightGoesToThis}
        highlightThisGoesToMode={highlightThisGoesToMode}
        highlightGoesToThisMode={highlightGoesToThisMode}
        isPermutationWithIndexExecutable={isPermutationWithIndexExecutable}
        permutationIndicatorColor={permutationIndicatorColor}
      />
      <InnerCell
        row={rowValue}
        column={columnValue}
        correctPosition={correctPosition}
        correctRightNeighbor={correctRightNeighbor}
        correctBottomNeighbor={correctBottomNeighbor}
        highlighted={highlighted}
        disabled={disabled || focused}
        showIndex={showIndex}
        width={width}
        height={height}
        imageObject={imageObject}
        imageTop={imageTop}
        imageLeft={imageLeft}
        fieldWidth={fieldWidth}
        fieldHeight={fieldHeight}
      />
    </View>
  );
};

export default React.memo(Cell);
