function copyField(field) {
  return field.map((row) => row.slice());
}

// TODO: optimize this function with constant values
function generateField(numberRows, numberColumns) {
  let field = [];
  for (let row = 0; row < numberRows; row++) {
    field.push([]);
    for (let column = 0; column < numberColumns; column++) {
      field[row].push({ row: row, column: column });
    }
  }
  return field;
}

function getRelativeCell(field, row, column, relativeRow, relativeColumn) {
  const numberRows = field.length;
  const numberColumns = field[0].length;
  return field[(row + relativeRow + numberRows) % numberRows][
    (column + relativeColumn + numberColumns) % numberColumns
  ];
}

function correctRelativeCell(field, row, column, relativeRow, relativeColumn) {
  let cell = field[row][column];
  let relativeCell = getRelativeCell(
    field,
    row,
    column,
    relativeRow,
    relativeColumn
  );
  return (
    relativeCell.row === cell.row + relativeRow &&
    relativeCell.column === cell.column + relativeColumn
  );
}

function printField(field) {
  let verticalLine = field[0].reduce((str, element) => str + "---", "-");
  console.log(verticalLine);
  field.forEach((row) => {
    let rowString = row.reduce((str, element, index) => {
      let value = element.row.toString() + element.column.toString();
      if (index === row.length - 1) {
        return str + value;
      } else {
        return str + value + " ";
      }
    }, "");
    console.log("|" + rowString + "|");
  });
  console.log(verticalLine);
}

export {
  copyField,
  generateField,
  printField,
  getRelativeCell,
  correctRelativeCell,
};
