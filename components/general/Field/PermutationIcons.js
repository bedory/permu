import CurvedArrow from "../../../assets/permutationIcons/curved_arrow.svg";
import Dot from "../../../assets/permutationIcons/dot.svg";
import LongArrow from "../../../assets/permutationIcons/long_arrow.svg";
import Rotate from "../../../assets/permutationIcons/rotate.svg";
import ShortArrow from "../../../assets/permutationIcons/short_arrow.svg";
const PermutationIconMap = {
  none: false,
  CurvedArrow,
  Dot,
  LongArrow,
  Rotate,
  ShortArrow,
};

export default PermutationIconMap;
