import React, { useState, useCallback, useRef } from "react";
import { copyField, generateField } from "./FieldFunctions";

const useField = (numberRows, numberColumns, maxNumberUndos) => {
  const initialField = useRef(null);
  if (!initialField.current) {
    initialField.current = generateField(numberRows, numberColumns);
  }

  const fieldHistory = useRef();
  const [currentHistoryIndex, setCurrentHistoryIndex] = useState(0);
  const [upBoundaryHistoryIndex, setUpBoundaryHistoryIndex] = useState(0);
  const [lowBoundaryHistoryIndex, setLowBoundaryHistoryIndex] = useState(0);
  const [
    thisStateIsUsedToForceFieldRerender,
    setStateForceFieldRerender,
  ] = useState(true);

  const forceFieldRerender = () =>
    setStateForceFieldRerender((prevValue) => !prevValue);

  const reset = useCallback(
    (field = copyField(initialField.current)) => {
      fieldHistory.current = new Array(maxNumberUndos + 1);
      fieldHistory.current[0] = field;
      setCurrentHistoryIndex(0);
      setLowBoundaryHistoryIndex(0);
      setUpBoundaryHistoryIndex(0);
      forceFieldRerender();
    },
    [maxNumberUndos]
  );

  if (!fieldHistory.current) {
    reset();
  }

  const getCurrentField = useCallback(
    () => fieldHistory.current[currentHistoryIndex % (maxNumberUndos + 1)],
    [currentHistoryIndex, maxNumberUndos, thisStateIsUsedToForceFieldRerender]
  );

  const setNextField = useCallback(
    (fieldOrFunction) => {
      if (typeof fieldOrFunction === "function") {
        setCurrentHistoryIndex((prevIndex) => {
          const field = fieldOrFunction(
            fieldHistory.current[prevIndex % (maxNumberUndos + 1)]
          );
          const newCurrentHistoryIndex = prevIndex + 1;
          fieldHistory.current[
            newCurrentHistoryIndex % (maxNumberUndos + 1)
          ] = field;
          setUpBoundaryHistoryIndex(newCurrentHistoryIndex);
          setLowBoundaryHistoryIndex((prevLowBoundary) =>
            Math.max(prevLowBoundary, newCurrentHistoryIndex - maxNumberUndos)
          );
          return newCurrentHistoryIndex;
        });
      } else {
        setCurrentHistoryIndex((prevIndex) => {
          const newCurrentHistoryIndex = prevIndex + 1;
          fieldHistory.current[
            newCurrentHistoryIndex % (maxNumberUndos + 1)
          ] = fieldOrFunction;
          setUpBoundaryHistoryIndex(newCurrentHistoryIndex);
          setLowBoundaryHistoryIndex((prevLowBoundary) =>
            Math.max(prevLowBoundary, newCurrentHistoryIndex - maxNumberUndos)
          );
          return newCurrentHistoryIndex;
        });
      }
    },
    [maxNumberUndos]
  );

  const checkIfFinished = useCallback(() => {
    return !getCurrentField().some((row, rowIndex) => {
      return row.some((cellProps, columnIndex) => {
        return cellProps.row !== rowIndex || cellProps.column !== columnIndex;
      });
    });
  }, [getCurrentField]);

  const canUndo = useCallback(
    () => currentHistoryIndex > lowBoundaryHistoryIndex,
    [currentHistoryIndex, lowBoundaryHistoryIndex]
  );

  const undo = useCallback(() => {
    setCurrentHistoryIndex((prevIndex) => prevIndex - 1);
  }, []);

  const canRedo = useCallback(
    () => currentHistoryIndex < upBoundaryHistoryIndex,
    [currentHistoryIndex, upBoundaryHistoryIndex]
  );

  const redo = useCallback(() => {
    setCurrentHistoryIndex((prevIndex) => prevIndex + 1);
  }, []);

  return {
    numberRows,
    numberColumns,
    field: getCurrentField(),
    initialField: initialField.current,
    numberSteps: currentHistoryIndex,
    undo,
    redo,
    canUndo: canUndo(),
    canRedo: canRedo(),
    setNextField,
    finished: checkIfFinished(),
    reset,
  };
};

export default useField;
