import React, {
  useState,
  useRef,
  useContext,
  useEffect,
  useCallback,
} from "react";
import useInterval from "../IntervalHook/IntervalHook";
import { FieldWithPermutationPreviewContext } from "./FieldWithPermutationPreviewHook";
import Field from "./Field";
import { useIsFocused } from "@react-navigation/native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import { AnimationContext } from "../AnimationHook/AnimationHook";
import { SettingsContext } from "../SettingsHook/SettingsHook";
import { getHalfAngle } from "./Cell/CellButton/geoFunctions";

const FieldPreview = ({ imageObject, ...fieldProps }) => {
  const { windowWidth, windowHeight } = useStyleVariables();
  const {
    field,
    permutationFamily,
    applyPermutationOnCenter,
    getCenter,
  } = useContext(FieldWithPermutationPreviewContext);
  const { animateNext } = useContext(AnimationContext);
  const { settings } = useContext(SettingsContext);
  const { applyPreviewPermutation, intervalPreviewPermutation } = settings;

  const [centerPressed, setCenterPressed] = useState(false);
  const timeBeforeStartingFirstPermutationApply = 500;
  const timeBetweenPressingCenterAndApplyingPermutation = 400;
  const permutationAnimationDuration = 400;

  const applyPermutationOnCenterWithPermutationAnimation = () => {
    animateNext(true, permutationAnimationDuration);
    applyPermutationOnCenter();
    permutationFamily.setNextPermutation();
  };

  let centerPressedTimeout = useRef(null);
  let centerPermutationTimeout = useRef(null);
  const applyPermutationCenterWithPressAnimation = () => {
    clearTimeout(centerPressedTimeout.current);
    clearTimeout(centerPermutationTimeout.current);
    setCenterPressed(true);
    centerPressedTimeout.current = setTimeout(() => {
      setCenterPressed(false);
      applyPermutationOnCenterWithPermutationAnimation();
    }, timeBetweenPressingCenterAndApplyingPermutation);
  };

  const [resetPermutationInterval, clearPermutationInterval] = useInterval(
    applyPermutationCenterWithPressAnimation,
    intervalPreviewPermutation
  );

  const firstPressTimeout = useRef(null);
  const resetPermutationIntervalWithQuickFirstPermutation = () => {
    firstPressTimeout.current = setTimeout(() => {
      applyPermutationCenterWithPressAnimation();
      resetPermutationInterval();
    }, timeBeforeStartingFirstPermutationApply);
  };

  const isFocused = useIsFocused();

  // reset when changing permutation or field
  useEffect(() => {
    clearTimeout(centerPermutationTimeout.current);
    clearTimeout(centerPressedTimeout.current);
    clearTimeout(firstPressTimeout.current);
    if (isFocused && applyPreviewPermutation) {
      field.reset();
      resetPermutationIntervalWithQuickFirstPermutation();
    } else {
      clearPermutationInterval();
    }
    return clearPermutationInterval;
  }, [
    permutationFamily.permutationFamily,
    field.numberColumns,
    field.numberRows,
    isFocused,
  ]);

  const getCellProps = useCallback(
    (row, column) => ({
      disabled:
        row === getCenter().row &&
        column === getCenter().column &&
        centerPressed,
      buttonDisabled: true,
      showIndex: !imageObject,
      correctPosition: false,
      correctBottomNeighbor: false,
      correctRightNeighbor: false,
      showSwipeAnimation:
        row === getCenter().row &&
        column === getCenter().column &&
        centerPressed,
      swipeAngle: getHalfAngle(
        permutationFamily.getCurrentPermutation().swipeAngle
      ),
      swipeDuration: timeBetweenPressingCenterAndApplyingPermutation,
    }),
    [getCenter, centerPressed, imageObject]
  );

  return (
    <Field
      field={field.field}
      maxWidth={0.9 * windowWidth}
      maxHeight={0.55 * windowHeight}
      cellPropsCallback={getCellProps}
      imageObject={imageObject}
      {...fieldProps}
    />
  );
};

export default FieldPreview;
