import React, {
  useContext,
  useRef,
  useState,
  useCallback,
} from "react";
import Field from "./Field";
import {
  FieldContext,
  FieldWithPermutationContext,
  PermutationFamilyContext,
} from "./FieldWithPermutationHook";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";
import { SettingsContext } from "../SettingsHook/SettingsHook";

// only responsible for displaying field. All the logic is in the FieldHook
const FieldGame = ({
  turnHighlightThisGoesToModeOff,
  turnHighlightGoesToThisModeOff,
  imageObject,
  highlightThisGoesToMode = false,
  highlightGoesToThisMode = false,
  showEndResult = false,
  ...fieldProps
}) => {
  const { landscape } = useStyleVariables();
  const { windowWidth, windowHeight } = useStyleVariables();

  const { field, initialField } = useContext(FieldContext);
  const {
    applyPermutation,
    isPermutationWithIndexExecutableOnCell,
  } = useContext(FieldWithPermutationContext);
  const { permutationFamily } = useContext(PermutationFamilyContext);
  const { settings } = useContext(SettingsContext);
  const { highlightIndefinitely, highlightDuration } = settings;
  const [highlightedCell, setHighlightedCell] = useState();
  const highlightedCellTimeout = useRef();

  const setHighlight = useCallback(
    (row, column, type) => {
      setHighlightedCell({ row, column, type });
      if (!highlightIndefinitely) {
        clearTimeout(highlightedCellTimeout.current);
        highlightedCellTimeout.current = setTimeout(() => {
          setHighlightedCell(null);
        }, highlightDuration);
      }
    },
    [highlightIndefinitely]
  );

  const getHighlightThisGoesToCallback = (row, column, rowValue, columnValue) =>
    useCallback(() => {
      setHighlight(rowValue, columnValue, "index");
      turnHighlightThisGoesToModeOff();
      turnHighlightGoesToThisModeOff();
    }, [rowValue, columnValue]);

  const getHighlightGoesToThisCallback = (row, column) =>
    useCallback(() => {
      setHighlight(row, column, "value");
      turnHighlightThisGoesToModeOff();
      turnHighlightGoesToThisModeOff();
    }, [row, column]);

  const isCellHighlighted = useCallback(
    (row, column, rowValue, columnValue) => {
      if (highlightedCell) {
        if (highlightedCell.type === "index") {
          return (
            highlightedCell.row === row && highlightedCell.column === column
          );
        } else if (highlightedCell.type === "value") {
          return (
            highlightedCell.row === rowValue &&
            highlightedCell.column === columnValue
          );
        }
      }
      return false;
    },
    [highlightedCell]
  );

  const getIsPermutationWithIndexExecutableCallback = (row, column) =>
    useCallback(
      (permutationIndex) =>
        isPermutationWithIndexExecutableOnCell(row, column, permutationIndex),
      [row, column]
    );

  const getCellProps = useCallback(
    (row, column, rowValue, columnValue) => {
      return {
        highlighted: isCellHighlighted(row, column, rowValue, columnValue),
        showIndex: !imageObject,
        highlightThisGoesTo: getHighlightThisGoesToCallback(
          row,
          column,
          rowValue,
          columnValue
        ),
        highlightGoesToThis: getHighlightGoesToThisCallback(
          row,
          column,
          rowValue,
          columnValue
        ),
        highlightThisGoesToMode: highlightThisGoesToMode,
        highlightGoesToThisMode: highlightGoesToThisMode,
        isPermutationWithIndexExecutable: getIsPermutationWithIndexExecutableCallback(
          row,
          column
        ),
      };
    },
    [
      imageObject,
      highlightedCell,
      highlightThisGoesToMode,
      highlightGoesToThisMode,
    ]
  );

  return (
    <Field
      field={showEndResult ? initialField : field}
      maxWidth={landscape ? windowWidth - 100 : 0.85 * windowWidth}
      maxHeight={landscape ? windowHeight * 0.9 : windowHeight - 100}
      cellPropsCallback={getCellProps}
      imageObject={imageObject}
      {...fieldProps}
      permutationFamily={permutationFamily}
      applyPermutation={applyPermutation}
    />
  );
};

export default React.memo(FieldGame);
