import {
  isPermutationSequenceClosed,
  getDimensionsPermutation,
  getMinMaxRelativeRowColumn,
  createExtendedPermutation,
} from "./PermutationFunctions";

function arePermutationFamilySequencesClosed(permutationFamily) {
  return permutationFamily.permutations.every((permutation) => {
    if (permutation.extended) {
      return permutation.closed;
    } else {
      return isPermutationSequenceClosed(permutation);
    }
  });
}

function getMinMaxRelativeRowColumnPermutationFamily(permutationFamily) {
  if (!arePermutationFamilySequencesClosed(permutationFamily)) {
    return {
      minRelativeRow: false,
      maxRelativeColumn: false,
      minRelativeColumn: false,
      maxRelativeRow: false,
    };
  }

  let minRelativeRowOfFamily = 0;
  let maxRelativeRowOfFamily = 0;
  let minRelativeColumnOfFamily = 0;
  let maxRelativeColumnOfFamily = 0;

  permutationFamily.permutations.forEach((permutation) => {
    const {
      minRelativeRow,
      maxRelativeRow,
      minRelativeColumn,
      maxRelativeColumn,
    } = permutation.extended
      ? permutation
      : getMinMaxRelativeRowColumn(permutation);

    minRelativeRowOfFamily = Math.min(minRelativeRow, minRelativeRowOfFamily);
    maxRelativeRowOfFamily = Math.max(maxRelativeRow, maxRelativeRowOfFamily);
    minRelativeColumnOfFamily = Math.min(
      minRelativeColumn,
      minRelativeColumnOfFamily
    );
    maxRelativeColumnOfFamily = Math.max(
      maxRelativeColumn,
      maxRelativeColumnOfFamily
    );
  });

  return {
    minRelativeRow: minRelativeRowOfFamily,
    maxRelativeRow: maxRelativeRowOfFamily,
    minRelativeColumn: minRelativeColumnOfFamily,
    maxRelativeColumn: maxRelativeColumnOfFamily,
  };
}

function getDimensionsPermutationFamily(permutationFamily) {
  if (!arePermutationFamilySequencesClosed(permutationFamily)) {
    return { width: false, height: false };
  }

  let widthOfFamily = 0;
  let heightOfFamily = 0;
  permutationFamily.permutations.forEach((permutation) => {
    const { width, height } = permutation.extended
      ? permutation
      : getDimensionsPermutation(permutation);
    widthOfFamily = Math.max(width, widthOfFamily);
    heightOfFamily = Math.max(height, heightOfFamily);
  });
  return { width: widthOfFamily, height: heightOfFamily };
}

function createExtendedPermutationFamily(permutationFamily) {
  const extendedPermutations = permutationFamily.permutations.map(
    (permutation) =>
      permutation.extended
        ? permutation
        : createExtendedPermutation(permutation)
  );
  const familyOfExtendedPermutations = {
    ...permutationFamily,
    permutations: extendedPermutations,
  };

  const closed = arePermutationFamilySequencesClosed(
    familyOfExtendedPermutations
  );

  return {
    ...familyOfExtendedPermutations,
    extended: true,
    numberOfPermutations: permutationFamily.permutations.length,
    closed,
    allowsNoWrap: closed,
    ...getMinMaxRelativeRowColumnPermutationFamily(
      familyOfExtendedPermutations
    ),
    ...getDimensionsPermutationFamily(familyOfExtendedPermutations),
  };
}

export {
  arePermutationFamilySequencesClosed,
  getDimensionsPermutationFamily,
  createExtendedPermutationFamily,
};
