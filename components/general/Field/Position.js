function createLocation(row, column) {
  return {
    row: row,
    column: column,
  };
}

function createRelativeLocation(row, column) {
  return {
    relativeRow: row,
    relativeColumn: column,
  };
}

function createLocationTranslation(row, column, newRow, newColumn) {
  return {
    location: createLocation(row, column),
    newLocation: createLocation(newRow, newColumn),
  };
}

function createLocationTranslationWithLocationInput(location, newLocation) {
  return {location, newLocation};
}

function getNeighboringLocations(row, column, numberRows, numberColumns) {
  const columnPlus1 = (column + 1) % numberColumns;
  const columnMin1 = (column - 1 + numberColumns) % numberColumns;
  const rowPlus1 = (row + 1) % numberRows;
  const rowMin1 = (row - 1 + numberRows) % numberRows;
  return {
    topLeft: createLocation(rowMin1, columnMin1),
    topMiddle: createLocation(rowMin1, column),
    topRight: createLocation(rowMin1, columnPlus1),
    middleLeft: createLocation(row, columnMin1),
    middleMiddle: createLocation(row, column),
    middleRight: createLocation(row, columnPlus1),
    bottomLeft: createLocation(rowPlus1, columnMin1),
    bottomMiddle: createLocation(rowPlus1, column),
    bottomRight: createLocation(rowPlus1, columnPlus1),
  };
}

export {
  createLocation,
  createRelativeLocation,
  createLocationTranslation,
  createLocationTranslationWithLocationInput,
  getNeighboringLocations,
};
