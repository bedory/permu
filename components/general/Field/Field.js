import React from "react";
import useStyle from "./FieldStyle";
import { View, StyleSheet } from "react-native";
import Cell from "./Cell/Cell";
import Caption from "../Caption/Caption";
import { correctRelativeCell } from "./FieldFunctions";

const emptyCallback = () => undefined;

const Field = ({
  maxWidth,
  maxHeight,
  field,
  imageObject,
  style,
  cellPropsCallback = emptyCallback,
  applyPermutation,
  permutationFamily,
}) => {
  const numberRows = field.length;
  const numberColumns = field[0].length;

  let width, height, cellWidth, cellHeight;
  if (imageObject) {
    if (maxHeight / maxWidth > imageObject.height / imageObject.width) {
      width = maxWidth;
      height = (width * imageObject.height) / imageObject.width;
    } else {
      height = maxHeight;
      width = (height * imageObject.width) / imageObject.height;
    }
    cellWidth = width / numberColumns;
    cellHeight = height / numberRows;
  } else {
    const maxCellWidth = maxWidth / numberColumns;
    const maxCellHeight = maxHeight / numberRows;
    const minCellDim = Math.min(maxCellWidth, maxCellHeight);
    cellWidth = minCellDim;
    cellHeight = minCellDim;
    width = cellWidth * numberColumns;
    height = cellHeight * numberRows;
  }

  const internalStyle = useStyle(
    width,
    height,
    numberColumns * cellWidth,
    numberRows * cellHeight,
    cellWidth
  );

  let gridDom = field.reduce((result, row, rowIndex) => {
    let rowDom = row.map((cellValue, columnIndex) => {
      const rowValue = cellValue.row;
      const columnValue = cellValue.column;
      return (
        <Cell
          key={cellValue.row * 10 + cellValue.column}
          row={rowIndex}
          column={columnIndex}
          rowValue={rowValue}
          columnValue={columnValue}
          width={cellWidth + 1}
          height={cellHeight + 1}
          top={rowIndex * cellHeight}
          left={columnIndex * cellWidth}
          imageTop={cellValue.row * cellHeight}
          imageLeft={cellValue.column * cellWidth}
          fieldWidth={width}
          fieldHeight={height}
          permutationFamily={permutationFamily}
          applyPermutation={applyPermutation}
          correctPosition={
            cellValue.row === rowIndex && cellValue.column === columnIndex
          }
          correctRightNeighbor={correctRelativeCell(
            field,
            rowIndex,
            columnIndex,
            0,
            1
          )}
          correctBottomNeighbor={correctRelativeCell(
            field,
            rowIndex,
            columnIndex,
            1,
            0
          )}
          imageObject={imageObject}
          {...cellPropsCallback(rowIndex, columnIndex, rowValue, columnValue)}
        />
      );
    });
    return result.concat(rowDom);
  }, []);

  const fieldDom = <View style={internalStyle.field}>{gridDom}</View>;

  return (
    <View style={internalStyle.root}>
      <View style={StyleSheet.compose(internalStyle.container, style)}>
        {imageObject ? (
          fieldDom
        ) : (
          <View style={internalStyle.fieldContainer}>{fieldDom}</View>
        )}
      </View>
      {imageObject && imageObject.artist && (
        <Caption imageObject={imageObject} />
      )}
    </View>
  );
};

export default React.memo(Field);
