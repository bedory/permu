import React, { useCallback, useRef, useState } from "react";
import { createExtendedPermutationFamily } from "./PermutationFamilyFunctions";

const usePermutationFamilyPreview = (initialPermutationFamily) => {
  const [permutationFamily, setPermutationFamily] = useState(() =>
    initialPermutationFamily.extended
      ? initialPermutationFamily
      : createExtendedPermutationFamily(initialPermutationFamily)
  );
  const permutationIndex = useRef(0);

  const setPermutationFamilyAndExtend = (newPermutationFamily) => {
    permutationIndex.current = 0;
    setPermutationFamily(
      newPermutationFamily.extended
        ? newPermutationFamily
        : createExtendedPermutationFamily(newPermutationFamily)
    );
  };

  const increasePermutationIndex = () => {
    permutationIndex.current =
      (permutationIndex.current + 1) % permutationFamily.numberOfPermutations;
  };

  const getCurrentPermutation = useCallback(() => {
    return permutationFamily.permutations[permutationIndex.current];
  }, [permutationFamily]);

  return {
    setNextPermutation: increasePermutationIndex,
    getCurrentPermutation,
    setPermutationFamily: setPermutationFamilyAndExtend,
    permutationFamily: permutationFamily,
  };
};

export default usePermutationFamilyPreview;
