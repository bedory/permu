import React, { useCallback, useRef } from "react";
import { getRandomInt } from "../randomFunctions";
import { createExtendedPermutationFamily } from "./PermutationFamilyFunctions";

const usePermutationFamily = (permutationFamilyInput) => {
  let permutationFamily = useRef();
  if (!permutationFamily.current) {
    permutationFamily.current = permutationFamilyInput.extended
      ? permutationFamilyInput
      : createExtendedPermutationFamily(permutationFamilyInput);
  }

  const getPermutation = useCallback((index) => {
    return permutationFamily.current.permutations[index];
  }, []);

  const getRandomPermutation = useCallback(() => {
    return permutationFamily.current.permutations[
      getRandomInt(0, permutationFamily.current.permutations.length - 1)
    ];
  }, []);

  return {
    getPermutation,
    getRandomPermutation,
    permutationFamily: permutationFamily.current,
  };
};

export default usePermutationFamily;
