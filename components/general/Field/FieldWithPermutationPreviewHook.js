import React, { useState, useCallback, createContext, useEffect } from "react";
import useFieldPreview from "./FieldPreviewHook";
import usePermutationFamilyPreview from "./PermutationFamilyPreviewHook";
import {
  applyPermutationToField,
  getRangeCellsOnWhichGivenPermutationIsExecutable,
  isPermutationExecutableOnCell,
} from "./FieldWithPermutationFunctions";
import { copyField } from "./FieldFunctions";

const useFieldWithPermutationPreview = (
  initialNumberRows,
  initialNumberColumns,
  initialPermutationFamily,
  initialWrap = true
) => {
  const field = useFieldPreview(initialNumberRows, initialNumberColumns);
  const permutationFamily = usePermutationFamilyPreview(
    initialPermutationFamily
  );
  const [wrap, setWrap] = useState(initialWrap);

  const getCenter = useCallback(() => {
    let centerRow, centerColumn;
    centerRow = Math.floor((field.numberRows - 1) / 2);
    centerColumn = Math.floor((field.numberColumns - 1) / 2);
    if (!isCurrentPermutationExecutableOnCell(centerRow, centerColumn)) {
      let {
        maxRow,
        minRow,
        maxColumn,
        minColumn,
      } = getRangeCellsOnWhichGivenPermutationIsExecutable(
        permutationFamily.getCurrentPermutation(),
        field.numberRows,
        field.numberColumns,
        false
      );
      centerRow = minRow + Math.floor((maxRow - minRow) / 2);
      centerColumn = minColumn + Math.floor((maxColumn - minColumn) / 2);
    }
    return { row: centerRow, column: centerColumn };
  }, [
    field.numberColumns,
    field.numberRows,
    permutationFamily.getCurrentPermutation,
  ]);

  const applyPermutationOnCenter = () => {
    field.setField((prevField) => {
      let newField = copyField(prevField);
      applyPermutationToField(
        permutationFamily.getCurrentPermutation(),
        newField,
        getCenter().row,
        getCenter().column
      );
      return newField;
    });
  };

  const isCurrentPermutationExecutableOnCell = useCallback(
    (row, column) => {
      return isPermutationExecutableOnCell(
        row,
        column,
        permutationFamily.getCurrentPermutation(),
        field.numberRows,
        field.numberColumns,
        false
      );
    },
    [permutationFamily, field.numberRows, field.numberColumns]
  );

  useEffect(() => {
    field.reset();
  }, [field.reset, permutationFamily.permutationFamily]); //eslint-disable-line

  return {
    field,
    permutationFamily,
    wrap,
    setWrap,
    applyPermutationOnCenter,
    isCurrentPermutationExecutableOnCell,
    getCenter,
  };
};

const FieldWithPermutationPreviewContext = createContext();

export { FieldWithPermutationPreviewContext };
export default useFieldWithPermutationPreview;
