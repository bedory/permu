import { StyleSheet } from "react-native";
import useStyleVariables from "../StyleVariablesHook/StyleVariablesHook";

function useStyle(
  containerWidth,
  containerHeight,
  fieldWidth,
  fieldHeight,
  cellWidth
) {
  const { sm, contrastBackgroundColor } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    container: {
      width: containerWidth,
      height: containerHeight,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    fieldContainer: {
      width: fieldWidth + sm,
      height: fieldHeight + sm,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: contrastBackgroundColor,
      borderRadius: cellWidth ? cellWidth / 8 : 5,
    },
    field: {
      width: fieldWidth,
      height: fieldHeight,
    },
  });
}

export default useStyle;
