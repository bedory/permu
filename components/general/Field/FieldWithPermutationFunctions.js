import { getMinMaxRelativeRowColumn } from "./PermutationFunctions";
import { gcd_two_numbers, lcm_two_numbers } from "../randomFunctions";

function applyPermutationToField(permutation, field, row, column) {
  const numberRows = field.length;
  const numberColumns = field[0].length;
  const startRow =
    (row + permutation.start.relativeRow + numberRows) % numberRows;
  const startColumn =
    (column + permutation.start.relativeColumn + numberColumns) % numberColumns;
  let currentRow = startRow;
  let currentColumn = startColumn;
  let currentBuffer = field[currentRow][currentColumn];
  let nextBuffer;
  let sequenceIndex = 0;
  do {
    let nextRow =
      (currentRow +
        permutation.sequence[sequenceIndex].relativeRow +
        numberRows) %
      numberRows;
    let nextColumn =
      (currentColumn +
        permutation.sequence[sequenceIndex].relativeColumn +
        numberColumns) %
      numberColumns;
    nextBuffer = field[nextRow][nextColumn];
    field[nextRow][nextColumn] = currentBuffer;
    sequenceIndex = (sequenceIndex + 1) % permutation.sequence.length;
    currentRow = nextRow;
    currentColumn = nextColumn;
    currentBuffer = nextBuffer;
  } while (currentRow !== startRow || currentColumn !== startColumn);
}

function getRangeCellsOnWhichGivenPermutationIsExecutable(
  permutation,
  numberRows,
  numberColumns,
  wrap
) {
  if (wrap) {
    return {
      minRow: 0,
      maxRow: numberRows - 1,
      minColumn: 0,
      maxColumn: numberColumns - 1,
    };
  } else {
    let {
      minRelativeRow,
      maxRelativeRow,
      minRelativeColumn,
      maxRelativeColumn,
    } = permutation.extended
      ? permutation
      : getMinMaxRelativeRowColumn(permutation);
    return {
      minRow: -minRelativeRow,
      maxRow: numberRows - 1 - maxRelativeRow,
      minColumn: -minRelativeColumn,
      maxColumn: numberColumns - 1 - maxRelativeColumn,
    };
  }
}

function isPermutationExecutableOnCell(
  row,
  column,
  permutation,
  numberRows,
  numberColumns,
  wrap
) {
  const {
    minRow,
    maxRow,
    minColumn,
    maxColumn,
  } = getRangeCellsOnWhichGivenPermutationIsExecutable(
    permutation,
    numberRows,
    numberColumns,
    wrap
  );
  return (
    minRow <= row && maxRow >= row && minColumn <= column && maxColumn >= column
  );
}

function getNumberIterationsBeforeIdentity(
  permutation,
  numberRows,
  numberColumns
) {
  const sumRelativeRows = permutation.sequence.reduce(
    (sum, { relativeRow }) => (sum += relativeRow),
    0
  );

  const sumRelativeColumns = permutation.sequence.reduce(
    (sum, { relativeColumn }) => (sum += relativeColumn),
    0
  );

  return lcm_two_numbers(
    numberColumns / gcd_two_numbers(numberColumns, sumRelativeColumns),
    numberRows / gcd_two_numbers(numberRows, sumRelativeRows)
  );
}

function settingsToId(
  numberRows,
  numberColumns,
  permutationFamily,
  wrap,
) {
  return `numRows:${numberRows}-numColumns:${numberColumns}-perm:${permutationFamily.name}-wrap:${wrap}`;
}

function fieldToObjectOfObjects(field) {
  let newField = {};
  field.forEach((row, index) => {
    let rowObject = {};
    row.forEach((cell, index) => (rowObject[index] = cell));
    newField[index] = rowObject;
  });
  return newField;
}

export {
  applyPermutationToField,
  getRangeCellsOnWhichGivenPermutationIsExecutable,
  isPermutationExecutableOnCell,
  getNumberIterationsBeforeIdentity,
  settingsToId,
  fieldToObjectOfObjects,
};
