function isPermutationSequenceClosed(permutation) {
  const sumRelativeRowsInSequence = permutation.sequence.reduce(
    (sum, relativeLocation) => sum + relativeLocation.relativeRow,
    0,
  );
  const sumRelativeColumnsInSequence = permutation.sequence.reduce(
    (sum, relativeLocation) => sum + relativeLocation.relativeColumn,
    0,
  );
  return sumRelativeColumnsInSequence === 0 && sumRelativeRowsInSequence === 0;
}

function getMinMaxRelativeRowColumn(permutation) {
  if (!isPermutationSequenceClosed(permutation)) {
    return {
      minRelativeRow: false,
      maxRelativeRow: false,
      minRelativeColumn: false,
      maxRelativeColumn: false,
    };
  }

  let currentRow = permutation.start.relativeRow;
  let currentColumn = permutation.start.relativeColumn;

  let minRelativeRow = 0;
  let maxRelativeRow = 0;
  let minRelativeColumn = 0;
  let maxRelativeColumn = 0;

  const updateExtremeValues = () => {
    minRelativeRow = Math.min(minRelativeRow, currentRow);
    maxRelativeRow = Math.max(maxRelativeRow, currentRow);
    minRelativeColumn = Math.min(minRelativeColumn, currentColumn);
    maxRelativeColumn = Math.max(maxRelativeColumn, currentColumn);
  };

  updateExtremeValues();
  permutation.sequence.forEach(relativeLocation => {
    currentRow += relativeLocation.relativeRow;
    currentColumn += relativeLocation.relativeColumn;
    updateExtremeValues();
  });

  return {
    minRelativeRow,
    maxRelativeRow,
    minRelativeColumn,
    maxRelativeColumn,
  };
}

function getDimensionsPermutation(permutation) {
  let relativeExtremes = getMinMaxRelativeRowColumn(permutation);
  if (!relativeExtremes) {
    return {width: false, height: false};
  }

  let {
    minRelativeRow,
    maxRelativeRow,
    minRelativeColumn,
    maxRelativeColumn,
  } = relativeExtremes;

  return {
    width: Math.max(maxRelativeColumn - minRelativeColumn, 1),
    height: Math.max(maxRelativeRow - minRelativeRow, 1),
  };
}

function createExtendedPermutation(permutation) {
  return {
    ...permutation,
    extended: true,
    closed: isPermutationSequenceClosed(permutation),
    ...getMinMaxRelativeRowColumn(permutation),
    ...getDimensionsPermutation(permutation),
  };
}

export {
  isPermutationSequenceClosed,
  getDimensionsPermutation,
  getMinMaxRelativeRowColumn,
  createExtendedPermutation,
};
