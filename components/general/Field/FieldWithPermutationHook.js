import React, { useEffect, createContext, useCallback, useRef } from "react";
import { getRandomInt } from "../randomFunctions";
import useField from "./FieldHook";
import usePermutationFamily from "./PermutationFamilyHook";
import useStopwatch from "../StopwatchHook/StopwatchHook";
import {
  applyPermutationToField, fieldToObjectOfObjects,
  getNumberIterationsBeforeIdentity,
  getRangeCellsOnWhichGivenPermutationIsExecutable,
  isPermutationExecutableOnCell,
} from "./FieldWithPermutationFunctions";
import { copyField } from "./FieldFunctions";

const useFieldWithPermutation = (
  numberRowsInput,
  numberColumnsInput,
  permutationFamilyInput,
  wrapInput,
  maxNumberUndos,
  stopwatchPrecision
) => {
  const {
    numberRows,
    numberColumns,
    field,
    initialField,
    numberSteps,
    undo,
    redo,
    canUndo,
    canRedo,
    setNextField,
    finished,
    reset,
  } = useField(numberRowsInput, numberColumnsInput, maxNumberUndos);
  const {
    getPermutation,
    getRandomPermutation,
    permutationFamily,
  } = usePermutationFamily(permutationFamilyInput);
  const { time, stopTime, resetTime, startTime, restartTime } = useStopwatch(
    stopwatchPrecision
  );
  const wrap = useRef(wrapInput);
  const gameInfo = useRef({
    startField: fieldToObjectOfObjects(initialField),
    startTime: 0,
    permutationFamily: permutationFamily.name,
    steps: {},
  });

  const applyPermutation = useCallback(
    (row, column, index) => {
      gameInfo.current.steps[numberSteps] = {
        row,
        column,
        permutation: getPermutation(index).name,
        time: Date.now(),
      };
      setNextField((prevField) => {
        let nextField = copyField(prevField);
        applyPermutationToField(getPermutation(index), nextField, row, column);
        return nextField;
      });
    },
    [getPermutation, setNextField, numberSteps]
  );

  const scramble = useCallback(
    (numberIterations = 20) => {
      let newField = copyField(initialField);
      for (let i = 0; i < numberIterations; i++) {
        let randomPermutation = getRandomPermutation();
        const range = getRangeCellsOnWhichGivenPermutationIsExecutable(
          randomPermutation,
          numberRows,
          numberColumns,
          wrap.current
        );
        let numberOfTimesToApplyPermutation = getRandomInt(
          1,
          getNumberIterationsBeforeIdentity(
            randomPermutation,
            numberRows,
            numberColumns
          ) - 1
        );
        let randomRowIndex = getRandomInt(range.minRow, range.maxRow);
        let randomColumnIndex = getRandomInt(range.minColumn, range.maxColumn);
        for (let j = 0; j < numberOfTimesToApplyPermutation; j++) {
          applyPermutationToField(
            randomPermutation,
            newField,
            randomRowIndex,
            randomColumnIndex
          );
        }
      }
      reset(newField);
      gameInfo.current.startField = fieldToObjectOfObjects(newField);
      gameInfo.current.startTime = Date.now();
      gameInfo.current.steps = {};
      restartTime();
    },
    [
      getRandomPermutation,
      initialField,
      numberColumns,
      numberRows,
      reset,
      restartTime,
    ]
  );

  useEffect(() => {
    if (finished) {
      stopTime();
    }
  }, [finished]); // eslint-disable-line

  const isPermutationWithIndexExecutableOnCell = useCallback(
    (row, column, permutationIndex) => {
      return isPermutationExecutableOnCell(
        row,
        column,
        getPermutation(permutationIndex),
        numberRows,
        numberColumns,
        wrap.current
      );
    },
    [getPermutation, numberColumns, numberRows]
  );

  return {
    field: {
      numberRows,
      numberColumns,
      field,
      initialField,
      numberSteps,
      undo,
      redo,
      canUndo,
      canRedo,
      setNextField,
      finished,
      reset,
    },
    permutationFamily: {
      getPermutation,
      getRandomPermutation,
      permutationFamily,
    },
    fieldWithPermutation: {
      wrap: wrap.current,
      applyPermutation,
      scramble,
      isPermutationWithIndexExecutableOnCell,
      gameInfo: gameInfo.current,
    },
    time,
  };
};

const FieldWithPermutationContext = createContext();
const TimeContext = createContext();
const FieldContext = createContext();
const PermutationFamilyContext = createContext();

export {
  FieldWithPermutationContext,
  TimeContext,
  FieldContext,
  PermutationFamilyContext,
};
export default useFieldWithPermutation;
