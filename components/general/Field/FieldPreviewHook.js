import React, { useState, useCallback, useEffect } from "react";
import { generateField } from "./FieldFunctions";

const useFieldPreview = (initialNumberRows, initialNumberColumns) => {
  const [numberRows, setNumberRows] = useState(initialNumberRows);
  const [numberColumns, setNumberColumns] = useState(initialNumberColumns);
  const [field, setField] = useState(() =>
    generateField(numberRows, numberColumns)
  );

  const reset = useCallback(() => {
    setField(generateField(numberRows, numberColumns));
  }, [numberColumns, numberRows]);

  const setDimensions = (dim1, dim2) => {
    let rows = Math.max(dim1, dim2);
    let columns = Math.min(dim1, dim2);
    setNumberRows(rows);
    setNumberColumns(columns);
  };

  useEffect(() => {
    reset();
  }, [numberColumns, numberRows]); // eslint-disable-line

  return {
    numberRows,
    numberColumns,
    setDimensions,
    field,
    setField,
    reset,
  };
};

export default useFieldPreview;
