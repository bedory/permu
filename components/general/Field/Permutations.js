import { createRelativeLocation } from "./Position";

const swapRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(0, 1), createRelativeLocation(0, -1)],
};

const swapLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(0, -1), createRelativeLocation(0, 1)],
};

const swapDown = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, 0), createRelativeLocation(-1, 0)],
};

const swapUp = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, 0), createRelativeLocation(1, 0)],
};

const diagonalSwapUpRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, 1), createRelativeLocation(1, -1)],
};

const diagonalSwapUpLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, -1), createRelativeLocation(1, 1)],
};

const diagonalSwapDownLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, -1), createRelativeLocation(-1, 1)],
};

const diagonalSwapDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, 1), createRelativeLocation(-1, -1)],
};

const bigSwapRight = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(0, 1),
    createRelativeLocation(0, 1),
    createRelativeLocation(0, -2),
  ],
};

const bigSwapLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(0, -1),
    createRelativeLocation(0, -1),
    createRelativeLocation(0, 2),
  ],
};

const bigSwapUp = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(-1, 0),
    createRelativeLocation(-1, 0),
    createRelativeLocation(2, 0),
  ],
};

const bigSwapDown = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(1, 0),
    createRelativeLocation(1, 0),
    createRelativeLocation(-2, 0),
  ],
};

const shiftRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(0, 1)],
};

const shiftLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(0, -1)],
};

const shiftUp = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, 0)],
};

const shiftDown = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, 0)],
};

const smallRotateRightSequence = [
  createRelativeLocation(0, 1),
  createRelativeLocation(1, 0),
  createRelativeLocation(0, -1),
  createRelativeLocation(-1, 0),
];

const smallRotateLeftSequence = [
  createRelativeLocation(1, 0),
  createRelativeLocation(0, 1),
  createRelativeLocation(-1, 0),
  createRelativeLocation(0, -1),
];

const smallRotateUpRight = {
  start: createRelativeLocation(-1, 0),
  sequence: smallRotateRightSequence,
};

const smallRotateUpLeft = {
  start: createRelativeLocation(-1, -1),
  sequence: smallRotateLeftSequence,
};

const smallRotateLeftUp = {
  start: createRelativeLocation(-1, -1),
  sequence: smallRotateRightSequence,
};

const smallRotateLeftDown = {
  start: createRelativeLocation(0, -1),
  sequence: smallRotateLeftSequence,
};

const smallRotateDownLeft = {
  start: createRelativeLocation(0, -1),
  sequence: smallRotateRightSequence,
};

const smallRotateDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: smallRotateLeftSequence,
};

const smallRotateRightDown = {
  start: createRelativeLocation(0, 0),
  sequence: smallRotateRightSequence,
};

const smallRotateRightUp = {
  start: createRelativeLocation(-1, 0),
  sequence: smallRotateLeftSequence,
};

const bigRotateRightSequence = [
  createRelativeLocation(0, 1),
  createRelativeLocation(0, 1),
  createRelativeLocation(1, 0),
  createRelativeLocation(1, 0),
  createRelativeLocation(0, -1),
  createRelativeLocation(0, -1),
  createRelativeLocation(-1, 0),
  createRelativeLocation(-1, 0),
];

const bigRotateLeftSequence = [
  createRelativeLocation(1, 0),
  createRelativeLocation(1, 0),
  createRelativeLocation(0, 1),
  createRelativeLocation(0, 1),
  createRelativeLocation(-1, 0),
  createRelativeLocation(-1, 0),
  createRelativeLocation(0, -1),
  createRelativeLocation(0, -1),
];

const bigRotateUpRight = {
  start: createRelativeLocation(-2, 0),
  sequence: bigRotateRightSequence,
};

const bigRotateUpLeft = {
  start: createRelativeLocation(-2, -2),
  sequence: bigRotateLeftSequence,
};

const bigRotateLeftUp = {
  start: createRelativeLocation(-2, -2),
  sequence: bigRotateRightSequence,
};

const bigRotateLeftDown = {
  start: createRelativeLocation(0, -2),
  sequence: bigRotateLeftSequence,
};

const bigRotateDownLeft = {
  start: createRelativeLocation(0, -2),
  sequence: bigRotateRightSequence,
};

const bigRotateDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: bigRotateLeftSequence,
};

const bigRotateRightDown = {
  start: createRelativeLocation(0, 0),
  sequence: bigRotateRightSequence,
};

const bigRotateRightUp = {
  start: createRelativeLocation(-2, 0),
  sequence: bigRotateLeftSequence,
};

const triangleUpRight = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(-2, 1),
    createRelativeLocation(0, -2),
    createRelativeLocation(2, 1),
  ],
};

const triangleUpLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(-2, -1),
    createRelativeLocation(0, 2),
    createRelativeLocation(2, -1),
  ],
};

const triangleLeftUp = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(-1, -2),
    createRelativeLocation(2, 0),
    createRelativeLocation(-1, 2),
  ],
};

const triangleLeftDown = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(1, -2),
    createRelativeLocation(-2, 0),
    createRelativeLocation(1, 2),
  ],
};

const triangleDownLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(2, -1),
    createRelativeLocation(0, 2),
    createRelativeLocation(-2, -1),
  ],
};

const triangleDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(2, 1),
    createRelativeLocation(0, -2),
    createRelativeLocation(-2, 1),
  ],
};

const triangleRightDown = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(1, 2),
    createRelativeLocation(-2, 0),
    createRelativeLocation(1, -2),
  ],
};

const triangleRightUp = {
  start: createRelativeLocation(0, 0),
  sequence: [
    createRelativeLocation(-1, 2),
    createRelativeLocation(2, 0),
    createRelativeLocation(-1, -2),
  ],
};

const diagonalUpRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, 1)],
};

const diagonalUpLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, -1)],
};

const diagonalDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, 1)],
};

const diagonalDownLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, -1)],
};

const chessHorseUpRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-2, 1), createRelativeLocation(2, -1)],
};

const chessHorseUpLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-2, -1), createRelativeLocation(2, 1)],
};

const chessHorseLeftUp = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, -2), createRelativeLocation(1, 2)],
};

const chessHorseLeftDown = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, -2), createRelativeLocation(-1, 2)],
};

const chessHorseDownLeft = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(2, -1), createRelativeLocation(-2, 1)],
};

const chessHorseDownRight = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(2, 1), createRelativeLocation(-2, -1)],
};

const chessHorseRightDown = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(1, 2), createRelativeLocation(-1, -2)],
};

const chessHorseRightUp = {
  start: createRelativeLocation(0, 0),
  sequence: [createRelativeLocation(-1, 2), createRelativeLocation(1, -2)],
};

export {
  swapDown,
  swapLeft,
  swapRight,
  swapUp,
  diagonalSwapDownLeft,
  diagonalSwapDownRight,
  diagonalSwapUpLeft,
  diagonalSwapUpRight,
  bigSwapDown,
  bigSwapLeft,
  bigSwapRight,
  bigSwapUp,
  shiftRight,
  shiftLeft,
  shiftUp,
  shiftDown,
  smallRotateRightUp,
  smallRotateDownLeft,
  smallRotateDownRight,
  smallRotateLeftDown,
  smallRotateLeftUp,
  smallRotateRightDown,
  smallRotateUpLeft,
  smallRotateUpRight,
  bigRotateRightUp,
  bigRotateDownLeft,
  bigRotateDownRight,
  bigRotateLeftDown,
  bigRotateLeftUp,
  bigRotateRightDown,
  bigRotateUpLeft,
  bigRotateUpRight,
  triangleRightUp,
  triangleDownLeft,
  triangleDownRight,
  triangleLeftDown,
  triangleLeftUp,
  triangleRightDown,
  triangleUpLeft,
  triangleUpRight,
  diagonalDownLeft,
  diagonalDownRight,
  diagonalUpLeft,
  diagonalUpRight,
  chessHorseDownLeft,
  chessHorseDownRight,
  chessHorseLeftDown,
  chessHorseLeftUp,
  chessHorseRightDown,
  chessHorseRightUp,
  chessHorseUpLeft,
  chessHorseUpRight,
};
