import React, { useState, useContext } from "react";
import { ScrollView, Alert } from "react-native";
import useSignInModalStyle from "./AccountStyle";
import { UserContext } from "../general/UserHook/UserHook";
import { MessageContext, MessageTypes } from "../general/Message/MessageHook";
import auth from "@react-native-firebase/auth";
import {
  checkValidEmailFormat,
  removeWhiteSpaceLeftAndRight,
} from "../general/randomFunctions";
import Screen from "../general/Screen/Screen";
import Separator from "../general/Separator/Separator";
import InputSection from "../general/InputSection/InputSection";
import functions from "@react-native-firebase/functions";
import CheckSignedInContainer from "../general/CheckSignedInContainer/CheckSignedInContainer";

const Account = ({ navigation }) => {
  const style = useSignInModalStyle();
  const { user, userJson, reload } = useContext(UserContext);
  const [signingOut, setSigningOut] = useState(false);
  const [username, setUsername] = useState(userJson && userJson.displayName);
  const [usernameError, setUsernameError] = useState();
  const [updatingUsername, setUpdatingUsername] = useState(false);
  const [email, setEmail] = useState(user && user.email);
  const [emailError, setEmailError] = useState();
  const [emailPassword, setEmailPassword] = useState();
  const [emailPasswordError, setEmailPasswordError] = useState();
  const [updatingEmail, setUpdatingEmail] = useState(false);
  const [oldPassword, setOldPassword] = useState();
  const [oldPasswordError, setOldPasswordError] = useState();
  const [newPassword, setNewPassword] = useState();
  const [newPasswordError, setNewPasswordError] = useState();
  const [updatingPassword, setUpdatingPassword] = useState(false);
  const [deletePassword, setDeletePassword] = useState();
  const [deletePasswordError, setDeletePasswordError] = useState();
  const [deletingAccount, setDeletingAccount] = useState(false);
  const { setMessageAndShow } = useContext(MessageContext);

  if (!user) {
    return (
      <Screen>
        <CheckSignedInContainer signInToText="to access account settings." />
      </Screen>
    );
  }

  const signOut = async () => {
    setSigningOut(true);
    try {
      await auth().signOut();
      await reload();
      navigation.navigate("Home");
      setMessageAndShow("Successfully signed out.", MessageTypes.SUCCESS);
    } catch (e) {
      console.log("from signOut", e);
    }
    setSigningOut(false);
  };

  const checkUsername = (username) => {
    let inputValid = true;
    if (!username || !username.length) {
      inputValid = false;
      setUsernameError("username can't be empty");
    } else if (username.length < 5) {
      inputValid = false;
      setUsernameError("Username must be at least 5 characters");
    } else if (username.length > 25) {
      inputValid = false;
      setUsernameError("Username cannot be more than 25 characters");
    } else if (username === user.displayName) {
      inputValid = false;
      setUsernameError("cannot be the same as the old username");
    } else {
      setUsernameError(null);
    }
    return inputValid;
  };

  const changeUsername = async () => {
    const usernameInput = removeWhiteSpaceLeftAndRight(username);
    if (checkUsername(usernameInput)) {
      setUpdatingUsername(true);
      try {
        const { data } = await functions().httpsCallable("changeUsername")({
          username: usernameInput,
        });
        if (data.success) {
          await reload();
          setMessageAndShow(
            "Successfully changed username.",
            MessageTypes.SUCCESS
          );
        } else {
          setMessageAndShow(data.errorMessage, MessageTypes.ERROR);
        }
      } catch (e) {
        setMessageAndShow(
          "Error while changing username. Try again later.",
          MessageTypes.ERROR
        );
        console.log(e);
      }
      setUpdatingUsername(false);
    }
  };

  const checkEmail = (email) => {
    let inputValid = true;
    if (!email || !email.length) {
      inputValid = false;
      setEmailError("Email can't be empty");
    } else if (!checkValidEmailFormat(email)) {
      inputValid = false;
      setEmailError(
        "Email must have correct format, e.g. john.doe@example.com"
      );
    } else if (userJson.email === email) {
      inputValid = false;
      setEmailError(
        "email address cannot be the same as the old email address"
      );
    } else {
      setEmailError(null);
    }

    if (!emailPassword || !emailPassword.length) {
      inputValid = false;
      setEmailPasswordError("password can't be empty");
    } else {
      setEmailPasswordError(null);
    }
    return inputValid;
  };

  const changeEmail = async () => {
    const emailInput = removeWhiteSpaceLeftAndRight(email);
    if (checkEmail(emailInput)) {
      setUpdatingEmail(true);
      try {
        const provider = auth.EmailAuthProvider;
        const authCredential = provider.credential(
          userJson.email,
          emailPassword
        );
        await user.reauthenticateWithCredential(authCredential);
        await user.updateEmail(emailInput);
        await user.sendEmailVerification();
        await auth().signOut();
        await reload();
        navigation.reset({
          index: 1,
          routes: [{ name: "Home" }, { name: "SignIn" }],
        });
        setMessageAndShow(
          "Successfully changed email. Please check your emails to verify the new email address.",
          MessageTypes.SUCCESS
        );
      } catch (e) {
        if (e.code === "auth/wrong-password") {
          setEmailPasswordError("password is incorrect");
        }
        if (e.code === "auth/email-already-in-use") {
          setEmailError("This email is already in use");
        } else {
          setMessageAndShow(
            "Error while changing email. Try again later.",
            MessageTypes.ERROR
          );
        }
      }
      setUpdatingEmail(false);
    }
  };

  const checkPassword = () => {
    let inputValid = true;

    if (!newPassword || !newPassword.length) {
      inputValid = false;
      setNewPasswordError("can't be empty");
    } else if (newPassword === oldPassword) {
      inputValid = false;
      setNewPasswordError("new password cannot be the same as old password");
    } else if (newPassword.length < 6) {
      inputValid = false;
      setNewPasswordError("must be at least 6 characters");
    } else if (newPassword.length > 25) {
      inputValid = false;
      setNewPasswordError("cannot be more than 25 characters");
    } else {
      setNewPasswordError(null);
    }
    if (!oldPassword || !oldPassword.length) {
      inputValid = false;
      setOldPasswordError("can't be empty");
    } else {
      setOldPasswordError(null);
    }

    return inputValid;
  };

  const changePassword = async () => {
    if (checkPassword()) {
      setUpdatingPassword(true);
      try {
        const provider = auth.EmailAuthProvider;
        const oldAuthCredential = provider.credential(
          userJson.email,
          oldPassword
        );
        await user.reauthenticateWithCredential(oldAuthCredential);
        await user.updatePassword(newPassword);
        // auth().signInWithEmailAndPassword(user.email, newPassword);
        await reload();
        setOldPassword(null);
        setNewPassword(null);
        setMessageAndShow(
          "Password successfully changed.",
          MessageTypes.SUCCESS
        );
      } catch (e) {
        if (e.code === "auth/wrong-password") {
          setOldPasswordError("old password is incorrect");
        } else {
          setMessageAndShow(
            "Error while changing password. Try again later.",
            MessageTypes.ERROR
          );
        }
        console.log(e);
      }
      setUpdatingPassword(false);
    }
  };

  const checkDelete = () => {
    let inputValid = true;
    if (!deletePassword || !deletePassword.length) {
      inputValid = false;
      setDeletePasswordError("password can't be empty");
    } else {
      setDeletePasswordError(null);
    }
    return inputValid;
  };

  const deleteAccount = async () => {
    setDeletingAccount(true);
    try {
      const provider = auth.EmailAuthProvider;
      const authCredential = provider.credential(
        userJson.email,
        deletePassword
      );
      await user.reauthenticateWithCredential(authCredential);
      await user.delete();
      await reload();
      navigation.navigate("Home");
      setMessageAndShow("Successfully deleted account.", MessageTypes.SUCCESS);
    } catch (e) {
      if (e.code === "auth/wrong-password") {
        setDeletePasswordError("password is incorrect");
      } else {
        setMessageAndShow(
          "Error while deleting account. Try again later.",
          MessageTypes.ERROR
        );
      }
      console.log(e);
    }
    setDeletingAccount(false);
  };

  const deleteAccountAlert = () => {
    if (checkDelete()) {
      Alert.alert(
        "delete account",
        "Are you sure you want to delete your account? This action cannot be undone.",
        [{ text: "cancel" }, { text: "confirm", onPress: deleteAccount }],
        { cancelable: true }
      );
    }
  };

  return (
    <Screen style={style.root}>
      <ScrollView contentContainerStyle={style.scroll}>
        <InputSection
          title={"Signed in as " + userJson.displayName}
          titleStyle={style.message}
          loadingIndicatorContainerProps={{ loading: signingOut }}
          buttonProps={{ text: "sign out", onPress: signOut }}
        />

        <Separator />

        <InputSection
          title="change username"
          arrayTextInputProps={[
            {
              label: "new username",
              placeholder: "PuzzlingJohn",
              value: username,
              onChangeText: setUsername,
              error: usernameError,
            },
          ]}
          loadingIndicatorContainerProps={{ loading: updatingUsername }}
          buttonProps={{ text: "submit", onPress: changeUsername }}
        />

        <Separator />

        <InputSection
          title="change email"
          arrayTextInputProps={[
            {
              label: "new email address",
              placeholder: "john.doe@email.com",
              value: email,
              onChangeText: setEmail,
              error: emailError,
            },
            {
              label: "password",
              placeholder: "secret123",
              value: emailPassword,
              onChangeText: setEmailPassword,
              error: emailPasswordError,
              secureTextEntry: true,
            },
          ]}
          loadingIndicatorContainerProps={{ loading: updatingEmail }}
          buttonProps={{ text: "submit", onPress: changeEmail }}
        />

        <Separator />

        <InputSection
          title="change password"
          arrayTextInputProps={[
            {
              label: "old password",
              placeholder: "secret123",
              value: oldPassword,
              onChangeText: setOldPassword,
              error: oldPasswordError,
              secureTextEntry: true,
            },
            {
              label: "new password",
              placeholder: "new_secret456",
              value: newPassword,
              onChangeText: setNewPassword,
              error: newPasswordError,
              secureTextEntry: true,
            },
          ]}
          loadingIndicatorContainerProps={{ loading: updatingPassword }}
          buttonProps={{ text: "submit", onPress: changePassword }}
        />

        <Separator />

        <InputSection
          title="delete account"
          arrayTextInputProps={[
            {
              label: "password",
              placeholder: "secret123",
              value: deletePassword,
              onChangeText: setDeletePassword,
              error: deletePasswordError,
              secureTextEntry: true,
            },
          ]}
          loadingIndicatorContainerProps={{ loading: deletingAccount }}
          buttonProps={{
            text: "delete",
            onPress: deleteAccountAlert,
            style: style.deleteButtonText,
          }}
        />
      </ScrollView>
    </Screen>
  );
};

export default Account;
