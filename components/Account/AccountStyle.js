import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    smFont,
    md,
    errorColor,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      padding: 0,
      alignItems: "stretch",
    },
    scroll: {
      alignItems: "center",
    },
    message: {
      fontSize: smFont,
      margin: md,
      textAlign: "center",
    },
    deleteButtonText: {
      color: errorColor,
    },
  });
}

export default useStyle;
