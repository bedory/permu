import React, { useContext, useCallback } from "react";
import { View } from "react-native";
import useOptionStyle from "./OptionStyle";
import { SettingsContext } from "../../general/SettingsHook/SettingsHook";
import StyledText from "../../general/StyledText/StyledText";
import Label from "./Label/Label";
import ContValueSetter from "./ContValueSetter/ContValueSetter";
import DiscrValueSetter from "./DiscrValueSetter/DiscrValueSetter";
import BoolValueSetter from "./BoolValueSetter/BoolValueSetter";

const Option = ({ option }) => {
  const style = useOptionStyle();
  const { settings, setSingleSetting } = useContext(SettingsContext);
  const value = settings[option.name];
  const setNewValue = useCallback(
    (newValue) => setSingleSetting(option.name, newValue),
    []
  );

  let valueSetter;
  switch (option.type) {
    case "bool":
      valueSetter = <BoolValueSetter value={value} setValue={setNewValue} />;
      break;
    case "contValue":
      valueSetter = (
        <ContValueSetter
          value={value}
          setValue={setNewValue}
          lowerBound={option.lowerBound}
          upperBound={option.upperBound}
          stepSize={option.stepSize}
          unit={option.unit}
        />
      );
      break;
    case "discrValue":
      valueSetter = (
        <DiscrValueSetter
          value={value}
          setValue={setNewValue}
          possibleValues={option.possibleValues}
        />
      );
      break;
    default:
      valueSetter = <StyledText>not yet implemented</StyledText>;
      break;
  }

  return (
    <View style={style.root}>
      <Label label={option.label} description={option.description} />
      <View style={style.valueSetterContainer}>{valueSetter}</View>
    </View>
  );
};

export default React.memo(Option);
