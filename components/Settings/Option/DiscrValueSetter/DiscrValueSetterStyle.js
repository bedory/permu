import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont, sm, contrastBackgroundColor } = useStyleVariables();
  return StyleSheet.create({
    pickerItem: {
      fontSize: smFont,
    },
    pickerButton: {
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: contrastBackgroundColor,
      borderRadius: 10,
    },
    pickerButtonText: {
      fontSize: smFont,
      lineHeight: smFont + 4,
      textAlign: "center",
      margin: sm,
    },
  });
}

export default useStyle;
