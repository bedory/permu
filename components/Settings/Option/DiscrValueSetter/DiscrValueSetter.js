import React from "react";
import useDiscrValueSetterStyle from "./DiscrValueSetterStyle";
import StyledPicker from "../../../general/StyledPicker/StyledPicker";
import PickerItem from "../../../general/StyledPicker/PickerItem/PickerItem";

const DiscrValueSetter = ({ value, setValue, possibleValues }) => {
  const style = useDiscrValueSetterStyle();
  return (
    <StyledPicker
      buttonContainerStyle={style.pickerButton}
      buttonTextStyle={style.pickerButtonText}
      itemStyle={style.pickerItem}
      selectedValue={value}
      onValueChange={setValue}
    >
      {possibleValues.map((possibleValue) => (
        <PickerItem
          key={possibleValue.value}
          label={possibleValue.label}
          value={possibleValue.value}
        />
      ))}
    </StyledPicker>
  );
};

export default React.memo(DiscrValueSetter);
