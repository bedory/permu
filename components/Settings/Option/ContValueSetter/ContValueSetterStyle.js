import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { fontFamily, textColor, smFont } = useStyleVariables();
  return StyleSheet.create({
    sliderContainer: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    slider: {
      flex: 1,
    },
    sliderTextInput: {
      flex: 0,
      width: 45,
      fontFamily,
      color: textColor,
      fontSize: smFont,
      textAlign: "right",
    },
    unit: {
      fontSize: smFont,
    },
  });
}

export default useStyle;
