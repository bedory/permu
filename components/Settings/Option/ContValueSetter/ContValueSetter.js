import React, { useState } from "react";
import { View, TextInput } from "react-native";
import useContValueSetterStyle from "./ContValueSetterStyle";
import Slider from "@react-native-community/slider";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";
import StyledText from "../../../general/StyledText/StyledText";

const ContValueSetter = ({
  value,
  setValue,
  lowerBound,
  upperBound,
  stepSize,
  unit,
}) => {
  const style = useContValueSetterStyle();
  const [tempValue, setTempValue] = useState(value && value.toString());
  const { accentColor, contrastBackgroundColor } = useStyleVariables();
  return (
    <View style={style.sliderContainer}>
      <Slider
        value={value}
        style={style.slider}
        minimumValue={lowerBound}
        maximumValue={upperBound}
        minimumTrackTintColor={accentColor}
        maximumTrackTintColor={contrastBackgroundColor}
        thumbTintColor={accentColor}
        onValueChange={(newValue) => {
          setValue(newValue);
          setTempValue(newValue.toString());
        }}
        step={stepSize}
      />
      <TextInput
        style={style.sliderTextInput}
        value={tempValue}
        keyboardType="numeric"
        onChangeText={setTempValue}
        onEndEditing={() => {
          let number = Number(tempValue);
          if (!isNaN(number)) {
            number -= number % stepSize;
            number = Math.max(lowerBound, number);
            number = Math.min(upperBound, number);
            setValue(number);
            setTempValue(number.toString());
          }
        }}
      />
      <StyledText style={style.unit}>{unit}</StyledText>
    </View>
  );
};

export default React.memo(ContValueSetter);
