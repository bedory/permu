import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { windowWidth } = useStyleVariables();
  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 10,
      marginBottom: 10,
    },
    valueSetterContainer: {
      width: 0.45 * windowWidth,
    },
  });
}

export default useStyle;
