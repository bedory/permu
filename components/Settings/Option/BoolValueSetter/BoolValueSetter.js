import React from "react";
import { Switch } from "react-native";
import useBoolValueSetterStyle from "./BoolValueSetterStyle";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

const BoolValueSetter = ({ value, setValue }) => {
  const style = useBoolValueSetterStyle();
  const {
    accentColor,
    disabledTextColor,
    darkAccentColor,
    contrastBackgroundColor,
  } = useStyleVariables();
  return (
    <Switch
      value={value}
      onValueChange={setValue}
      thumbColor={value ? accentColor : disabledTextColor}
      trackColor={{ true: darkAccentColor, false: contrastBackgroundColor }}
    />
  );
};

export default React.memo(BoolValueSetter);
