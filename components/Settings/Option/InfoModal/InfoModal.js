import React from "react";
import { View, Modal, TouchableWithoutFeedback } from "react-native";
import useInfoModalStyle from "./InfoModalStyle";
import StyledText from "../../../general/StyledText/StyledText";

const InfoModal = ({ onHideHelp, showHelp, description }) => {
  const style = useInfoModalStyle();
  return (
    <Modal
      onRequestClose={onHideHelp}
      visible={!!description && showHelp}
      transparent={true}
      animationType="fade"
    >
      <TouchableWithoutFeedback onPress={onHideHelp}>
        <View style={style.helpModalContainer}>
          <View style={style.helpModal}>
            <StyledText style={style.description}>{description}</StyledText>
          </View>
          <View style={style.backgroundBlur} />
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default React.memo(InfoModal);
