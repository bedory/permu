import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    windowWidth,
    contrastBackgroundColor,
    xl,
    smFont,
    backgroundColor,
  } = useStyleVariables();
  return StyleSheet.create({
    helpModalContainer: {
      justifyContent: "center",
      alignItems: "center",
      flex: 1,
    },
    helpModal: {
      width: 0.8 * windowWidth,
      backgroundColor: contrastBackgroundColor,
      padding: 25,
      borderRadius: xl,
      zIndex: 5,
    },
    backgroundBlur: {
      ...StyleSheet.absoluteFill,
      backgroundColor,
      opacity: 0.5,
    },
    description: {
      fontSize: smFont,
      lineHeight: smFont + 6,
      textAlign: "center",
    },
  });
}

export default useStyle;
