import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont, windowWidth, sm } = useStyleVariables();
  return StyleSheet.create({
    labelContainer: {
      flexDirection: "row",
      alignItems: "center",
      width: 0.45 * windowWidth,
    },
    label: {
      fontSize: smFont,
    },
    helpButtonContainer: {
      marginRight: sm,
    },
  });
}

export default useStyle;
