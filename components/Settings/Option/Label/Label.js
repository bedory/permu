import React, { useState, useCallback } from "react";
import { View } from "react-native";
import useLabelStyle from "./LabelStyle";
import StyledButton from "../../../general/StyledButton/StyledButton";
import HelpIcon from "../../../../assets/icons/help.svg";
import InfoModal from "../InfoModal/InfoModal";
import StyledText from "../../../general/StyledText/StyledText";

const Label = ({ description, label }) => {
  const [showHelp, setShowHelp] = useState(false);
  const onShowHelp = useCallback(() => setShowHelp(true), []);
  const onHideHelp = useCallback(() => setShowHelp(false), []);
  const style = useLabelStyle();
  return (
    <View style={style.labelContainer}>
      <InfoModal
        onHideHelp={onHideHelp}
        showHelp={showHelp}
        description={description}
      />
      {description && (
        <StyledButton
          Icon={HelpIcon}
          width={20}
          height={20}
          containerStyle={style.helpButtonContainer}
          onPress={onShowHelp}
        />
      )}
      <StyledText style={style.label}>{label}</StyledText>
    </View>
  );
};

export default React.memo(Label);
