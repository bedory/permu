import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    xlFont,
    md,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      justifyContent: "flex-start",
      alignItems: "stretch",
      padding: 0,
    },
    scrollView: {
      paddingHorizontal: md,
    },
    defaultContainer: {
      marginVertical: md,
    },
    defaultHeader: {
      fontSize: xlFont,
    },
    defaultButtonsContainer: {
      flexDirection: "row",
      justifyContent: "space-around",
      alignItems: "center",
    },
    defaultButton: {
      marginVertical: md,
    },
  });
}

export default useStyle;
