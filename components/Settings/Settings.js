import React, { useContext, useCallback } from "react";
import { View, Alert, SectionList } from "react-native";
import useSettingsStyle from "./SettingsStyle";
import SettingsStructure, {
  defaultCasual,
  defaultCompetitive,
} from "../general/SettingsHook/settingsConstants";
import StyledButton from "../general/StyledButton/StyledButton";
import { SettingsContext } from "../general/SettingsHook/SettingsHook";
import Screen from "../general/Screen/Screen";
import CategoryHeader from "./CategoryHeader/CategoryHeader";
import Option from "./Option/Option";
import StyledText from "../general/StyledText/StyledText";

const Settings = () => {
  const style = useSettingsStyle();
  const { setSettings } = useContext(SettingsContext);

  const resetToDefaultCasual = useCallback(() => {
    Alert.alert(
      "reset to default casual",
      "Are you sure you want to reset the settings to the default casual settings? This will remove any previously set settings.",
      [
        { text: "cancel" },
        { text: "confirm", onPress: () => setSettings(defaultCasual) },
      ],
      { cancelable: true }
    );
  }, []);

  const resetToDefaultCompetitive = useCallback(() => {
    Alert.alert(
      "reset to default competitive",
      "Are you sure you want to reset the settings to the default competitive settings? This will remove any previously set settings.",
      [
        { text: "cancel" },
        { text: "confirm", onPress: () => setSettings(defaultCompetitive) },
      ],
      { cancelable: true }
    );
  }, []);

  return (
    <Screen style={style.root}>
      <SectionList
        sections={SettingsStructure.categories}
        contentContainerStyle={style.scrollView}
        keyExtractor={(item) => item.name}
        renderSectionHeader={({ section: { name } }) => (
          <CategoryHeader title={name} />
        )}
        renderItem={({ item }) => <Option option={item} />}
        ListFooterComponent={
          <View style={style.defaultContainer}>
            <StyledText style={style.defaultHeader}>
              reset to default
            </StyledText>
            <View style={style.defaultButtonsContainer}>
              <StyledButton
                text={"casual"}
                containerStyle={style.defaultButton}
                onPress={resetToDefaultCasual}
              />
              <StyledButton
                text={"competitive"}
                containerStyle={style.defaultButton}
                onPress={resetToDefaultCompetitive}
              />
            </View>
          </View>
        }
      />
    </Screen>
  );
};

export default Settings;
