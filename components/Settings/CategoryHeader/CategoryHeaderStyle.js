import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { xlFont } = useStyleVariables();
  return StyleSheet.create({
    root: {},
    text: {
      fontSize: xlFont,
    },
  });
}

export default useStyle;
