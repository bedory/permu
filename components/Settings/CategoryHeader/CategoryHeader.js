import React from "react";
import { View } from "react-native";
import useCategoryHeaderStyle from "./CategoryHeaderStyle";
import StyledText from "../../general/StyledText/StyledText";

const CategoryHeader = ({ title }) => {
  const style = useCategoryHeaderStyle();
  return (
    <View style={style.root}>
      <StyledText style={style.text}>{title}</StyledText>
    </View>
  );
};

export default React.memo(CategoryHeader);
