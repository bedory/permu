import { StyleSheet } from "react-native";
import useStyleVariables from "../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {
    windowWidth,
    smFont,
  } = useStyleVariables();
  return StyleSheet.create({
    root: {
      padding: 0,
    },
    header: {
      alignSelf: "stretch",
    },
    imageList: {
      display: "flex",
      justifyContent: "flex-start",
      alignItems: "center",
      width: windowWidth,
    },
    separator: {
      height: 20,
    },
    noResultMessage: {
      fontSize: smFont,
    },
  });
}

export default useStyle;
