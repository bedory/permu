import Unsplash, { toJson } from "unsplash-js/native";
import firestore from "@react-native-firebase/firestore";
import storage from "@react-native-firebase/storage";

const unsplash = new Unsplash({
  // TODO: these have to be made secret through own proxy
  accessKey: "UpIFtAsupQJZv9kqkvV7h-JfROk9kN2LNKSCkMmuHxY",
  secret: "KEKBur29hgPsBegbo5HkLUM2Nufs4l8dRzcgTeJu1Q4",
});

/*
  image object: {
    width: original image width - number,
    height: original image height - number,
    url: link to image - string,
    artist: name of artist - string,
    artistUrl: link to artist page - string,
    platform: name of platform of on which artist is featured - string,
    platformUrl: link to platform - string,
    color: predominant color of image - string
  }
 */

function transformUnsplashImageObject(imageObject) {
  return {
    width: imageObject.width,
    height: imageObject.height,
    url: imageObject.urls.regular,
    artist: imageObject.user.name,
    artistUrl:
      imageObject.user.links.html + "?utm_source=permu&utm_medium=referral",
    platform: "unsplash",
    platformUrl: "https://unsplash.com?utm_source=permu&utm_medium=referral",
    color: "#fff",
  };
}

function transformUnsplashImageObjectsArray(imageObjectsArray) {
  return imageObjectsArray.map((imageObject) =>
    transformUnsplashImageObject(imageObject)
  );
}

function getRandomImages(numberResults) {
  return unsplash.photos
    .getRandomPhoto({ count: numberResults, featured: true })
    .then(toJson)
    .then(transformUnsplashImageObjectsArray);
}

function getSearchedImages(query, pageNumber, resultsPerPage) {
  return unsplash.search
    .photos(query, pageNumber, resultsPerPage)
    .then(toJson)
    .then((data) => data.results)
    .then(transformUnsplashImageObjectsArray);
}

function getNewestImages(pageNumber, resultsPerPage) {
  return unsplash.photos
    .listPhotos(pageNumber, resultsPerPage, "latest")
    .then(toJson)
    .then(transformUnsplashImageObjectsArray);
}

function getMostPopularImages(pageNumber, resultsPerPage) {
  return unsplash.photos
    .listPhotos(pageNumber, resultsPerPage, "popular")
    .then(toJson)
    .then(transformUnsplashImageObjectsArray);
}

function getArtworks(pageNumber, resultsPerPage, lastArtworkSnapshot) {
  const baseQuery = firestore()
    .collection("artworks")
    .orderBy("timestamp", "desc");
  let queryFromLastArtwork;
  if (lastArtworkSnapshot) {
    queryFromLastArtwork = baseQuery.startAfter(lastArtworkSnapshot);
  } else {
    queryFromLastArtwork = baseQuery;
  }
  return queryFromLastArtwork
    .limit(resultsPerPage)
    .get()
    .then(async (querySnapshot) => {
      const artworkSnapshots = querySnapshot.docs;
      const artworkObjects = artworkSnapshots.map((queryDocumentSnapshot) =>
        queryDocumentSnapshot.data()
      );
      const urls = await Promise.all(
        artworkObjects.map((artworkObject) =>
          storage().ref(artworkObject.variantLocations.medium).getDownloadURL()
        )
      );
      return artworkObjects.map((artworkObject, index) => ({
        width: artworkObject.width,
        height: artworkObject.height,
        url: urls[index],
        artist: artworkObject.artist,
        artistUrl: artworkObject.artistUrl,
        platform: artworkObject.platform,
        platformUrl: artworkObject.platformUrl,
        color: "#fff",
        snapshot: artworkSnapshots[index],
      }));
    })
    .catch(console.log);
}

function getImage(id) {
  return unsplash.photos
    .getPhoto(id)
    .then(toJson)
    .then(transformUnsplashImageObject);
}

function setImageAsSelected(imageObject) {
  // TODO: fix this
  // unsplash.photos.downloadPhoto(imageObject);
}

export {
  getRandomImages,
  getSearchedImages,
  getNewestImages,
  getMostPopularImages,
  getImage,
  setImageAsSelected,
  getArtworks,
};
