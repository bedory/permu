import React from "react";
import { Image, TouchableWithoutFeedback } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Caption from "../../general/Caption/Caption";
import { setImageAsSelected } from "../imageApi";
import useImagePreviewStyle from "./ImagePreviewStyle";

const ImagePreview = ({ imageObject }) => {
  const style = useImagePreviewStyle(imageObject);
  const navigation = useNavigation();

  return (
    <>
      <TouchableWithoutFeedback
        style={style.root}
        onPress={() => {
          setImageAsSelected(imageObject);
          navigation.navigate("GameSetup", {
            imageObject,
          });
        }}
      >
        <Image style={style.image} source={{ uri: imageObject.url }} />
      </TouchableWithoutFeedback>
      <Caption imageObject={imageObject} />
    </>
  );
};

export default React.memo(ImagePreview, () => false);
