import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle(imageObject) {
  const { windowWidth } = useStyleVariables();
  const width = windowWidth * 0.9;
  const height = (width * imageObject.height) / imageObject.width;
  return StyleSheet.create({
    root: {
      width,
      height,
      backgroundColor: imageObject.color,
      borderRadius: 10,
    },
    image: { width, height, borderRadius: 10 },
  });
}

export default useStyle;
