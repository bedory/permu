import React from "react";
import { View } from "react-native";
import useHeaderStyle from "./HeaderStyle";
import SearchBar from "./SearchBar/SearchBar";
import Tabs from "./Tabs/Tabs";

const Header = ({
  setTypeOfImages,
  typeOfImages,
  changeSearchQuery,
  searchQuery,
  clearSearchQuery,
}) => {
  const style = useHeaderStyle();
  return (
    <View style={style.root}>
      <SearchBar
        onChangeText={changeSearchQuery}
        value={searchQuery}
        onClear={clearSearchQuery}
        onBlur={() => setTypeOfImages("search")}
      />
      <Tabs onPress={setTypeOfImages} value={typeOfImages} />
    </View>
  );
};

export default React.memo(Header);
