import React from "react";
import { View } from "react-native";
import useTabsStyle from "./TabsStyle";
import ToggleButton from "../../../general/ToggleButton/ToggleButton";

const Tabs = ({ onPress, value }) => {
  const style = useTabsStyle();
  return (
    <View style={style.root}>
      <ToggleButton
        text="random"
        toggled={value === "random"}
        containerStyle={
          value === "random"
            ? style.toggledButtonContainer
            : style.buttonContainer
        }
        onPress={() => onPress("random")}
      />

      <ToggleButton
        text="newest"
        toggled={value === "newest"}
        containerStyle={
          value === "newest"
            ? style.toggledButtonContainer
            : style.buttonContainer
        }
        onPress={() => onPress("newest")}
      />
      <ToggleButton
        text="artworks"
        toggled={value === "art"}
        containerStyle={
          value === "art" ? style.toggledButtonContainer : style.buttonContainer
        }
        onPress={() => onPress("art")}
      />
    </View>
  );
};

export default Tabs;
