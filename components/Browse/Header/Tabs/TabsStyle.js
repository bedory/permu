import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { accentColor, sm } = useStyleVariables();
  return StyleSheet.create({
    root: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    toggledButtonContainer: {
      borderBottomWidth: 2,
      borderColor: accentColor,
      flex: 1,
      alignItems: "center",
      padding: sm,
    },
    buttonContainer: {
      flex: 1,
      alignItems: "center",
      padding: sm,
    },
  });
}

export default useStyle;
