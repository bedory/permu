import { StyleSheet } from "react-native";
import useStyleVariables from "../../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const { smFont, fontFamily, textColor} = useStyleVariables();
  return StyleSheet.create({
    root: {},
    bar: {
      fontSize: smFont,
      fontFamily,
      color: textColor,
    },
  });
}

export default useStyle;
