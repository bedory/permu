import React, { useContext } from "react";
import { View, TextInput } from "react-native";
import useSearchBarStyle from "./SearchBarStyle";
import { ThemeContext } from "../../../general/ThemeHook/ThemeHook";

const SearchBar = ({ onChangeText, value, onBlur }) => {
  const style = useSearchBarStyle();
  const { textColor, disabledTextColor, mode, xl } = useContext(ThemeContext);
  return (
    <View style={style.root}>
      <TextInput
        style={style.bar}
        underlineColorAndroid={textColor}
        inlineImageLeft={
          mode === "dark" ? "round_search_white_24" : "round_search_black_24"
        }
        inlineImagePadding={xl}
        placeholder={"search"}
        placeholderTextColor={disabledTextColor}
        onChangeText={onChangeText}
        value={value}
        onBlur={onBlur}
      />
    </View>
  );
};

export default React.memo(SearchBar);
