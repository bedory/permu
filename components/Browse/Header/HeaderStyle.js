import { StyleSheet } from "react-native";
import useStyleVariables from "../../general/StyleVariablesHook/StyleVariablesHook";

function useStyle() {
  const {sm, md, xl} = useStyleVariables();
  return StyleSheet.create({
    root: {
      paddingHorizontal: xl,
      paddingTop: sm,
      paddingBottom: md,
    },
  });
}

export default useStyle;
