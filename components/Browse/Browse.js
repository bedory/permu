import React, { useEffect, useState, useCallback, useRef } from "react";
import { View, Image, FlatList } from "react-native";
import useBrowseStyle from "./BrowseStyle";
import {
  getArtworks,
  getMostPopularImages,
  getNewestImages,
  getRandomImages,
  getSearchedImages,
} from "./imageApi";
import Header from "./Header/Header";
import ImagePreview from "./ImagePreview/ImagePreview";
import Screen from "../general/Screen/Screen";
import StyledText from "../general/StyledText/StyledText";
import LoadingIndicator from "../general/LoadingIndicator/LoadingIndicator";

const Browse = () => {
  const style = useBrowseStyle();
  const numberImagesPerFetch = 15;
  const [imageObjects, setImageObjects] = useState([]);
  const [noResultsMessage, setNoResultsMessage] = useState("");
  const pageNumber = useRef(0);
  const [typeOfImages, setTypeOfImages] = useState("random");
  const [searchQuery, setSearchQuery] = useState("");
  const queryDebounceTimeout = useRef();
  const queryDebounceTime = 500;
  const [refreshing, setRefreshing] = useState(true);
  const [fetching, setFetching] = useState(true);

  const cacheImage = useCallback((imageObject) => {
    Image.prefetch(imageObject.url);
  }, []);

  const getImages = useCallback(
    ({
      type = typeOfImages,
      clearPreviousImages = false,
      query = searchQuery,
    }) => {
      setFetching(true);
      pageNumber.current++;
      let imagesPromise;
      if (type === "random") {
        imagesPromise = getRandomImages(numberImagesPerFetch);
      } else if (type === "art") {
        imagesPromise = getArtworks(
          pageNumber.current,
          numberImagesPerFetch,
          !clearPreviousImages &&
            imageObjects &&
            imageObjects.length > 0 &&
            imageObjects[imageObjects.length - 1].snapshot
        );
      } else if (type === "newest") {
        imagesPromise = getNewestImages(
          pageNumber.current,
          numberImagesPerFetch
        );
      } else if (type === "search") {
        imagesPromise = getSearchedImages(
          query,
          pageNumber.current,
          numberImagesPerFetch
        );
      }

      imagesPromise
        .then((imageObjects) => {
          imageObjects.forEach((imageObject) => cacheImage(imageObject));
          if (imageObjects.length === 0) {
            setNoResultsMessage("Could not find any images.");
          }
          if (clearPreviousImages) {
            setImageObjects(imageObjects);
          } else {
            setImageObjects((prevImageObjects) => {
              return prevImageObjects.concat(imageObjects);
            });
          }
        })
        .catch(() => {
          setNoResultsMessage(
            "Something went wrong. Are you sure you have internet access?"
          );
        })
        .finally(() => {
          setRefreshing(false);
          setFetching(false);
        });
    },
    [cacheImage, searchQuery, typeOfImages, imageObjects]
  );

  const clearQuery = useCallback(() => {
    setSearchQuery("");
  }, []);

  const changeSearchQuery = useCallback(
    (newQuery) => {
      setSearchQuery(newQuery);
      clearTimeout(queryDebounceTimeout.current);
      queryDebounceTimeout.current = setTimeout(() => {
        if (/\S/.test(newQuery)) {
          setRefreshing(true);
          setTypeOfImages("search");
          pageNumber.current = 0;
          getImages({
            clearPreviousImages: true,
            type: "search",
            query: newQuery,
          });
        }
      }, queryDebounceTime);
    },
    [getImages]
  );

  const setTypeOfImagesAndReset = useCallback(
    (type) => {
      setRefreshing(true);
      setTypeOfImages(type);
      pageNumber.current = 0;
      getImages({ type, clearPreviousImages: true });
    },
    [getImages]
  );

  const reset = useCallback(() => {
    setRefreshing(true);
    pageNumber.current = 0;
    getImages({ clearPreviousImages: true });
  }, [getImages]);

  useEffect(() => getImages({}), []);

  return (
    <Screen style={style.root}>
      <FlatList
        ListHeaderComponent={
          <Header
            setTypeOfImages={setTypeOfImagesAndReset}
            typeOfImages={typeOfImages}
            changeSearchQuery={changeSearchQuery}
            searchQuery={searchQuery}
            clearSearchQuery={clearQuery}
          />
        }
        ListHeaderComponentStyle={style.header}
        contentContainerStyle={style.imageList}
        data={imageObjects}
        renderItem={({ item }) => <ImagePreview imageObject={item} />}
        keyExtractor={(item) => item.url}
        ListEmptyComponent={
          <StyledText style={style.noResultMessage}>
            {noResultsMessage}
          </StyledText>
        }
        onEndReachedThreshold={0.2}
        onEndReached={getImages}
        ItemSeparatorComponent={() => <View style={style.separator} />}
        initialNumToRender={3}
        onRefresh={reset}
        refreshing={refreshing}
        ListFooterComponent={<LoadingIndicator animating={fetching} />}
      />
    </Screen>
  );
};

export default Browse;
