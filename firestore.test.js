const firebase = require("@firebase/testing");

const PROJECT_ID = "permu-5b4f5";
const userId = "PuzzlingJohn_123";
const username = "PuzzlingJohn";
const otherUserId = "ThinkingMike_456";
const otherUsername = "ThinkingMike";
const userAuth = {
  uid: userId,
  email: "john.doe@email.com",
  displayName: username,
};

const highscoreDocPath = "/highscores/testHighscore";
const playDocPath = highscoreDocPath + "/plays/testPlay";
const extraInfoDocPath = playDocPath + "/extraInfo/playDetails";
const playObject = {
  numberSteps: 16,
  time: 14455,
  userId,
  username,
};
const playDetailsObject = {
  permutationFamily: "swap",
  startField: {
    0: {
      0: {
        row: 0,
        column: 2,
      },
      1: {
        row: 0,
        column: 0,
      },
      2: {
        row: 1,
        column: 2,
      },
    },
    1: {
      0: {
        row: 2,
        column: 2,
      },
      1: {
        row: 1,
        column: 0,
      },
      2: {
        row: 0,
        column: 1,
      },
    },
    2: {
      0: {
        row: 1,
        column: 1,
      },
      1: {
        row: 2,
        column: 0,
      },
      2: {
        row: 2,
        column: 1,
      },
    },
  },
  startTime: 1596871107121,
  steps: {
    0: {
      row: 0,
      column: 1,
      permutation: "up",
      time: 1596871108380,
    },
    1: {
      row: 1,
      column: 1,
      permutation: "right",
      time: 1596871108796,
    },
  },
};

function initFirestore(auth) {
  return firebase
    .initializeTestApp({ projectId: PROJECT_ID, auth })
    .firestore();
}

function initAdminFirestore() {
  return firebase.initializeAdminApp({ projectId: PROJECT_ID }).firestore();
}

beforeEach(async () => {
  await firebase.clearFirestoreData({ projectId: PROJECT_ID });
});

test("everybody can read highscores", async () => {
  const db = initFirestore(null);
  const testDoc = db.doc(highscoreDocPath);
  await firebase.assertSucceeds(testDoc.get());
});

test("nobody can write highscores (only backend servers can)", async () => {
  const db = initFirestore(null);
  const testDoc = db.doc(highscoreDocPath);
  await firebase.assertFails(testDoc.set({ foo: "bar" }));
});

test("everybody can read play", async () => {
  const db = initFirestore(null);
  const testDoc = db.doc(playDocPath);
  await firebase.assertSucceeds(testDoc.get());
});

test("user can create plays if signed in and listing self as user", async () => {
  const db = initFirestore(userAuth);
  const testPlay = db.doc(playDocPath);
  await firebase.assertSucceeds(testPlay.set(playObject));
});

test("user cannot create plays if not signed in", async () => {
  const db = initFirestore(null);
  const testPlay = db.doc(playDocPath);
  await firebase.assertFails(testPlay.set(playObject));
});

test("user cannot create plays if listing other user", async () => {
  const db = initFirestore(userAuth);
  const testPlay = db.doc(playDocPath);
  await firebase.assertFails(
    testPlay.set({ ...playObject, userId: otherUserId })
  );
});

test("user cannot create plays if one of required fields is missing", async () => {
  const db = initFirestore(userAuth);
  const testPlay = db.doc(playDocPath);
  let playObjectWithMissingField = {
    // missing username
    numberSteps: 16,
    time: 14455,
    userId,
  };
  await firebase.assertFails(testPlay.set(playObjectWithMissingField));
});

test("user cannot create plays if it has non-permitted fields", async () => {
  const db = initFirestore(userAuth);
  const testPlay = db.doc(playDocPath);
  await firebase.assertFails(
    testPlay.set({ ...playObject, someOtherField: "test" })
  );
});

test("user cannot update plays", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(userAuth);
  const playDoc = db.doc(playDocPath);
  await firebase.assertFails(playDoc.update({ username: otherUsername }));
});

test("user cannot delete plays", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(userAuth);
  const playDoc = db.doc(playDocPath);
  await firebase.assertFails(playDoc.delete());
});

test("nobody can read playDetails", async () => {
  const db = initFirestore(null);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertFails(testDoc.get());
});

test("user can create playDetail for own plays if signed in", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  console.log(playDocPath, playObject);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(userAuth);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertSucceeds(testDoc.set(playDetailsObject));
});

test("user cannot create playDetail if not signed in", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(null);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertFails(testDoc.set(playDetailsObject));
});

test("user cannot create playDetail for play of other user", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set({ ...playObject, userId: otherUserId });

  const db = initFirestore(userAuth);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertFails(testDoc.set(playDetailsObject));
});

test("user cannot create playDetail if play doesn't exist", async () => {
  const db = initFirestore(userAuth);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertFails(testDoc.set(playDetailsObject));
});

test("user cannot create playDetails if one of required fields is missing", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(userAuth);
  const testDoc = db.doc(extraInfoDocPath);
  let playDetailsObjectWithMissingField = {
    // startField missing
    permutationFamily: "swap",
    startTime: 1596871107121,
    steps: {},
  };
  await firebase.assertFails(testDoc.set(playDetailsObjectWithMissingField));
});

test("user cannot create plays if it has non-permitted fields", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDoc = adminDb.doc(playDocPath);
  await setupPlayDoc.set(playObject);

  const db = initFirestore(userAuth);
  const testDoc = db.doc(extraInfoDocPath);
  await firebase.assertFails(
    testDoc.set({ ...playDetailsObject, someOtherField: "test" })
  );
});

test("user cannot update playDetail", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDetailsDoc = adminDb.doc(extraInfoDocPath);
  await setupPlayDetailsDoc.set(playDetailsObject);

  const db = initFirestore(userAuth);
  const playDoc = db.doc(playDocPath);
  await firebase.assertFails(playDoc.update({ time: 5 }));
});

test("user cannot delete playDetail", async () => {
  const adminDb = initAdminFirestore();
  const setupPlayDetailsDoc = adminDb.doc(extraInfoDocPath);
  await setupPlayDetailsDoc.set(playDetailsObject);

  const db = initFirestore(userAuth);
  const playDoc = db.doc(playDocPath);
  await firebase.assertFails(playDoc.delete());
});

afterAll(async () => {
  await firebase.clearFirestoreData({ projectId: PROJECT_ID });
});

/*
checkDocumentsFields(requiredFields, optionalFields) {
let allFields = requiredFields.concat(optionalFields)
return request.resource.data.keys().hasAll(requiredFields) && request.resource.data.keys().hasOnly(allFields);

function hasAllRequiredFields() {
let requiredFields = ["userId", "username", "..."]
return (request.resource.data.keys().hasAll(requiredFields);
}

function hasOnlyAllowedFields() {
let optionalFields = ["otherOptionField", "optionField"]
let allowedFields = requiredFields.concat(optionalFields);
return (request.resource.data.keys().hasOnly(allowedFields));
 */
