#!/bin/bash

# This script is used to reset the a user's premium
# param 1: user id
curl -X POST -H "Content-Type: application/json" -H "X-Auth-Token:BewJfz0y1H6Sdmh0zcYCBivdrQrPPkMS" -d "{\"type\": \"refund\", \"data\": { \"userId\": \"$1\" }}" https://us-central1-permu-5b4f5.cloudfunctions.net/setPremium
echo ""
