#!/bin/bash

## INSTRUCTIONS
# ./createComponent <ComponentName> [path relative to components folder]

if [ "$1" == "" ]; then
  echo "Need to supply component name as first argument"
  exit 1
fi

componentPath="."
if [ "$2" != "" ]; then
  componentPath="$2"
fi

cd ../components/

# Check if the componentPath exists
if [ ! -d "$componentPath" ]; then
  echo "The supplied path doesn't exist. Try again with a valid path."
  exit 1
fi

# Check if directory (and therefore the component) already exists. If so, ask if override
if [ -d "$componentPath/$1" ]; then
  read -p "Component already exists. Do you want to override the component? (y/n)" answer
  case $answer in
    [Yy]* ) ;; # Continue
    * ) exit 1;;
  esac
else
  mkdir $componentPath/$1
fi

cp ./ComponentTemplate/ComponentTemplate.js $componentPath/$1/$1.js
cp ./ComponentTemplate/ComponentStyleTemplate.js $componentPath/$1/$1Style.js
sed -i s/ComponentTemplate/$1/g $componentPath/$1/$1.js
sed -i s/ComponentStyleTemplate/$1Style/g $componentPath/$1/$1.js
