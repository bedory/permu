import React from "react";
import App from "next/app";
import { ThemeProvider, createGlobalStyle } from "styled-components";

const theme = {
  backgroundColor: "#202020",
  cellBackgroundColor: "#303030",
  contrastBackgroundColor: "#101010",
  darkAccentColor: "#264c65",
  textColor: "#eaeaea",
  disabledTextColor: "#b7b7b7",
  blue: "#448bb8",
  green: "#236923",
  red: "#aa1f1f",
  yellow: "#9da403",
  disabledOpacity: 0.3,
};

const GlobalStyle = createGlobalStyle`
        @font-face {
            font-family: Comfortaa;
            src: url(Comfortaa-Light.ttf);
        }
       
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: Comfortaa, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
          color: ${({ theme }) => theme.textColor};
          
          font-size: 8pt;
          
          @media only screen and (min-width: 768px) {
            font-size: 12pt;
          }
        }
        * {
          box-sizing: border-box;
        }
        
        h1, h2, h3, h4, h5, h6, p, a {
            padding: 0;
            margin: 0;
        }
        
        h1 {
            font-size: 5em;
        }
        
        h2 {
            font-size: 4em;
        }
        
        h3 {
            font-size: 3em;
        }
        
        h4 {
            font-size: 2.5em;
        }
        
        h5 {
            font-size: 2em;
        }
        
        h6 {
            font-size: 1.5em;
        }
        
        p, a {
            font-size: 1em;
        }
`;
export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
        <GlobalStyle />
      </ThemeProvider>
    );
  }
}
