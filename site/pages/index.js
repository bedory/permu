import React, { useContext } from "react";
import Head from "next/head";
import Layout, {
  centerStyle,
  headerHeightDesktop,
  headerHeightMobile,
} from "../components/Layout";
import styled, { ThemeContext } from "styled-components";
import VideoSection from "../components/index/VideoSection";
import FeatureSection from "../components/index/FeatureSection";
import CallToAction from "../components/index/CallToActionSection";

const Main = styled.main`
  width: 100%;
  padding: 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  overflow: hidden;
`;

const BaseSection = styled.section`
  height: 36rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const callToActionHeight = "16rem";

const VideoSectionContainer = styled(BaseSection)`
  ${centerStyle}
  height: calc(100vh - ${headerHeightMobile} - ${callToActionHeight});
  
  @media only screen and (min-width: 768px) {
    height: calc(100vh - ${headerHeightDesktop} - ${callToActionHeight});
  }
`;

const CallToActionContainer = styled(BaseSection)`
  height: ${callToActionHeight};
`;

const FeatureSections = styled.div`
  margin: 6rem 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
`;

export default function Home() {
  const { green, yellow, blue } = useContext(ThemeContext);
  return (
    <Layout>
      <Head>
        <title>permu | between brainbreaker and jigsaw puzzle</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <VideoSectionContainer>
          <VideoSection />
        </VideoSectionContainer>

        <CallToActionContainer>
          <CallToAction />
        </CallToActionContainer>

        <FeatureSections>
          <FeatureSection
            imageUrl="/feature_game.jpg"
            side="left"
            color={green}
            title="swipe to solve"
            description="
                permu is puzzling game that fits somewhere between a rubik's
                cube and a jigsaw puzzle. The goal is to reassemble a scrambled
                field. You do so by simply swiping the cells. Just pick it up
                and start swiping.
            "
          />
          <FeatureSection
            imageUrl="/feature_own_images.jpg"
            side="right"
            color={blue}
            title="your play style"
            description="
                You can between choose use images as the playing field.
            "
          />
          <FeatureSection
            imageUrl="/feature_images.jpg"
            side="left"
            color={yellow}
            title="use images"
            description="
                You can use images as the playing field.
            "
          />
        </FeatureSections>
      </Main>
    </Layout>
  );
}
