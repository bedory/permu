import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 1em;

  @media only screen and (min-width: 768px) {
    flex-direction: row;
  }
`;

const GooglePlay = styled.img`
  height: 6em;
`;

function CallToAction() {
  return (
    <Container>
      <h3>Play it now:</h3>
      <a href="https://play.google.com/store/apps/details?id=com.bedory.permu">
        <GooglePlay src="/google-play.svg" aria-label="get it on google play" />
      </a>
    </Container>
  );
}

export default CallToAction;
