import React from "react";
import styled from "styled-components";

const WidthSetter = styled.div`
  max-width: 64em;
  width: 100vw;
  padding: 1em;
`;

const Container = styled.div`
  position: relative;
  overflow: hidden;
  padding-top: 56.25%;
`;

const YoutubePlayer = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
`;

function VideoSection() {
  return (
    <WidthSetter>
      <Container>
        <YoutubePlayer
          src="https://www.youtube.com/embed/AFqQmh6URpw"
          frameBorder="0"
          gesture="media"
          allow="encrypted-media"
          allowFullScreen
        />
      </Container>
    </WidthSetter>
  );
}

export default VideoSection;
