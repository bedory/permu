import React from "react";
import styled from "styled-components";

const FeatureSect = styled.section`
  position: relative;
  min-height: 48rem;
  display: flex;
  align-items: stretch;
  flex-direction: column;

  & + & {
    margin-top: 12rem;
  }

  &::before {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    content: "";
    z-index: 0;
    background-image: ${({ imageUrl }) => `url(${imageUrl})`};
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    // background-attachment: fixed;
  }

  @media only screen and (max-width: 768px) {
    overflow-y: hidden;
    min-height: 84rem;
    justify-content: flex-end;

    &::before {
      left: 2rem;
      right: 2rem;
      bottom: 20rem;
    }
  }

  @media only screen and (min-width: 768px) {
    flex-direction: row;
    justify-content: ${({ side }) =>
      side === "left" ? "flex-start" : "flex-end"};

    &::before {
      left: ${({ side }) => (side === "left" ? "33%" : "0")};
      right: ${({ side }) => (side === "left" ? "0" : "33%")};
      top: 1rem;
      bottom: 1rem;
    }
  }
`;

const FeatureDescriptionContainer = styled.div`
  position: relative;
  z-index: 2;
  padding: 3em;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  &::before {
    margin: 0 1em;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    content: "";
    background-color: ${({ color }) => color};
    box-shadow: 0 0 50px 2px #000;
  }

  @media only screen and (max-width: 768px) {
    &::before {
      transform: skewY(-7.5deg);
      bottom: -500px;
    }
  }

  @media only screen and (min-width: 768px) {
    padding: 5em;
    width: 40%;

    &::before {
      left: ${({ side }) => (side === "left" ? "-500px" : "-6em")};
      right: ${({ side }) => (side === "left" ? "-6em" : "-500px")};
      transform: ${({ side }) =>
        side === "left" ? "skew(25deg)" : "skew(25deg)"};
    }
  }
`;

const FeatureTitle = styled.h2`
  text-align: center;
  margin: 0.33em;
  position: relative;
  z-index: 3;
`;

const FeatureDescription = styled.h5`
  text-align: center;
  opacity: 0.75;
  z-index: 3;
`;

function FeatureSection({ imageUrl, side, color, title, description }) {
  return (
    <FeatureSect imageUrl={imageUrl} side={side} color={color}>
      <FeatureDescriptionContainer color={color} side={side}>
        <header>
          <FeatureTitle>{title}</FeatureTitle>
        </header>
        <FeatureDescription>{description}</FeatureDescription>
      </FeatureDescriptionContainer>
    </FeatureSect>
  );
}

export default React.memo(FeatureSection);
