import React from "react";
import styled from "styled-components";

const centerStyle = `
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const headerHeightMobile = "12rem";
const headerHeightDesktop = "8rem";
const footerHeight = "6rem";

const Container = styled.div`
  position: relative;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  background-color: ${({ theme }) => theme.backgroundColor};
`;

const Header = styled.header`
  height: ${headerHeightMobile};
  padding: 1em 0;
  ${centerStyle}

  @media only screen and (min-width: 768px) {
    font-size: 1rem;
    height: ${headerHeightDesktop};
  }
`;

const Footer = styled.footer`
  min-height: ${footerHeight};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 1em;
  margin-top: 1em;
  font-size: 2rem;
  gap: 1em;

  @media only screen and (min-width: 768px) {
    flex-direction: row;
    font-size: 1rem;
  }
`;

const BigLogo = styled.img`
  height: 100%;
`;

const SocialMediaList = styled.ul`
  order: 2;
  flex: 1;
  list-style: none;
  display: flex;
  flex-direction: row;
  position: relative;
  z-index: auto;
  justify-content: center;
  align-items: center;
  padding: 0;

  a {
    opacity: 0.6;
  }
  a:hover,
  a:focus {
    opacity: 1;
  }

  @media only screen and (min-width: 768px) {
    order: 3;
  }
`;

const SocialMediaIcon = styled.img`
  width: 2em;
  height: 2em;
  margin: 0 0.5em;
`;

const CreatedAndOwnedContainer = styled.div`
  order: 3;
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;

  p {
    margin: 0.5em;
  }

  @media only screen and (min-width: 768px) {
    flex-direction: row;
    order: 1;
  }
`;

const BedoryGamesLogo = styled.img`
  height: 4em;
`;

const BedoryLogo = styled.img`
  height: 2.5em;
`;

const AvailableOnContainer = styled.div`
  order: 1;
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  
    @media only screen and (min-width: 768px) {
    flex-direction: row;
    order: 2;
`;

const GooglePlay = styled.img`
  height: 3em;
`;

function Layout({ children }) {
  return (
    <Container>
      <Header>
        <BigLogo src="/big_logo.svg" aria-label="permu" />
      </Header>

      {children}
      <Footer>
        <CreatedAndOwnedContainer>
          <p>Created and owned by </p>
          <a href="https://bedory.com">
            <BedoryLogo src="/bedory_logo_big.svg" aria-label="bedory" />
          </a>
          <p>and</p>
          <a href="https://games.bedory.com">
            <BedoryGamesLogo
              src="/bedory_games_logo_big.svg"
              aria-label="bedory games"
            />
          </a>
        </CreatedAndOwnedContainer>
        <AvailableOnContainer>
          <a href="https://play.google.com/store/apps/details?id=com.bedory.permu">
            <GooglePlay
              src="/google-play.svg"
              aria-label="get it on google play"
            />
          </a>
        </AvailableOnContainer>
        <SocialMediaList>
          <li>
            <a href="https://www.facebook.com/bedory.solutions">
              <SocialMediaIcon
                src="/facebook.svg"
                aria-label="facebook"
                alt=""
              />
            </a>
          </li>
          <li>
            <a href="https://twitter.com/bedory_solution">
              <SocialMediaIcon src="/twitter.svg" aria-label="twitter" alt="" />
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com/channel/UCRKdlSQA5O6Bhi31h_cX7cA">
              <SocialMediaIcon src="/youtube.svg" aria-label="youtube" alt="" />
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/bedory.solutions/">
              <SocialMediaIcon
                src="/instagram.svg"
                aria-label="instagram"
                alt=""
              />
            </a>
          </li>
        </SocialMediaList>
      </Footer>
    </Container>
  );
}

export default Layout;
export { centerStyle, headerHeightDesktop, headerHeightMobile, footerHeight };
