const functions = require("firebase-functions");

const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "permu-5b4f5.appspot.com",
});

const sharp = require("sharp");

const path = require("path");
const os = require("os");
const fs = require("fs-extra");
const Busboy = require("busboy");
const { v4: uuid } = require("uuid");

exports.setPremium = functions.https.onRequest(async (req, res) => {
  if (req.method === "POST") {
    // iapHub token
    if (req.get("x-auth-token") === "BewJfz0y1H6Sdmh0zcYCBivdrQrPPkMS") {
      if (req.body.type === "refund") {
        try {
          await admin
            .auth()
            .setCustomUserClaims(req.body.data.userId, { premium: false });
          res.status(200).send("Successfully refunded");
        } catch (e) {
          res.status(500).send("Something went wrong while processing refund.");
        }
      } else if (req.body.type === "purchase") {
        try {
          await admin
            .auth()
            .setCustomUserClaims(req.body.data.userId, { premium: true });
          res.status(200).send("Successfully granted premium");
        } catch (e) {
          res.status(500).send("Something went wrong while granting premium.");
        }
      } else {
        res.status(200).send("Authorized, but type not recognized.");
      }
    } else {
      res.status(401).send("Not Authorized.");
    }
  } else {
    res.status(405).send("POST method required.");
  }
  // this should never be triggered
  res.end();
});

exports.changeUsername = functions.https.onCall(async (data, context) => {
  function validateUsername() {
    let inputValid = true;
    let errorMessage = null;
    let username = data.username;
    let oldUsername = context.auth.token.displayName;
    if (!username || !username.length) {
      inputValid = false;
      errorMessage = "Username cannot be empty.";
    } else if (username.length < 5) {
      inputValid = false;
      errorMessage = "Username must be at least 5 characters.";
    } else if (username.length > 25) {
      inputValid = false;
      errorMessage = "Username cannot be more than 25 characters.";
    } else if (username === oldUsername) {
      inputValid = false;
      errorMessage = "Username cannot be the same as the old username.";
    }
    return [inputValid, errorMessage];
  }

  const [usernameValid, errorMessage] = validateUsername();
  if (usernameValid) {
    try {
      // watch out for collection group gotcha's: https://firebase.googleblog.com/2019/06/understanding-collection-group-queries.html
      const userPlaysQuery = admin
        .firestore()
        .collectionGroup("plays")
        .where("userId", "==", context.auth.uid);
      const userPlaySnapshots = await userPlaysQuery.get();
      let batch = admin.firestore().batch();
      userPlaySnapshots.forEach((playSnapshot) => {
        batch.update(playSnapshot.ref, { username: data.username });
      });
      await Promise.all([
        batch.commit(),
        admin
          .auth()
          .updateUser(context.auth.uid, { displayName: data.username }),
      ]);
      return {
        success: true,
      };
    } catch (e) {
      console.log(e);
      return {
        success: false,
        errorMessage: "Something went wrong.",
      };
    }
  } else {
    return {
      success: false,
      errorMessage,
    };
  }
});

exports.deletePlaysWhenUserIsDeleted = functions.auth
  .user()
  .onDelete(async (user) => {
    const userPlaysQuery = admin
      .firestore()
      .collectionGroup("plays")
      .where("userId", "==", user.uid);
    const userPlaySnapshots = await userPlaysQuery.get();
    const playsDeletePromises = userPlaySnapshots.docs.map((playSnapshot) => {
      return Promise.all([
        playSnapshot.ref.delete(),
        playSnapshot.ref.collection("extraInfo").doc("playDetails").delete(),
      ]);
    });
    return Promise.all(playsDeletePromises);
  });

exports.uploadArtwork = functions.https.onRequest(async (req, res) => {
  if (req.method === "POST") {
    if (req.query.token === "10F7cAVcMfbt") {
      try {
        const busboy = new Busboy({
          headers: req.headers,
          limits: { fileSize: 10 * 1024 * 1024 },
        });
        const fields = {};
        const tempDir = fs.mkdtempSync(path.join(os.tmpdir(), "artwork-"));
        const bucket = admin.storage().bucket();
        console.log("Starting processing image artwork upload.");

        busboy.on(
          "field",
          (
            fieldname,
            val,
            fieldnameTruncated,
            valTruncated,
            encoding,
            mimetype
          ) => {
            fields[fieldname] = val;
            console.log(
              `Successfully read field "${fieldname}" with value "${val}".`
            );
          }
        );

        busboy.on(
          "file",
          async (fieldname, stream, filename, encoding, mimetype) => {
            const filePath =
              path.join(tempDir, fields.title) +
              "_original." +
              mimetype.substr(6);
            fields.artwork = filePath;
            stream.pipe(
              fs.createWriteStream(filePath, {
                metadata: { contentType: mimetype },
              })
            );
            console.log("Artwork successfully uploaded.");
          }
        );

        // This callback will be invoked after all uploaded files are saved.
        busboy.on("finish", async () => {
          console.log(`Start transforming artwork ${fields.title}.`);
          const promises = [];
          const variants = [
            { size: 2000, name: "full", fit: "inside", quality: 75 },
            { size: 1000, name: "medium", fit: "inside", quality: 75 },
            { size: 500, name: "small", fit: "inside", quality: 75 },
            { size: 250, name: "thumbnail", fit: "cover", quality: 50 },
          ];

          const baseTransform = sharp(fields.artwork);
          const metadata = await baseTransform.clone().metadata();
          fields.width = metadata.width;
          fields.height = metadata.height;

          const variantLocations = {};
          variants.forEach(({ size, name, fit, quality }) => {
            const pathToVariant =
              path.join(tempDir, fields.title.replace(/\s/g, "_")) +
              "_" +
              name +
              ".jpeg";
            const variantDestination = `artists/${fields.artist.replace(
              /\s/g,
              "_"
            )}/${fields.title.replace(/\s/g, "_")}/${fields.title.replace(
              /\s/g,
              "_"
            )}_${name}.jpeg`;
            variantLocations[name] = variantDestination;
            const transform = baseTransform
              .clone()
              .resize(size, size, { fit })
              .jpeg({ quality })
              .withMetadata()
              .toFile(pathToVariant)
              .then(() => {
                return bucket.upload(pathToVariant, {
                  gzip: true,
                  destination: variantDestination,
                  metadata: {
                    contentType: "image/jpeg",
                    cacheControl: "public, max-age=31536000",
                    metadata: {
                      title: fields.title,
                      artist: fields.artist,
                      variant: name,
                    },
                  },
                });
              })
              .then((data) => {
                console.log(`Done creating and uploading ${name} variant`);
                return data;
              });
            promises.push(transform);
          });

          const responses = await Promise.all(promises);
          fs.removeSync(tempDir);
          console.log("Done uploading variants");

          await admin
            .firestore()
            .collection("artworks")
            .doc(
              `${fields.artist.replace(/\s/g, "_")}-${fields.title.replace(
                /\s/g,
                "_"
              )}`
            )
            .set({
              title: fields.title,
              artist: fields.artist,
              artistUrl: fields.artworkUrl,
              platform: fields.platformName,
              platformUrl: fields.platformUrl,
              variantLocations: variantLocations,
              width: fields.width,
              height: fields.height,
              timestamp: admin.firestore.Timestamp.now(),
            })
            .catch(console.log);
          console.log("Done uploading info file to firestore.");
          console.log("Done.");
          res.end();
        });

        // The raw bytes of the upload will be in req.rawBody.  Send it to busboy, and get
        // a callback when it's finished.
        busboy.end(req.rawBody);
      } catch (e) {
        console.log(e);
        res.status(500).send("Something went wrong while processing image.");
      }
    } else {
      res.status(401).send("Not Authorized.");
    }
  } else {
    res.status(405).send("POST method required.");
  }
  // this should never be triggered
  res.end();
});
