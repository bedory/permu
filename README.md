# rotatit
## description

## instructions

## file structure

## software stack

## scripts

## TODO
### game
* check performance multiple devices
* ability to import and use specific field (useful for competitions in which everybody uses same field)
* save game to continue later (but then doesn't count towards high score)
* at end of game, also record whether they used "find cell" or "show end result". Only qualify for highscore if didn't use. Possibly show (negative) count of how many times they used find cell."
* favorite image (long press or heart upper right corner)
* A loading indicator when rotating the game screen
* Upload permutations and permutationFamilies to firestore intead of hardcoding
* make permutation indicators with image field be opposite color of image
* settings whether inverse color permutation indicator or always black white inverse
* minimal field dimensions depending on permutation
* ability users create custom permutations
* add google play analytics
* add question whether the user is competitive or casual when first opening the app
* unique username
* option in settings to disable screen transitions
* low hardware phone settings preset
* option show start banner
* How to play video and images on how to play page
* request refund button somewhere (either account or abuot page)
* setting: confirm before resetting
* promotion: find systematic way to solve ... and receive 1% of the app's profits (zet Simon in)
* switch to appodeal
* after having played a couple of games: Did you enjoy game? -> please give rating. Otherwise, contact info@bedory for complaint.
* option to turn on/off modal animations
* refactor field to use object of objects instead of array of arrays in order to integrate better with firestore
* write unit tests (jest and enzyme)
* gitlab ci-cd automatic test
* gitlab ci-cd automatic lint checker and reporter in gitlab
* create nicer website
* fix the issue with the image field cells sometimes having a single pixel between them. Try the RN component PixelRatio
* adapt google play screenshots to new interface.
* developed and owned by bedory & bedory games (with logos)
* fix ad consent card preventing splash screen hiding
* fix google sign in 
* when the field is completed, somehow allows the user to appreciate the final result and the fact that he completed the field. Maybe confetti animation and a clear view of the final result.
* fix promo codes

### options
* Two additional default settings: fot people who like sudoku (math puzzles) and for people who like traditional mathing puzzles.
* option indicating that when swiping quickly compute swipe angle based on location press, and when long press compute angle based on center button (see execute swiping)
